#include <Wire.h>
#include <RTClib.h>
#include <EEPROM.h>
RTC_DS1307 RTC;
int x;


void setup () {
  pinMode(9, OUTPUT);   // Set our LED as an output pin
  pinMode(A3, INPUT_PULLUP);
  digitalWrite(A3, HIGH);
  Wire.begin();              // Start our wire and real time clock
  RTC.begin();
  Serial.begin(9600);
  if (! RTC.isrunning()) {                       // Make sure RTC is running
    
   // RTC.adjust(DateTime(F(__DATE__), F(__TIME__)));
   // Serial.print('X');
  } 
  }

void loop () 
{ 
  int brightness; //brightness when rtc is not running
  if(Serial.available()) 
  {  
    save_serial_data(); //Data from protocol 
  }
  DateTime now = RTC.now();
  /*Send the Data to protocol for acknowledgement */
  if(x==1)
  {
    Serial.print(now.hour(),DEC);
    Serial.print(':');
    Serial.print(now.minute(),DEC);
    Serial.print('T');
    Serial.print(EEPROM.read(2));
    Serial.print(':');
    Serial.print(EEPROM.read(3));
    Serial.print(',');
    Serial.print(EEPROM.read(4));
    Serial.print(':');
    Serial.print(EEPROM.read(5));
    Serial.print('H');
    Serial.print(EEPROM.read(6));
    Serial.print('L');
    Serial.print(EEPROM.read(7));
    //Serial.print('\n');
    x=0;
  }
  if((now.year()!=2000)&&(now.year()!=2165))//check if rtc is working 
  {
    if(now.hour()>=EEPROM.read(2)&&now.minute()>= EEPROM.read(3)&&now.hour()<=EEPROM.read(4))
    {
      if(now.minute()< EEPROM.read(5))
      analogWrite(9, EEPROM.read(6));//high brightness
      else if(now.hour()<EEPROM.read(4)&&now.minute()>=EEPROM.read(5))
      analogWrite(9, EEPROM.read(6));//low brightness
      else
      analogWrite(9, EEPROM.read(7));//low brightness
    } 
    else
    {
      analogWrite(9, EEPROM.read(7));//low brightness
    }       
  }
  else
  {
     if(digitalRead(A3)==0)
    {
      while(digitalRead(A3)!=1);
      brightness=brightness+50;
      EEPROM.write(0,brightness);
    }
    analogWrite(9, EEPROM.read(0));
  }  
}


void save_serial_data()
{
  while(Serial.available())
  {
   String recieved_data = Serial.readString();
   if(recieved_data[0]==':'&& recieved_data[1]=='A'&& recieved_data[12]=='B'&& recieved_data[13]==';')//SET DATA OF time
   {
      int day1    =10*(recieved_data[2] - '0') + recieved_data[3] - '0';
      int month1  =10*(recieved_data[4] - '0') + recieved_data[5] - '0';
      int year1   =10*(recieved_data[6] - '0') + recieved_data[7] - '0';
      int hour1   =10*(recieved_data[8] - '0') + recieved_data[9] - '0';
      int minute1 =10*(recieved_data[10] - '0') + recieved_data[11] - '0';
      RTC.adjust(DateTime(year1, month1, day1, hour1, minute1, 0));//adjusting rtc values from protocol 
   }
   else if(recieved_data[0]==':'&& recieved_data[1]=='A'&&recieved_data[2]=='B' && recieved_data[17]=='B'&& recieved_data[18]==';')//SET DATA OF Brightness and brightness time
   {
      EEPROM.write(2,10*(recieved_data[3] - '0') + recieved_data[4] - '0');//High brightness hour
      EEPROM.write(3,(10*(recieved_data[5] - '0') + recieved_data[6] - '0'));//High brightness minute
      EEPROM.write(4,(10*(recieved_data[7] - '0') + recieved_data[8] - '0'));//Low brightness hour
      EEPROM.write(5,(10*(recieved_data[9] - '0') + recieved_data[10] - '0'));//Low brightness minute
      EEPROM.write(6,(100*(recieved_data[11] - '0') + 10*(recieved_data[12] - '0')+recieved_data[13] - '0'));   //High Brightness value
      EEPROM.write(7,(100*(recieved_data[14] - '0') +10*(recieved_data[15] - '0')+recieved_data[16] - '0'));//Low brightness value
   }
   else if(recieved_data[0]==':'&& recieved_data[1]=='A'&& recieved_data[2]=='H'&& recieved_data[3]=='B'&& recieved_data[4]==';')//Acknowledgement
   {
      x=1;
   }
 }   
}
