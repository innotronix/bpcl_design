#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>
#include <FS.h>
#include <EEPROM.h>
#include "index.h"
//****************************PIN CONNECTION*****************************
#define ssid      "BPCLRPDU 1.05"// WiFi SSID
#define password  "BPCLRPDU1.05" 

#define SECLK 14
#define LATCH 12//
#define SERDS1 13//
#define SERDS2 5//
#define SERDS3 4//
#define RS485 2//
#define A     0
#define B     16
#define OE    15 
#define OTAUSER         "admin"    // Set OTA user
#define OTAPASSWORD     "admin"    // Set OTA password
#define OTAPATH         "/firmware"// Set path for update
//*******************RELATED WITH pwm***************************************
//#define bright_f_low  5000//6000
//#define bright_f_high 31000//31000


#define bright_low 250//1
#define bright_high 250//7

ESP8266WebServer server ( 80 );
ESP8266HTTPUpdateServer httpUpdater;
//************************************characters for promotional message********************************************
unsigned char data_m_blank[16]= {0x00};//blank
unsigned char data_m_fslash[16]={0x02,0x02,0x06,0x06,0x0c,0x0c,0x18,0x18,0x30,0x30,0x60,0x60,0x40,0x40,0x00,0x00};//'/'
unsigned char data_m_dash[16]=  {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x3c,0x3c,0x00,0x00,0x00,0x00,0x00,0x00,0x00};//'-'
unsigned char data_m_dot[16]=   {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x18,0x18,0x00,0x00};//dot
unsigned char data_m_ex[16]=     {0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x00,0x00,0x18,0x18,0x00,0x00};//!
unsigned char data_m_A[16]=     {0x38,0x7C,0xC6,0xC6,0xC6,0xC6,0xC6,0xFE,0xFE,0xC6,0xC6,0xC6,0xC6,0xC6,0x00,0x00};//A
unsigned char data_m_B[16]=     {0xF8,0xFC,0xC6,0xC6,0xC6,0xC6,0xFC,0xFC,0xC6,0xC6,0xC6,0xC6,0xFC,0xF8,0x00,0x00};//B                               
unsigned char data_m_C[16]=     {0x38,0x7C,0xC6,0xC0,0xC0,0xC0,0xC0,0xC0,0xC0,0xC0,0xC0,0xC6,0x7C,0x38,0x00,0x00};//C 
unsigned char data_m_D[16]=     {0xF8,0xFC,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xFC,0xF8,0x00,0x00};//D                         
unsigned char data_m_E[16]=     {0xFE,0xFE,0xC0,0xC0,0xC0,0xC0,0xFC,0xFC,0xC0,0xC0,0xC0,0xC0,0xFE,0xFE,0x00,0x00};//E
unsigned char data_m_F[16]=     {0xFE,0xFE,0xC0,0xC0,0xC0,0xC0,0xF8,0xF8,0xC0,0xC0,0xC0,0xC0,0xC0,0xC0,0x00,0x00};//F
unsigned char data_m_G[16]=     {0x38,0x7C,0xC6,0xC0,0xC0,0xC0,0xC0,0xCE,0xCE,0xC6,0xC6,0xC6,0x7A,0x32,0x00,0x00};//G                           
unsigned char data_m_H[16]=     {0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xFE,0xFE,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0x00,0x00};//H 
unsigned char data_m_I[16]=     {0x7E,0x7E,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x7E,0x7E,0x00,0x00};//I   
unsigned char data_m_J[16]=     {0xFE,0xFE,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x98,0xD8,0x70,0x20,0x00,0x00};//I 
unsigned char data_m_K[16]=     {0xC2,0xC2,0xC6,0xCC,0xD8,0xF0,0xE0,0xE0,0xF0,0xD8,0xCC,0xC6,0xC2,0xC2,0x00,0x00};//K                         
unsigned char data_m_L[16]=     {0xC0,0xC0,0xC0,0xC0,0xC0,0xC0,0xC0,0xC0,0xC0,0xC0,0xC0,0xC0,0xFC,0xFC,0x00,0x00};//L                           
unsigned char data_m_M[16]=     {0xC6,0xC6,0xEE,0xEE,0xFE,0xFE,0xD6,0xD6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0x00,0x00};//M 
unsigned char data_m_N[16]=     {0xC6,0xC6,0xE6,0xE6,0xF6,0xF6,0xF6,0xDE,0xDE,0xDE,0xCE,0xCE,0xC6,0xC6,0x00,0x00};//N                             
unsigned char data_m_O[16]=     {0x38,0x7C,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0x7C,0x38,0x00,0x00};//O                               
unsigned char data_m_P[16]=     {0xF8,0xFC,0xC6,0xC6,0xC6,0xC6,0xC6,0xFC,0xF8,0xC0,0xC0,0xC0,0xC0,0xC0,0x00,0x00};//P  
unsigned char data_m_Q[16]=     {0x38,0x7C,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xD6,0xCE,0x7C,0x3A,0x00,0x00};//Q                            
unsigned char data_m_R[16]=     {0xF8,0xFC,0xC6,0xC6,0xC6,0xC6,0xFC,0xF8,0xE0,0xF0,0xD8,0xCC,0xC6,0xC2,0x00,0x00};//R 
unsigned char data_m_S[16]=     {0x38,0x7C,0xC6,0xC0,0xC0,0xC0,0x78,0x3C,0x06,0x06,0x06,0xC6,0x7C,0x38,0x00,0x00};//S                            
unsigned char data_m_T[16]=     {0x7E,0x7E,0x18,0x18,0x18,0x18,0x18,0X18,0x18,0x18,0x18,0x18,0x18,0x18,0x00,0x00};//T                            
unsigned char data_m_U[16]=     {0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0x7C,0x38,0x00,0x00};//U  
unsigned char data_m_V[16]=     {0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0x6C,0x38,0x10,0x00,0x00};//V                           
unsigned char data_m_W[16]=     {0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xD6,0xD6,0xFE,0xFE,0xEE,0xEE,0xC6,0xC6,0x00,0x00};//W
unsigned char data_m_X[16]=     {0xC6,0xC6,0xC6,0xC6,0xC6,0x44,0x38,0x38,0x44,0xC6,0xC6,0xC6,0xC6,0xC6,0x00,0x00};//X
unsigned char data_m_Y[16]=     {0xCC,0xCC,0xCC,0xCC,0xCC,0xFC,0x78,0x30,0x30,0x30,0x30,0x30,0x30,0x30,0x00,0x00};//Y 
unsigned char data_m_Z[16]=     {0xFE,0xFE,0x02,0x06,0x0C,0x08,0x18,0x30,0x20,0x60,0xC0,0x80,0xFE,0xFE,0x00,0x00};//Z

unsigned char data_m_a[16]=     {0x00,0x00,0x00,0x00,0x00,0x38,0x6C,0x06,0x3E,0x6E,0xC6,0xC6,0x6E,0x3A,0x00,0x00};//a
unsigned char data_m_b[16]=     {0xC0,0xC0,0xC0,0xC0,0xC0,0xF8,0xEC,0xC6,0xC6,0xC6,0xC6,0xC6,0xEC,0xF8,0x00,0x00};//b                             
unsigned char data_m_c[16]=     {0x00,0x00,0x00,0x00,0x00,0x38,0x6C,0xC0,0xC0,0xC0,0xC0,0xC0,0x6C,0x38,0x00,0x00};//c
unsigned char data_m_d[16]=     {0x06,0x06,0x06,0x06,0x06,0x3E,0x6E,0xC6,0xC6,0xC6,0xC6,0xC6,0x6E,0x3E,0x00,0x00};//d                      
unsigned char data_m_e[16]=     {0x00,0x00,0x00,0x00,0x00,0x38,0x6C,0xC6,0xFE,0xFE,0xC0,0xC6,0x7C,0x38,0x00,0x00};//e
unsigned char data_m_f[16]=     {0x38,0x7C,0xC6,0xC0,0xC0,0xF8,0xF8,0xC0,0xC0,0xC0,0xC0,0xC0,0xC0,0xC0,0x00,0x00};//f
unsigned char data_m_g[16]=     {0x00,0x00,0x00,0x00,0x00,0x38,0x6C,0xC6,0xC6,0x6E,0x3E,0x06,0x06,0xC6,0x6C,0x38};//g                          
unsigned char data_m_h[16]=     {0xC0,0xC0,0xC0,0xC0,0xC0,0xF8,0xFC,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0x00,0x00};//h
unsigned char data_m_i[16]=     {0x00,0x18,0x18,0x00,0x00,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x00,0x00};//i   
unsigned char data_m_j[16]=     {0x00,0x18,0x18,0x00,0x00,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x98,0xD8,0x70,0x20};//j 
unsigned char data_m_k[16]=     {0xC0,0xC0,0xC0,0xC2,0xC6,0xCC,0xD8,0xF0,0xE0,0xF0,0xD8,0xCC,0xC6,0xC2,0x00,0x00};//k                         
unsigned char data_m_l[16]=     {0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x00,0x00};//l                          
unsigned char data_m_m[16]=     {0x00,0x00,0x00,0x00,0x00,0xC6,0xEE,0xEE,0xFE,0xFE,0xD6,0xD6,0xC6,0xC6,0x00,0x00};//m 
unsigned char data_m_n[16]=     {0x00,0x00,0x00,0x00,0x00,0xD8,0xFC,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0x00,0x00};//n                            
unsigned char data_m_o[16]=     {0x00,0x00,0x00,0x00,0x00,0x38,0x6C,0xC6,0xC6,0xC6,0xC6,0xC6,0x6C,0x38,0x00,0x00};//o                              
unsigned char data_m_p[16]=     {0x00,0x00,0x00,0x00,0x00,0xF8,0xEC,0xC6,0xC6,0xC6,0xEC,0xF8,0xC0,0xC0,0xC0,0xC0};// p
unsigned char data_m_q[16]=     {0x00,0x00,0x00,0x00,0x00,0x3E,0x6E,0xC6,0xC6,0xC6,0x6E,0x3E,0x06,0x06,0x06,0x06};// q                          
unsigned char data_m_r[16]=     {0x00,0x00,0x00,0x00,0x00,0x36,0x3E,0x30,0x30,0x30,0x30,0x30,0x30,0x30,0x00,0x00};//r 
unsigned char data_m_s[16]=     {0x00,0x00,0x00,0x00,0x38,0x7C,0xC6,0xC0,0x78,0x3C,0x06,0xC6,0x7C,0x38,0x00,0x00};//s                            
unsigned char data_m_t[16]=     {0x18,0x18,0x18,0x18,0x7E,0x7E,0x18,0X18,0x18,0x18,0x18,0x18,0x1E,0x0E,0x00,0x00};//t                           
unsigned char data_m_u[16]=     {0x00,0x00,0x00,0x00,0x00,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0x7E,0x3A,0x00,0x00};//u 
unsigned char data_m_v[16]=     {0x00,0x00,0x00,0x00,0x00,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0x6C,0x38,0x10,0x00,0x00};//v                           
unsigned char data_m_w[16]=     {0x00,0x00,0x00,0x00,0x00,0xC6,0xD6,0xD6,0xFE,0xFE,0xEE,0xEE,0xC6,0xC6,0x00,0x00};//w
unsigned char data_m_x[16]=     {0x00,0x00,0x00,0x00,0x00,0x00,0x82,0xC6,0x6C,0x38,0x38,0x6C,0xC6,0x82,0x00,0x00};//x
unsigned char data_m_y[16]=     {0x00,0x00,0x00,0x00,0x00,0x00,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0x30,0x60,0xC0};//y
unsigned char data_m_z[16]=     {0x00,0x00,0x00,0x00,0x00,0x7E,0x7E,0x06,0x0C,0x18,0x30,0x60,0x7E,0x7E,0x00,0x00};//z


unsigned char data_m_9[16]=     {0x7C,0x6E,0xC6,0xC6,0xC6,0xC6,0x7E,0x36,0x06,0x06,0x06,0xC6,0xEE,0x7C,0x00,0x00};//9
unsigned char data_m_8[16]=     {0x38,0x6C,0xC6,0xC6,0xC6,0x44,0x38,0x6C,0xC6,0xC6,0xC6,0xC6,0x6C,0x38,0x00,0x00};//8
unsigned char data_m_7[16]=     {0xFE,0xFE,0x04,0x0C,0x08,0x18,0x10,0x10,0x30,0x20,0x60,0x60,0x60,0x40,0x00,0x00};//7
unsigned char data_m_6[16]=     {0x3C,0x7E,0xC6,0xC0,0xC0,0xC0,0xD8,0xFC,0xC6,0xC6,0xC6,0xC6,0x6C,0x38,0x00,0x00};//6
unsigned char data_m_5[16]=     {0xFE,0xFE,0xC0,0xC0,0xC0,0xC0,0xD8,0xFC,0xC6,0x06,0x06,0xC6,0xEC,0x78,0x00,0x00};//5
unsigned char data_m_4[16]=     {0x0C,0x1C,0x1C,0x3C,0x2C,0x6C,0x4C,0xCC,0x8C,0xFE,0xFE,0x0C,0x0C,0x0C,0x00,0x00};//4                                 
unsigned char data_m_3[16]=     {0x38,0x6C,0xC6,0xC6,0x06,0x06,0x1C,0x1C,0x06,0x06,0x06,0xC6,0xEC,0x38,0x00,0x00};//3
unsigned char data_m_2[16]=     {0x7C,0xEE,0xC6,0x06,0x06,0x06,0x06,0x0C,0x18,0x30,0x60,0xC0,0xFE,0xFE,0x00,0x00};//2                               
unsigned char data_m_1[16]=     {0x0C,0x1C,0x3C,0x0C,0x0C,0x0C,0x0C,0x0C,0x0C,0x0C,0x0C,0x0C,0x0C,0x0C,0x00,0x00};//1
unsigned char data_m_0[16]=     {0x38,0x6C,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0x6C,0x38,0x00,0x00};//0

//****************************************************small numbers and small characters*****************************
unsigned char data_num_blank[16]= {0x00};//blank
//**********************************related with english H AND - ****************************************************                                                          
unsigned char data_num_H[16]=     {0x00,0x63,0x63,0x63,0x63,0x63,0x7F,0x7F,0x63,0x63,0x63,0x63,0x63,0x00,0x00,0x00};//H                            
unsigned char data_num_DASH[16]=  {0x00,0x00,0x00,0x00,0x00,0x00,0x03,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};//DASH
unsigned char data_num_a[16]=     {0x00,0x00,0x00,0x00,0x1C,0x36,0x03,0x1F,0x37,0x63,0x63,0x37,0x1F,0x00,0x00,0x00};//a
//**********************************related with english LPG**************************************************** 
unsigned char data_num_C[16]=     {0x00,0x1C,0x3E,0x63,0x60,0x60,0x60,0x60,0x60,0x60,0x63,0x3E,0x1C,0x00,0x00,0x00};//C                            
unsigned char data_num_N[16]=     {0x00,0x63,0x73,0x7B,0x7F,0x6F,0x67,0x63,0x63,0x63,0x63,0x63,0x63,0x00,0x00,0x00};//N                           
//**********************************related with english LPG**************************************************** 
unsigned char data_num_L[16]=     {0x00,0x60,0x60,0x60,0x60,0x60,0x60,0x60,0x60,0x60,0x60,0x7F,0x7F,0x00,0x00,0x00};//L                            
unsigned char data_num_G[16]=     {0x00,0x3E,0x77,0x63,0x43,0xC0,0xC0,0xCF,0xCF,0x43,0x63,0x77,0x3B,0x00,0x00,0x00};//G   
//**********************************related with english petrol****************************************************                           
unsigned char data_num_P[16]=     {0x00,0x7C,0x7E,0X63,0x63,0x63,0x7E,0x7C,0x60,0x60,0x60,0x60,0X60,0x00,0x00,0x00};//P 
unsigned char data_num_e[16]=     {0x00,0x00,0x00,0x00,0x1C,0x36,0x63,0x7F,0x7F,0x60,0x63,0x3E,0x1C,0x00,0x00,0x00};//e                                                                 
unsigned char data_num_t[16]=     {0x00,0x0C,0x0C,0x0C,0x1F,0x1F,0x0C,0x0C,0x0C,0x0C,0x0C,0x0F,0x07,0x00,0x00,0x00};//t                                    
unsigned char data_num_r[16]=     {0x00,0x00,0x00,0x00,0x1B,0x1F,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x00,0x00,0x00};//r                                                               
unsigned char data_num_o[16]=     {0x00,0x00,0x00,0x00,0x1C,0x36,0x63,0x63,0x63,0x63,0x63,0x36,0x1C,0x00,0x00,0x00};//o
unsigned char data_num_l[16]=     {0x00,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x00,0x00,0x00};//l
//**********************************related with english diesel**************************************************** 
unsigned char data_num_D[16]=     {0x00,0xFC,0xEE,0xC6,0xC2,0xC3,0xC3,0xC3,0xC3,0xC2,0xC6,0xEE,0xFC,0x00,0x00,0x00};//D 
unsigned char data_num_i[16]=     {0x00,0x03,0x03,0x00,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x00,0x00,0x00};//i 
unsigned char data_num_s[16]=     {0x00,0x00,0x00,0x00,0x1E,0x33,0x33,0x38,0x0E,0x03,0x33,0x33,0x1E,0x00,0x00,0x00};//s
//**********************************related with english speed****************************************************
unsigned char data_num_S[16]=     {0x00,0x1C,0x37,0x63,0x60,0x60,0x3C,0x1E,0x07,0x63,0x63,0x36,0x1C,0x00,0x00,0x00};//S
unsigned char data_num_p[16]=     {0x00,0x00,0x00,0x00,0x7C,0x76,0x63,0x63,0x63,0x63,0x7E,0x6C,0X60,0x60,0x60,0x00};//p
unsigned char data_num_d[16]=     {0x00,0x03,0x03,0x03,0x1F,0x37,0x23,0x63,0x63,0x63,0x63,0x37,0X1F,0x00,0x00,0x00};//d
//**********************************related with hindi petrol**************************************************** 
unsigned char data_num_ppe[16]=   {0x08,0x1C,0x0C,0x06,0xFF,0xFF,0x63,0x63,0x63,0x63,0x3F,0x1F,0x03,0x03,0x03,0x00};//pe in hindi
unsigned char data_num_ptro[16]=  {0x01,0x03,0x01,0x00,0xFF,0xFF,0x0C,0x3C,0x7C,0xC0,0xC2,0xFF,0x7C,0x18,0x3c,0x66};//tro in hindi
unsigned char data_num_pvl[16]=   {0x08,0x1C,0x0C,0x06,0xFF,0xFF,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x00};///mh in hindi
unsigned char data_num_ll2[16]=   {0x00,0x00,0x00,0x00,0xFF,0xFF,0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x00,0x00};///l2 in hindi
unsigned char data_num_ll1[16]=   {0x00,0x00,0x00,0x00,0xFF,0xFF,0x03,0x03,0xEF,0xFF,0x9B,0x93,0x83,0xC3,0xC3,0x00};///ll in hindi
//**********************************related with hindi diesel **************************************************** 
unsigned char data_num_dde1[16]=  {0x06,0x0F,0x0D,0x0C,0xFF,0xFF,0x0C,0x7C,0xFC,0xC0,0xFC,0x7E,0x86,0xC6,0x7C,0x38};//DE in hindi
unsigned char data_num_dde2[16]=  {0x00,0x00,0x00,0x00,0xFF,0xFF,0x00,0x00,0x00,0x00,0x02,0x03,0x01,0x00,0x00,0x00};//DE in hindi
unsigned char data_num_dvl[16]=   {0x00,0x00,0x02,0x03,0xFF,0xFF,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x00};///mh in hindi
unsigned char data_num_dje2[16]=  {0x00,0x00,0x00,0x00,0xFF,0xFF,0x00,0x00,0x00,0x00,0x02,0x03,0x01,0x00,0x00,0x00};//JE in hindi
unsigned char data_num_dje1[16]=  {0x00,0x00,0x00,0x00,0xFF,0xFF,0x03,0x03,0x7F,0x7F,0x33,0x1B,0x9B,0xFB,0x73,0x00};//JE in hindi

//**********************************related with hindi speed**************************************************** 
unsigned char data_num_sse[16]=   {0x00,0x00,0x00,0x00,0xFF,0xFF,0x33,0x33,0x33,0xBF,0xFF,0x63,0x33,0x1B,0x0B,0x00};//SE in hindi
unsigned char data_num_spe[16]=   {0x01,0x03,0x03,0x03,0xFF,0xFF,0x63,0x63,0x63,0x63,0x3F,0x1F,0x03,0x03,0x03,0x00};//pe in hindI
unsigned char data_num_svl[16]=   {0x18,0x1C,0x06,0x13,0xFF,0xFF,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x00};///mh in hindi
unsigned char data_num_sde1[16]=  {0x00,0x00,0x00,0x00,0xFF,0xFF,0x0C,0x7C,0xFC,0xC0,0xFC,0x7E,0x86,0xC6,0x7C,0x38};//DI in hindi
unsigned char data_num_sde2[16]=  {0x00,0x00,0x00,0x00,0xFF,0xFF,0x00,0x00,0x00,0x00,0x02,0x03,0x01,0x00,0x00,0x00};//DI in hindi
//**********************************related with hindi hispeed**************************************************** 
unsigned char data_num_hhe[16]=   {0x00,0x00,0x00,0x00,0xFF,0xFF,0x0C,0x7C,0xFC,0xC0,0xFC,0x7E,0xC6,0xC4,0xE0,0x70};//he in hindi
unsigned char data_num_hvl[16]=   {0x00,0x00,0x00,0x00,0xFF,0xFF,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x00};///mh in hindi
unsigned char data_num_hye[16]=   {0x00,0x00,0x00,0x00,0xFF,0xFF,0x73,0x3B,0x1B,0x1B,0xF3,0xE3,0x7F,0x3F,0x03,0x00};//ye in hindi
//**********************************related with dash for hi speed**************************************************** 
unsigned char data_num_hdash[16]= {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x1E,0x1E,0x00,0x00,0x00,0x00,0x00,0x00};//'-'
//**********************************related with vertical line for hi speed****************************************************
unsigned char data_num_lgap[16]=  {0x00,0x40,0x40,0x40,0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};///
//**********************************related with an  for CNG and LPG****************************************************
unsigned char data_num_aa[16]=    {0x00,0x00,0x00,0x00,0xFF,0xFF,0x63,0x63,0x63,0x62,0x64,0x30,0x18,0x0C,0x06,0x00};///
unsigned char data_num_nn[16]=    {0x00,0x00,0x00,0x00,0xFF,0xFF,0x03,0x03,0x3F,0x7F,0x53,0x53,0x23,0x03,0x03,0x00};///

//**********************************related with Tamil diesel **************************************************** 
unsigned char data_num_des1[16]=  {0x00,0x00,0x00,0xC0,0xC0,0xC0,0xC1,0xC7,0xC9,0xC8,0xD8,0xFF,0x7F,0x00,0x00,0x00};
unsigned char data_num_des2[16]=  {0x00,0x00,0x00,0x03,0x05,0x06,0xC6,0x4F,0xCF,0x0C,0x0C,0xCB,0xC7,0x00,0x00,0x00};
unsigned char data_num_des3[16]=  {0x00,0x00,0x00,0xF8,0xF8,0x60,0x60,0xF8,0xF8,0x60,0x60,0xE0,0xC0,0x00,0x00,0x00};
unsigned char data_num_des4[16]=  {0x00,0x00,0x00,0x00,0x1F,0x3F,0x60,0x7C,0x7E,0x66,0x66,0x7E,0x3C,0x00,0x00,0x00};
unsigned char data_num_des5[16]=  {0x00,0x00,0x30,0x30,0x07,0x81,0xC1,0xC1,0xC1,0xC1,0xE3,0x7F,0x3E,0x00,0x00,0x00};
unsigned char data_num_des6[16]=  {0x00,0x00,0x00,0x00,0x00,0x80,0x80,0x80,0x80,0x80,0x80,0x00,0x00,0x00,0x00,0x00};
//**********************************related with Tamil petrol **************************************************** 
unsigned char data_num_pete1[16]=  {0x00,0x00,0x1F,0x7F,0xE1,0xC1,0xC1,0xDD,0x75,0x3D,0x00,0x00,0x00,0x00,0x00,0x00};
unsigned char data_num_pete2[16]=  {0x00,0x00,0x00,0x80,0x80,0xB3,0xB3,0xB3,0xBF,0xBF,0x00,0x00,0x00,0x00,0x00,0x00};
unsigned char data_num_pete3[16]=  {0x00,0x00,0x0C,0x0C,0x00,0x60,0x60,0x60,0x7E,0x7E,0x00,0x00,0x00,0x00,0x00,0x00};
unsigned char data_num_pete4[16]=  {0x00,0x00,0x00,0x3C,0xF5,0xDD,0xC1,0xDD,0xF5,0x3D,0x00,0x00,0x00,0x00,0x00,0x00};
unsigned char data_num_pete5[16]=  {0x00,0x00,0x00,0x00,0xFD,0xFD,0x99,0x99,0x99,0x99,0x30,0x60,0xC0,0x00,0x00,0x00};
unsigned char data_num_pete6[16]=  {0x00,0x00,0x00,0x00,0xF9,0xFB,0xB3,0xB3,0xB2,0xB3,0x00,0x00,0x00,0x00,0x00,0x00};
unsigned char data_num_pete7[16]=  {0x00,0x30,0x30,0x00,0xC6,0xE7,0x33,0xB3,0xBF,0x9E,0x00,0x00,0x00,0x00,0x00,0x00};

//**********************************related with Tamil Speed **************************************************** 
unsigned char data_num_speed1[16]=  {0x00,0x00,0x00,0x00,0x3F,0x7F,0x60,0x60,0x60,0x7E,0x7E,0x76,0x3E,0x1C,0x00,0x00};
unsigned char data_num_speed2[16]=  {0x00,0xC0,0xC0,0x00,0xBC,0xFC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCF,0xC7,0x00,0x00};
unsigned char data_num_speed3[16]=  {0x00,0x00,0x00,0x00,0xE6,0xE6,0x66,0x66,0x66,0x66,0x66,0x66,0xC7,0x87,0x00,0x00};
unsigned char data_num_speed4[16]=  {0x00,0x7E,0x4A,0x46,0x71,0x31,0x31,0x31,0x31,0x31,0x31,0x31,0xF1,0xF1,0x00,0x00};
unsigned char data_num_speed5[16]=  {0x00,0x18,0x18,0x00,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0xFF,0xFF,0x00,0x00};

//**********************************related with Tamil Hi-Speed **************************************************** 
unsigned char data_num_hispeed1[16]=  {0x00,0x00,0x00,0x66,0x66,0x66,0x66,0x7E,0x7E,0x66,0x66,0x66,0x66,0x66,0x00,0x00};
unsigned char data_num_hispeed2[16]=  {0x00,0x60,0x60,0x00,0x00,0x60,0x60,0x60,0x6C,0x60,0x60,0x60,0x60,0x60,0x00,0x00};

//**********************************related with Tamil CNG **************************************************** 
unsigned char data_num_cng1[16]=  {0x00,0x03,0x04,0x08,0x08,0x7F,0x48,0x48,0x48,0x48,0x7F,0x88,0x88,0x88,0x48,0x30};
unsigned char data_num_cng2[16]=  {0x00,0x80,0x40,0x20,0x20,0x21,0x22,0x24,0x28,0x28,0x28,0x2E,0x29,0x29,0x26,0x20};
unsigned char data_num_cng3[16]=  {0x00,0x00,0x00,0x00,0xF1,0x22,0x24,0x28,0x28,0x28,0x28,0x2E,0x29,0x29,0x26,0x20};
unsigned char data_num_cng4[16]=  {0x60,0x60,0x00,0x00,0x8F,0x52,0x22,0x32,0x4A,0x4A,0x4A,0x4A,0x4A,0x4A,0x32,0x00};
unsigned char data_num_cng5[16]=  {0x00,0x00,0x00,0x18,0x25,0x42,0x72,0x4A,0x4A,0x32,0x00,0x1F,0x20,0x40,0x41,0x3E};
unsigned char data_num_cng6[16]=  {0x1C,0x22,0x41,0xC1,0x21,0x11,0x11,0x11,0x21,0x41,0x81,0x38,0x64,0xA4,0x18,0x00};
//**********************************related with Tamil LPG **************************************************** 
unsigned char data_num_lpg1[16]=  {0x00,0x00,0x00,0x00,0xFF,0xFF,0xC0,0xC0,0xC0,0xDE,0xF6,0xE2,0xF6,0xFE,0x00,0x00};
unsigned char data_num_lpg2[16]=  {0x00,0x00,0x00,0x00,0xF1,0xF3,0xE7,0xE6,0xE6,0xE7,0xE7,0xE6,0xE3,0xE1,0x00,0x00};
unsigned char data_num_lpg3[16]=  {0x00,0x03,0x03,0x00,0xF8,0xFC,0x0C,0x0C,0x0C,0xCC,0x6C,0x26,0x67,0xE3,0x00,0x00};
unsigned char data_num_lpg4[16]=  {0x00,0x00,0x00,0x00,0xE0,0x30,0x30,0x36,0x36,0x30,0x30,0x70,0xF0,0xE0,0x00,0x00};
unsigned char data_num_lpg5[16]=  {0x00,0x0F,0x09,0x08,0xCE,0xCE,0xC6,0xC6,0xC6,0xC6,0xC6,0xC6,0xFE,0xFE,0x00,0x00};
unsigned char data_num_lpg6[16]=  {0x00,0xC0,0x40,0xCF,0x1F,0x1B,0x1F,0xCE,0xC0,0x0F,0x18,0x18,0x1F,0x0F,0x00,0x00};
unsigned char data_num_lpg7[16]=  {0x00,0x3F,0x63,0xF8,0xFC,0x66,0x66,0x06,0x7E,0xC0,0x1E,0xF2,0x82,0xFC,0x00,0x00};
//*****************************big numbers**********************************************************************
unsigned char data_num_big_9[96]={0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                                  0xF8,0xFF,0x7F,0x3F,0x00,0x00,0x00,0xF0,0xF0,0xF0,0xFC,0x7F,0x7F,0x1F,0x00,0x00,                                                                    
                                  0x00,0x00,0x0F,0x3F,0x7F,0xF8,0xF0,0xF0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xF0,0xF0,                                                                    
                                  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
                                  0x7F,0xFF,0xDF,0x9E,0x1E,0x1E,0x1E,0x1E,0x3C,0x3C,0xF8,0xF8,0xF0,0xC0,0x00,0x00,                                                                    
                                  0x00,0x00,0xE0,0xF0,0xF8,0x7C,0x3C,0x3E,0x1E,0x1E,0x1E,0x1F,0x1F,0x1F,0x3F,0x3F};//9

unsigned char data_num_big_8[96]={0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                                  0x7F,0xF8,0xF0,0xF0,0xE0,0xE0,0xE0,0xF0,0xF0,0xF8,0xFC,0x7F,0x3F,0x0F,0x00,0x00,                                                                    
                                  0x00,0x00,0x0F,0x3F,0x7F,0x7C,0xF8,0xF0,0xF0,0xF0,0xF0,0xF0,0x78,0x3F,0x1F,0x7F,                                                                    
                                  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                                  0xFC,0x3E,0x1E,0x1F,0x0F,0x0F,0x0F,0x1F,0x1F,0x3E,0x7E,0xFC,0xF8,0xE0,0x00,0x00,                                                                    
                                  0x00,0x00,0xE0,0xF8,0xFC,0x7C,0x3E,0x1E,0x1E,0x1E,0x1E,0x1E,0x3C,0xF8,0xF0,0xF8};//8

unsigned char data_num_big_7[96]={0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                                  0x03,0x07,0x07,0x0F,0x0F,0x0F,0x1F,0x1F,0x1F,0x1E,0x1E,0x3E,0x3E,0x3E,0x00,0x00,                                                                    
                                  0x00,0x00,0xFF,0xFF,0xFF,0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x03,0x03,                                                                    
                                  0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                                  0xC0,0xC0,0x80,0x80,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,                                                                  
                                  0x00,0x00,0xFF,0xFF,0xFF,0xFF,0xFE,0x3E,0x3C,0x78,0x78,0xF0,0xF0,0xE0,0xE0,0xC0};//7


unsigned char data_num_big_6[96]={0x01,0x01,0x01,0x01,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                                  0xF8,0xF0,0xF0,0xF0,0xF0,0xF0,0xF0,0xF0,0xF8,0xF8,0x7C,0x7F,0x3F,0x0F,0x00,0x00,                                                                    
                                  0x00,0x00,0x07,0x1F,0x3F,0x7C,0x78,0xF0,0xF0,0xF0,0xF0,0xF0,0xF3,0xFF,0xFF,0xFC,                                                                    
                                  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,
                                  0x3E,0x1F,0x1F,0x1F,0x1F,0x1F,0x1F,0x1F,0x1E,0x3E,0x7E,0xFC,0xF8,0xC0,0x00,0x00,                                                                  
                                  0x00,0x00,0xF0,0xF8,0xFC,0x3E,0x3E,0x1F,0x1F,0x00,0x00,0x00,0xF0,0xFC,0xFC,0x7E};//6
                                  
unsigned char data_num_big_5[96]={0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x00,
                                  0xF0,0x00,0x00,0x00,0x00,0x00,0xE0,0xE0,0xF0,0xF0,0xFC,0xFF,0x7F,0x1F,0x00,0x00,                                                                    
                                  0x00,0x00,0xFF,0xFF,0xFF,0xFF,0xF0,0xF0,0xF0,0xF0,0xF0,0xF7,0xFF,0xFF,0xFF,0xF0,                                                                   
                                  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                                  0x3E,0x3E,0x1F,0x1F,0x1F,0x1F,0x1F,0x3E,0x3E,0x7C,0xFC,0xF8,0xF0,0xC0,0x00,0x00,                                                                 
                                  0x00,0x00,0xFE,0xFE,0xFE,0xFE,0x00,0x00,0x00,0x00,0x00,0xC0,0xF0,0xF8,0xF8,0x7C};//5

unsigned char data_num_big_4[96]={0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                                  0xF0,0xE0,0xE0,0xFF,0xFF,0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,                                                                    
                                  0x00,0x00,0x00,0x00,0x01,0x01,0x03,0x03,0x07,0x0F,0x0E,0x1E,0x1C,0x3C,0x78,0x78,                                                                    
                                  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                                  0x78,0x78,0x78,0xFF,0xFF,0xFF,0xFF,0x78,0x78,0x78,0x78,0x78,0x78,0x78,0x00,0x00,                                                                    
                                  0x00,0x00,0xF8,0xF8,0xF8,0xF8,0xF8,0xF8,0xF8,0x78,0x78,0x78,0x78,0x78,0x78,0x78};//4

unsigned char data_num_big_3[96]={0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                                  0x07,0x00,0x00,0x00,0x00,0x00,0xF0,0xF0,0xF0,0xF0,0xF8,0x7F,0x3F,0x1F,0x00,0x00,                                                                    
                                  0x00,0x00,0x03,0x1F,0x3F,0x7F,0x78,0xF0,0xF0,0xF0,0x00,0x00,0x00,0x00,0x07,0x07,                                                                    
                                  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
                                  0xFC,0x7E,0x1E,0x1F,0x0F,0x0F,0x0F,0x0F,0x1F,0x1E,0x3E,0xFC,0xF8,0xF0,0x00,0x00,                                                                    
                                  0x00,0x00,0xC0,0xF8,0xFC,0xFC,0x3E,0x1E,0x1E,0x1E,0x1E,0x1E,0x3C,0x7C,0xF8,0xE0};//3

unsigned char data_num_big_2[96]={0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x00,0x00,//ROW1
                                  0x01,0x03,0x07,0x0F,0x1F,0x3E,0x7C,0xF8,0xF8,0xF8,0xFF,0xFF,0xFF,0xFF,0x00,0x00,//ROW2                                                                    
                                  0x00,0x00,0x07,0x3F,0x7F,0xFF,0xF8,0xF0,0xE0,0xE0,0xE0,0x00,0x00,0x00,0x00,0x00,//ROW3                                                                   
                                  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x00,0x00,0x00,0x00,0x00,//ROW4
                                  0xF0,0xE0,0xC0,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0xFF,0xFF,0xFF,0x00,0x00,//ROW5                                                                    
                                  0x00,0x00,0xC0,0xF0,0xF8,0xFC,0x3C,0x3E,0x1F,0x1F,0x1F,0x1E,0x3E,0x3C,0x7C,0xF8};//2//ROW6
                                  
unsigned char data_num_big_1[96]={0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,//ROW1
                                  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,//ROW2                                                                    
                                  0x00,0x00,0x00,0x00,0x00,0x01,0x07,0x07,0x07,0x07,0x00,0x00,0x00,0x00,0x00,0x00,//ROW3                                                                    
                                  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,// ROW4
                                  0x70,0x70,0x70,0x70,0x70,0x70,0x70,0x70,0x70,0x70,0x70,0x70,0x70,0x70,0x00,0x00,//ROW5                                                                    
                                  0x00,0x00,0x70,0x70,0xF0,0xF0,0xF0,0xF0,0xF0,0xF0,0x70,0x70,0x70,0x70,0x70,0x70};//1//ROW6

unsigned char data_num_big_0[96]={0x01,0x01,0x01,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x0,//ROW1
                                  0xF0,0xF0,0xF0,0xF0,0xF0,0xF0,0xF0,0xF8,0xF8,0x7C,0x7F,0x3F,0x1F,0x0F,0x00,0x00,//ROW2                                                                    
                                  0x00,0x00,0x0F,0x1F,0x3F,0x7C,0x78,0xF8,0xF0,0xF0,0xF0,0xF0,0xF0,0xF0,0xF0,0xF0,//ROW3                                                                    
                                  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,// ROW4
                                  0x1F,0x1F,0x1F,0x1F,0x1F,0x1E,0x1E,0x3E,0x3E,0x7C,0xFC,0xF8,0xF0,0xE0,0x00,0x00,//ROW5                                                                    
                                  0x00,0x00,0xE0,0xF0,0xF8,0x7C,0x3C,0x3E,0x1E,0x1E,0x1E,0x1F,0x1F,0x1F,0x1F,0x1F};//1//ROW6
unsigned char data_num_big_gapp[96]={0};

//*******************************medium numbers*******************************************************************
unsigned char data_num_med_9[16]=  {0x00,0x00,0x3E,0x77,0x63,0x63,0x63,0x63,0x3F,0x1B,0x03,0x63,0x76,0x3C,0x00,0x00};//9
unsigned char data_num_med_8[16]=  {0x00,0x00,0x1C,0x36,0x63,0x63,0x22,0x1C,0x36,0x63,0x63,0x63,0x36,0x1C,0x00,0x00};//8
unsigned char data_num_med_7[16]=  {0x00,0x00,0x7F,0x7F,0x02,0x06,0x04,0x0C,0x08,0x18,0x18,0x18,0x30,0x30,0x00,0x00};//7 
unsigned char data_num_med_6[16]=  {0x00,0x00,0x1E,0x3F,0x63,0x60,0x6C,0x7E,0x63,0x63,0x63,0x63,0x36,0x3E,0x00,0x00};//6
unsigned char data_num_med_5[16]=  {0x00,0x00,0x7F,0x7F,0x60,0x60,0x6E,0x7E,0x63,0x03,0x03,0x63,0x77,0x3E,0x00,0x00};//5
unsigned char data_num_med_4[16]=  {0x00,0x00,0x06,0x0E,0x1E,0x1E,0x36,0x66,0x46,0x7F,0x7F,0x06,0x06,0x06,0x00,0x00};//4                                  
unsigned char data_num_med_3[16]=  {0x00,0x00,0x1C,0x3E,0x63,0x63,0x06,0x1C,0x1E,0x03,0x03,0x63,0x77,0x3E,0x00,0x00};//3
unsigned char data_num_med_2[16]=  {0x00,0x00,0x3E,0x77,0x63,0x63,0x03,0x06,0x0E,0x1C,0x30,0x70,0x7F,0x7F,0x00,0x00};//2                                  
unsigned char data_num_med_1[16]=  {0x00,0x00,0x0c,0x1c,0x3c,0x0c,0x0c,0x0c,0x0c,0x0c,0x0c,0x0c,0x0c,0x0c,0x00,0x00};//1
unsigned char data_num_med_0[16]=  {0x00,0x00,0x1C,0x36,0x63,0x63,0x63,0x63,0x63,0x63,0x63,0x63,0x36,0x1C,0x00,0x00};//0
//*******************************DOTS*******************************************************************
unsigned char data_num_med_dot[16]={0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xC0,0xC0,0x00,0x00,0x00,0x00};//dot
//*******************************gap for medium numbers*******************************************************************
unsigned char data_num_med_gapp[16]={0};//gapp
//****************************used for vertical scrolling****************************************************************
unsigned char vertical_change=0,vertical_change2=0,vertical_change3=0,vertical_change4=0;
unsigned char change_v[20]={0},change_v2[20]={0},change_v3[20]={0},change_v4[20]={0};
//****************************used for internal memory store for prodcut value****************************************************************
unsigned char store_count=0,prodcut_value=0,read_flag=0,erase_flag=0;
union product_store
{
 unsigned int store_int;
 unsigned char store_ch[2];
}product_info;
unsigned char memory_ch[3]={0};
 unsigned char read_ch=0;
//************************************************date save********************************************************
unsigned char date_save[6]={0};
unsigned char date1[16]={0},date2[16]={0},date3[16]={0};
unsigned char date4[16]={0},date5[16]={0},date6[16]={0};
//****************************used for first dsplay ****************************************************************
unsigned char data11[96]={0},data11_temp[96]={0};//1 5 9 13                         
unsigned char data12[96]={0},data12_temp[96]={0};//2 6 10 14                          
unsigned char data13[96]={0},data13_temp[96]={0};//3 7 11 15                          
unsigned char data14[96]={0},data14_temp[96]={0};// 4 8 12 16
//****************************used for second dsplay ****************************************************************
unsigned char data21[96]={0},data21_temp[96]={0};//1 5 9 13                         
unsigned char data22[96]={0},data22_temp[96]={0};//2 6 10 14                          
unsigned char data23[96]={0},data23_temp[96]={0};//3 7 11 15                          
unsigned char data24[96]={0},data24_temp[96]={0};// 4 8 12 16
//****************************used for third dsplay ****************************************************************
unsigned char data31[96]={0},data31_temp[96]={0};//1 5 9 13                         
unsigned char data32[96]={0},data32_temp[96]={0};//2 6 10 14                          
unsigned char data33[96]={0},data33_temp[96]={0};//3 7 11 15                          
unsigned char data34[96]={0},data34_temp[96]={0};// 4 8 12 16
//****************************used for forth dsplay ****************************************************************
unsigned char data41[96]={0},data41_temp[96]={0};//1 5 9 13                         
unsigned char data42[96]={0},data42_temp[96]={0};//2 6 10 14                          
unsigned char data43[96]={0},data43_temp[96]={0};//3 7 11 15                          
unsigned char data44[96]={0},data44_temp[96]={0};// 4 8 12 16
unsigned long counter_change=0,counter_change2=0;
unsigned long counter_change3=0,counter_change4=0;
unsigned long counter_shift=0,i=0,display_time1=0,display_time_m=0;
unsigned long display_time2=0,display_time3=0,display_time4=0,m1=0;
unsigned char u[16]={0};
//*****************************serial communication *****************************************************************
String  recieved_data;
unsigned char recieve=0,data_serial_recieved=0,rec_im=0;
unsigned char data_num_med_rec1[16]={0},data_num_med_rec2[16]={0};
unsigned char data_num_big_rec1[96]={0},data_num_big_rec2[96]={0},data_num_big_rec3[96]={0};
unsigned char id_01_rec[7]={0},id_02_rec[7]={0},id_03_rec[7]={0};
unsigned char id_04_rec[7]={0},id_05_rec[7]={0},id_06_rec[7]={0},id_07_rec[7]={0},roll_change_data=0,brightness[4]={0};
unsigned char dealer[7]={0};
unsigned int   brightness_value=0;
//*******************************************************************************************************************
unsigned char seg_no1=0,data_c1=0,data_c2=0,data_temp[16]={0};
unsigned int im=0;
unsigned long bright_v=0,bright_f=0;
//****************************************delaler code************************************************************
unsigned char dealer_vs=0,dealer_v[2]={0};
unsigned char data_dealer1[16]={0},data_dealer2[16]={0},data_dealer3[16]={0};
unsigned char data_dealer4[16]={0},data_dealer5[16]={0},data_dealer6[16]={0};
//**************************used for shifter register****************************************************************
unsigned int m=0,n=0,data11_shift=0,data21_shift=0,data31_shift=0,data41_shift=0,j=0;
unsigned char cm=0,display_init1=0,display_init2=0,display_init3=0,display_init_final=0;
//***************************horizontal scrolling *******************************************************************
unsigned char  zm1=0,zm=0,jm=0;
unsigned int change_message=0;
unsigned char message_flag=0;
unsigned char message_store[200]={0},message_store_count=0;
unsigned char message[3520]={0};
unsigned char data_temp_m[16]={0},data_temp_m1[16]={0},data_temp_m2[16]={0},data_temp_m3[16]={0},data_temp_m4[16]={0};
unsigned char data_temp_m5[16]={0},data_temp_m6[16]={0},data_temp_m7[16]={0},data_temp_m8[16]={0},data_temp_m9[16]={0},data_temp_m10[16]={0};
unsigned char data_temp_m11[16]={0},data_temp_m12[16]={0},data_temp_m13[16]={0},data_temp_m14[16]={0},data_temp_m15[16]={0};
//***********************************function for horizontal scrolling promotion al message********************************************
void message_display(void);//message array
void mes_rol(void);
void clear_horizontal_data(void);
//*********************************clear all shifter register data and store product****************************************************
void clear_fun(void);
void memory_store_product(unsigned char *store_array,unsigned char prodcut);
void memory_read(void);
void memory_erase(void);
//*********************************given data to shift register****************************************************
void use(unsigned char *data1_shift,unsigned char *data2_shift,unsigned char *data3_shift,unsigned char *data4_shift);
void shift_func(void);
//*********************************horizontal message scrolling****************************************************


//*********************************id data converter****************************************************
void id1_data(void);//petrol
void id2_data(void);//diesel
void id3_data(void);//speed
void id4_data(void);//hi-speed
void id5_data(void);//lpg
void id6_data(void);//cng
void find_rolling_function(void);


//*********************************display big, medium and character with vertical scrolling**********************
void selecting_num_big(unsigned char num_sel,unsigned char *data_num_big_rec);
void selecting_num_med(unsigned char num_sel,unsigned char *data_num_med_rec); 
void data_display(unsigned char *data1_temp,unsigned char *data2_temp,unsigned char *data3_temp,unsigned char *data4_temp,unsigned char p10_no,unsigned char pt_di,unsigned char *data_fi,unsigned char *data_se,unsigned char *data_th,unsigned char *data_fo,unsigned char *data_fv,unsigned char *data_si);
void rolling_vertical(unsigned char *data_change,unsigned char *data_change1,unsigned char p10_no,unsigned char vertical_temp);
void func_verti(unsigned char p10_no,unsigned char vertical_temp);
void rolling_vertical_product(unsigned char *data_change,unsigned char *data_change1,unsigned char p10_no,unsigned char vertical_temp);
void func_verti_product(unsigned char p10_no,unsigned char vertical_temp);
void func_verti_line1(void);
void func_verti_line1_product(void);
void func_verti_line2(void);
void func_verti_line2_product(void);
void func_verti_line3(void);
void func_verti_line3_product(void);
//********************selection of numbers,date and dealer code display*******************************************
void data_display_diesel(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1);
void data_display_petrol(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1);
void data_display_speed(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1);
void data_display_HIspeed(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1);
void data_display_LPG(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1);
void data_display_CNG(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1);
void data_display_petrol_hindi(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1);
void data_display_diesel_hindi(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1);
void data_display_speed_hindi(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1);
void data_display_hispeed_hindi(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1);
void data_display_LPG_hindi(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1);
void data_display_CNG_hindi(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1);
//*********************************dealer code*********************************************
void dealer_date(void);
void dealer_code(unsigned char *data1_temp,unsigned char *data2_temp,unsigned char *data3_temp,unsigned char *data4_temp,unsigned char p10_no,unsigned char *data_fi,unsigned char *data_se,unsigned char *data_th,unsigned char *data_fo,unsigned char *data_fv,unsigned char *data_si);
//*****************reieve data serially and save in memory*********************************
void save_serial_data(void);
//********************wifi initialization**************************************************
void wifi_init(void);
//*********************display on 3 board**************************************************
void display_set1(void);
void display_set1_verti(void);
void display_set2(void);
void display_set2_verti(void);
void display_set3(void);
void display_set3_verti(void);
void main_display(void);
//************************************setup**********************************************************************
void setup()
{
 //***********************pin selection for shifter register*************************************************** 
  pinMode(SECLK, OUTPUT);
  pinMode(LATCH, OUTPUT);
  pinMode(SERDS1, OUTPUT);
  pinMode(SERDS2, OUTPUT);
  pinMode(SERDS3, OUTPUT);
  pinMode(RS485, OUTPUT);
  pinMode(A, OUTPUT);
  pinMode(B, OUTPUT);
  pinMode(OE, OUTPUT);
  Serial.begin(9600);//initia;ize serial 
//**************************************internal eeprom and serial intialization**********************************
  EEPROM.begin(512);  //Initialize EEPROM
  httpUpdater.setup(&server, OTAPATH, OTAUSER, OTAPASSWORD);
  server.onNotFound(handleNotFound);
  server.begin();
  wifi_init();
  
//******************************read internal eeprom **********************************************************

 for(rec_im=0;rec_im<7;rec_im++)// aal id's values
 {
    id_01_rec[rec_im]=EEPROM.read(rec_im);
    id_02_rec[rec_im]=EEPROM.read(rec_im+7);
    id_03_rec[rec_im]=EEPROM.read(rec_im+14);
    id_04_rec[rec_im]=EEPROM.read(rec_im+21);
    id_05_rec[rec_im]=EEPROM.read(rec_im+28);
    id_06_rec[rec_im]=EEPROM.read(rec_im+35);    
 }
 //******************************brightness********************************************************************** 
 for(rec_im=0;rec_im<5;rec_im++)
    brightness[rec_im]=EEPROM.read(rec_im+50);

 if(brightness[0]=='h' && brightness[1]=='i' )
 {
  bright_v=bright_high;
 // bright_f=bright_f_high;
  //analogWriteFreq(bright_f);
 }
 else
 {
  bright_v=bright_low ;
  //bright_f=bright_f_low; 
  //analogWriteFreq(bright_f);
 }
 //******************************dealer code********************************************************************** 
  for(rec_im=0;rec_im<6;rec_im++)
   dealer[rec_im]=EEPROM.read(rec_im+60);
   
  for(rec_im=0;rec_im<2;rec_im++)
   dealer_v[rec_im]=EEPROM.read(rec_im+68);
   
 if(dealer_v[0]=='h' && dealer_v[1]=='i' )
  dealer_vs=1;
 else
  dealer_vs=0;
//******************************date save********************************************************************** 
 for(rec_im=0;rec_im<6;rec_im++)
    date_save[rec_im]=EEPROM.read(rec_im+80);
//******************************promotional message**************************************************************
 message_flag=EEPROM.read(87);
 message_store_count=EEPROM.read(89);
 for(rec_im=0;rec_im<200;rec_im++)
 {
  message_store[rec_im]=EEPROM.read(rec_im+100);
  Serial.println(message_store[rec_im]);
 }
//******************************RELATED WITH MEMORY**************************************************************
 store_count=EEPROM.read(302);
 Serial.println(store_count);
  digitalWrite(RS485,LOW);
 
  
}

 void handleNotFound() {
  server.send(404, "text/plain", "404: Not found");
}

// the loop routine runs over and over again forever:
void loop() 
{
 find_rolling_function();
 server.handleClient();
 //WiFiClient client = server.available();

//*****************read serial data******************************************************************************
 if (Serial.available()) 
 {  
  save_serial_data();
  if(read_flag==1)
  {
   
   digitalWrite(RS485,HIGH); 
   delay(1000);
   memory_read();
   read_flag=0;
   
  }
  if(erase_flag==1)
  {
   memory_erase();
   erase_flag=0;
  }
  digitalWrite(RS485,LOW);
 } 


  display_time_m++; 

main_display();
//************SENDING DATA TO SHIFT REGISTER***************************************************************
 shift_func();
}//end loop 


//*********************************id data converter****************************************************
void id1_data(void)//petrol
{
 if(id_01_rec[0]==1)
  selecting_num_big(id_01_rec[0],data_num_big_rec1);
 else
  selecting_num_big(10,data_num_big_rec1);
 selecting_num_big(id_01_rec[1],data_num_big_rec2);
 selecting_num_big(id_01_rec[2],data_num_big_rec3);
 selecting_num_med(id_01_rec[3],data_num_med_rec1);
 selecting_num_med(id_01_rec[4],data_num_med_rec2); 
}
void id2_data(void)//diesel
{
 if(id_02_rec[0]==1)
  selecting_num_big(id_02_rec[0],data_num_big_rec1);
 else
   selecting_num_big(10,data_num_big_rec1);
 selecting_num_big(id_02_rec[1],data_num_big_rec2);
 selecting_num_big(id_02_rec[2],data_num_big_rec3);
 selecting_num_med(id_02_rec[3],data_num_med_rec1);
 selecting_num_med(id_02_rec[4],data_num_med_rec2);  
}
void id3_data(void)//speed
{
 if(id_03_rec[0]==1)
  selecting_num_big(id_03_rec[0],data_num_big_rec1);
 else
  selecting_num_big(10,data_num_big_rec1);
 selecting_num_big(id_03_rec[1],data_num_big_rec2);
 selecting_num_big(id_03_rec[2],data_num_big_rec3);
 selecting_num_med(id_03_rec[3],data_num_med_rec1);
 selecting_num_med(id_03_rec[4],data_num_med_rec2);
}
void id4_data(void)//hi-speed
{
 if(id_04_rec[0]==1)
  selecting_num_big(id_04_rec[0],data_num_big_rec1);
 else
  selecting_num_big(10,data_num_big_rec1);
 selecting_num_big(id_04_rec[1],data_num_big_rec2);
 selecting_num_big(id_04_rec[2],data_num_big_rec3);
 selecting_num_med(id_04_rec[3],data_num_med_rec1);
 selecting_num_med(id_04_rec[4],data_num_med_rec2);  
}
void id5_data(void)//lpg
{
  if(id_05_rec[0]==1)
  selecting_num_big(id_05_rec[0],data_num_big_rec1);
 else
  selecting_num_big(10,data_num_big_rec1);
 selecting_num_big(id_05_rec[1],data_num_big_rec2);
 selecting_num_big(id_05_rec[2],data_num_big_rec3);
 selecting_num_med(id_05_rec[3],data_num_med_rec1);
 selecting_num_med(id_05_rec[4],data_num_med_rec2); 
}
void id6_data(void)//cng
{
 if(id_06_rec[0]==1)
  selecting_num_big(id_06_rec[0],data_num_big_rec1);
 else
  selecting_num_big(10,data_num_big_rec1);
 selecting_num_big(id_06_rec[1],data_num_big_rec2);
 selecting_num_big(id_06_rec[2],data_num_big_rec3);
 selecting_num_med(id_06_rec[3],data_num_med_rec1);
 selecting_num_med(id_06_rec[4],data_num_med_rec2);  
}
//***********************************************function for vertical scrolling****************************************************
void func_verti_line1(void)//line1 english
{
 func_verti(0,vertical_change);
 rolling_vertical(data11,data11_temp,0,vertical_change); 
 rolling_vertical(data12,data12_temp,0,vertical_change);  
 rolling_vertical(data13,data13_temp,0,vertical_change); 
 rolling_vertical(data14,data14_temp,0,vertical_change);
}
void func_verti_line1_product(void)//line1 hindi
{
 func_verti_product(0,vertical_change);
 rolling_vertical_product(data11,data11_temp,0,vertical_change); 
 rolling_vertical_product(data12,data12_temp,0,vertical_change);  
 rolling_vertical_product(data13,data13_temp,0,vertical_change); 
 rolling_vertical_product(data14,data14_temp,0,vertical_change); 
}
void func_verti_line2(void)//line2 english
{
 func_verti(0,vertical_change);
 rolling_vertical(data21,data21_temp,0,vertical_change); 
 rolling_vertical(data22,data22_temp,0,vertical_change);  
 rolling_vertical(data23,data23_temp,0,vertical_change); 
 rolling_vertical(data24,data24_temp,0,vertical_change); 
}
void func_verti_line2_product(void)//line2 hindi
{
 func_verti_product(0,vertical_change);
 rolling_vertical_product(data21,data21_temp,0,vertical_change); 
 rolling_vertical_product(data22,data22_temp,0,vertical_change);  
 rolling_vertical_product(data23,data23_temp,0,vertical_change); 
 rolling_vertical_product(data24,data24_temp,0,vertical_change); 
}
void func_verti_line3(void)//line3 english
{
 func_verti(0,vertical_change);
 rolling_vertical(data31,data31_temp,0,vertical_change); 
 rolling_vertical(data32,data32_temp,0,vertical_change);  
 rolling_vertical(data33,data33_temp,0,vertical_change); 
 rolling_vertical(data34,data34_temp,0,vertical_change); 
}
void func_verti_line3_product(void)//line3 hindi
{
 func_verti_product(0,vertical_change);
 rolling_vertical_product(data31,data31_temp,0,vertical_change);
 rolling_vertical_product(data32,data32_temp,0,vertical_change);
 rolling_vertical_product(data33,data33_temp,0,vertical_change);
 rolling_vertical_product(data34,data34_temp,0,vertical_change); 
}

//*************************************************main display*********************************************************************
void main_display(void)
{
  if((display_time_m>=0)&&(display_time_m<(425+(message_store_count*25))))//promotion message
 {
   if(message_flag==1)
   {
    if(change_v[13]==0)
   {
      for(rec_im=0;rec_im<96;rec_im++)
     {
       data31[rec_im]=data21[rec_im]=data11[rec_im]=0;
       data32[rec_im]=data22[rec_im]=data12[rec_im]=0;
       data33[rec_im]=data23[rec_im]=data13[rec_im]=0;
       data34[rec_im]=data24[rec_im]=data14[rec_im]=0;
     }
      message_display();
     change_v[13]=1;
   
   } 
   if(change_message<(message_store_count+17)) 
    mes_rol();  
  }
  else
   display_time_m=(425+message_store_count*25); 
 }
   
  //****************************************************dealer code display********************************************************** 
 else if((display_time_m>=(425+message_store_count*25))&&(display_time_m<(1425+message_store_count*25)))//dealer code
 {
   if(dealer_vs==1)
   {
   if(change_v[0]==0)
   {
   
     display_time1=1;
     display_time2=1;
     display_time3=1;
    
     display_set1();
     display_set2();
     display_set3();
      
    change_v[0]=1;
    vertical_change=0;
    counter_change=0;
   } 
   counter_change++;
   if(vertical_change<8)
   { 
    if(counter_change>4)
    {
     counter_change=0;
      display_set1_verti();
      display_set2_verti();
      display_set3_verti();   
     vertical_change++;      
    }
   }
  }
  else
   display_time_m=(1425+message_store_count*25);
 } 
 
 //****************************************************petrol display************************************************************************  
 else if((display_time_m>=(1425+message_store_count*25))&&(display_time_m<(2425+message_store_count*25)))//petrol
 {
 if(display_init_final>=1)
  {
   if(change_v[1]==0)
   {
     display_time1++;
     display_time2++;
     display_time3++;
     display_set1();
     display_set2();
     display_set3();
      
    change_v[1]=1;
    vertical_change=0;
    counter_change=0;
   } 
   counter_change++;
   if(vertical_change<8)
   { 
    if(counter_change>4)
    {
     counter_change=0;
      display_set1_verti();
      display_set2_verti();
      display_set3_verti();   
     vertical_change++;      
    }
   }
  }
  else
   display_time_m=(2425+message_store_count*25);
 }
 //****************************************************petrol in hindi************************************************************************
 else if((display_time_m>=(2425+message_store_count*25))&&(display_time_m<(3425+message_store_count*25)))//
 {
   if(display_init_final>=1)
   {
   if(change_v[2]==0)
   {
     display_time1++;
     display_time2++;
     display_time3++;
     display_set1();
     display_set2();
     display_set3();
     
    change_v[2]=1;
    vertical_change=0;
    counter_change=0;
   } 
   counter_change++;
   if(vertical_change<8)
   { 
    if(counter_change>4)
    {
     counter_change=0;
     display_set1_verti();
     display_set2_verti();
     display_set3_verti();  
     vertical_change++;      
    }
    }
   }
   else
    display_time_m=(3425+message_store_count*25);
 }
//**************************************************diesel**********************************************************************************
 else if((display_time_m>=(3425+message_store_count*25))&&(display_time_m<(4425+message_store_count*25)))//diesel
 {
  
  if(display_init_final>=2)
  {
   if(change_v[3]==0)
   { 
     display_time1++;
     display_time2++;
     display_time3++;
     display_set1();
     display_set2();
     display_set3();
     
     vertical_change=0;
    counter_change=0;
    change_v[3]=1; 
   } 
   counter_change++;
   if(vertical_change<8)
   { 
    if(counter_change>4)
    {
     counter_change=0;
     display_set1_verti();
     display_set2_verti();
     display_set3_verti(); 
     vertical_change++;    
    } 
   }
  }
  else
  display_time_m=(4425+message_store_count*25);
 }
//**************************************************diesel in hindi**********************************************************************************
 else if((display_time_m>=(4425+message_store_count*25))&&(display_time_m<(5425+message_store_count*25)))//diesel
 {
 if(display_init_final>=2)
  { 
   if(change_v[4]==0)
   {
     display_time1++;
     display_time2++;
     display_time3++;
     display_set1();
     display_set2();
     display_set3();
     
     vertical_change=0;
    counter_change=0;
    change_v[4]=1; 
   } 
   counter_change++;
   if(vertical_change<8)
   { 
    if(counter_change>4)
    {
     counter_change=0;
    
     display_set1_verti();
     display_set2_verti();
     display_set3_verti();
     vertical_change++;    
    } 
   }
  }
  else
   display_time_m=(5425+message_store_count*25);
 } 
//**************************************************speed**********************************************************************************
 else if((display_time_m>=(5425+message_store_count*25))&&(display_time_m<(6425+message_store_count*25)))//speed
 {
 if(display_init_final>=3)
  {
   if(change_v[5]==0)
   { 
     display_time1++;
     display_time2++;
     display_time3++;
     display_set1();
     display_set2();
     display_set3();
    
     vertical_change=0;
    counter_change=0;
    change_v[5]=1;
   } 
   counter_change++;
   if(vertical_change<8)
   { 
    if(counter_change>4)
    {
     counter_change=0;
     
     display_set1_verti();
     display_set2_verti();
     display_set3_verti();
     vertical_change++;   
    }   
   }
  }
  else
  display_time_m=(6425+message_store_count*25);
 }
//**************************************************speed in hindi**********************************************************************************
 else if((display_time_m>=(6425+message_store_count*25))&&(display_time_m<(7425+message_store_count*25)))//speed
 {
 if(display_init_final>=3)
  {
   if(change_v[6]==0)
   {
     display_time1++;
     display_time2++;
     display_time3++;
     display_set1();
     display_set2();
     display_set3();
     
     vertical_change=0;
    counter_change=0;
    change_v[6]=1;
   }  
   counter_change++;
   if(vertical_change<8)
   { 
    if(counter_change>4)
    {
     counter_change=0;
    
     display_set1_verti();
     display_set2_verti();
     display_set3_verti();   
     vertical_change++;   
    } 
   }
  }
  else
   display_time_m=(7425+message_store_count*25);
 }
//**************************************************hi speed**********************************************************************************
 else if((display_time_m>=(7425+message_store_count*25))&&(display_time_m<(8425+message_store_count*25)))//hi speed
 {
   if(display_init_final>=4)
  {
   if(change_v[7]==0)
   { 
     display_time1++;
     display_time2++;
     display_time3++;
     display_set1();
     display_set2();
     display_set3();
    
     vertical_change=0;
    counter_change=0;
    change_v[7]=1;
   } 
   counter_change++;
   if(vertical_change<8)
   { 
    if(counter_change>4)
    {
     counter_change=0;
    
     display_set1_verti();
     display_set2_verti();
     display_set3_verti();  
     vertical_change++;   
    }   
   }
  }
  else
   display_time_m=(8425+message_store_count*25);
 }
//**************************************************hi speed in hindi**********************************************************************************
 else if((display_time_m>=(8425+message_store_count*25))&&(display_time_m<(9425+message_store_count*25)))//hi speed
 {
 if(display_init_final>=4)
  {
   if(change_v[8]==0)
   {
     display_time1++;
     display_time2++;
     display_time3++; 
     display_set1();
     display_set2();
     display_set3();
       
     vertical_change=0;
    counter_change=0;
    change_v[8]=1;
   } 
   counter_change++;
   if(vertical_change<8)
   { 
    if(counter_change>4)
    {
     counter_change=0;
    
     display_set1_verti();
     display_set2_verti();
     display_set3_verti();
     vertical_change++;   
    }  
   }
  }
  else
   display_time_m=(9425+message_store_count*25);
 }
//**************************************************LPG**********************************************************************************
 else if((display_time_m>=(9425+message_store_count*25))&&(display_time_m<(10425+message_store_count*25)))//LPG
 {
 if(display_init_final>=5)
  {
   if(change_v[9]==0)
   { 
     display_time1++;
     display_time2++;
     display_time3++; 
     display_set1();
     display_set2();
     display_set3();
      
     vertical_change=0;
    counter_change=0;
    change_v[9]=1;
   } 
   counter_change++;
   if(vertical_change<8)
   { 
    if(counter_change>4)
    {
     counter_change=0;
     
     display_set1_verti();
     display_set2_verti();
     display_set3_verti();  
     vertical_change++;   
    }   
   }
  }
  else
   display_time_m=(10425+message_store_count*25);
}
//**************************************************LPG in hindi **********************************************************************************
 else if((display_time_m>=(10425+message_store_count*25))&&(display_time_m<(11425+message_store_count*25)))//LPG
 {
  if(display_init_final>=5)
  {
   if(change_v[10]==0)
   { 
     display_time1++;
     display_time2++;
     display_time3++;
     display_set1();
     display_set2();
     display_set3();
        
     vertical_change=0;
    counter_change=0;
    change_v[10]=1;
   } 
   counter_change++;
   if(vertical_change<8)
   { 
    if(counter_change>4)
    {
     counter_change=0;
    
     display_set1_verti();
     display_set2_verti();
     display_set3_verti();  
     vertical_change++;   
    }   
   }
  }
  else
   display_time_m=(11425+message_store_count*25);
 }
 //**************************************************CNG**********************************************************************************
 else if((display_time_m>=(11425+message_store_count*25))&&(display_time_m<(12425+message_store_count*25)))//CNG
 {
  if(display_init_final>=6)
  {
   if(change_v[11]==0)
   { 
     display_time1++;
     display_time2++;
     display_time3++;
     display_set1();
     display_set2();
     display_set3();
       
     vertical_change=0;
    counter_change=0;
    change_v[11]=1;
   } 
   counter_change++;
   if(vertical_change<8)
   { 
    if(counter_change>4)
    {
     counter_change=0;
     
     display_set1_verti();
     display_set2_verti();
     display_set3_verti();  
     vertical_change++;   
    }   
   }
  }
  else
   display_time_m=(12425+message_store_count*25);
 }
//**************************************************CNG in hindi **********************************************************************************
 else if((display_time_m>=(12425+message_store_count*25))&&(display_time_m<(13425+message_store_count*25)))//CNG
 {
  if(display_init_final>=6)
  {
   if(change_v[12]==0)
   { 
     display_time1++;
     display_time2++;
     display_time3++;
     display_set1();
     display_set2();
     display_set3();
       
     vertical_change=0;
    counter_change=0;
    change_v[12]=1;
   } 
   counter_change++;
   if(vertical_change<8)
   { 
    if(counter_change>4)
    {
     counter_change=0;
     
     display_set1_verti();
     display_set2_verti();
     display_set3_verti();   
     vertical_change++;   
    }   
   }
  }
  else
   display_time_m=(13425+message_store_count*25);
}
//**************************************************end and clear all data of shift register********************************************
 else if(display_time_m>=(13425+message_store_count*25))
 {
  display_time_m=0;
  for(rec_im=0;rec_im<20;rec_im++)
   change_v[rec_im]=0;
  for(rec_im=0;rec_im<96;rec_im++)
     {
       data31[rec_im]=data21[rec_im]=data11[rec_im]=0;
       data32[rec_im]=data22[rec_im]=data12[rec_im]=0;
       data33[rec_im]=data23[rec_im]=data13[rec_im]=0;
       data34[rec_im]=data24[rec_im]=data14[rec_im]=0;
     }
   clear_horizontal_data();
  change_message=0;
 }
}
//***********************************rolling count******************************************************************************
void find_rolling_function(void)
{
  display_init1=0;
  display_init2=0;
  display_init3=0;
  display_init_final=0;
  
  if(id_01_rec[5]==1 && id_01_rec[6]==1)
   display_init1++;
  if(id_02_rec[5]==1 && id_02_rec[6]==2)
   display_init1++;
  if(id_03_rec[5]==1 && id_03_rec[6]==3)
   display_init1++;
  if(id_04_rec[5]==1 && id_04_rec[6]==4)
   display_init1++;
  if(id_05_rec[5]==1 && id_05_rec[6]==5)
   display_init1++;
  if(id_06_rec[5]==1 && id_06_rec[6]==6)
   display_init1++;

  if(id_01_rec[5]==2 && id_01_rec[6]==1)
   display_init2++;
  if(id_02_rec[5]==2 && id_02_rec[6]==2)
   display_init2++;
  if(id_03_rec[5]==2 && id_03_rec[6]==3)
   display_init2++;
  if(id_04_rec[5]==2 && id_04_rec[6]==4)
   display_init2++;
  if(id_05_rec[5]==2 && id_05_rec[6]==5)
   display_init2++;
  if(id_06_rec[5]==2 && id_06_rec[6]==6)
   display_init2++;

  if(id_01_rec[5]==3 && id_01_rec[6]==1)
   display_init3++;
  if(id_02_rec[5]==3 && id_02_rec[6]==2)
   display_init3++;
  if(id_03_rec[5]==3 && id_03_rec[6]==3)
   display_init3++;
  if(id_04_rec[5]==3 && id_04_rec[6]==4)
   display_init3++;
  if(id_05_rec[5]==3 && id_05_rec[6]==5)
   display_init3++;
  if(id_06_rec[5]==3 && id_06_rec[6]==6)
   display_init3++;

  if(display_init1>display_init2)
   display_init_final=display_init1;
  else
   display_init_final=display_init2;

  if(display_init_final<display_init3)
   display_init_final=display_init3;
  else
   display_init_final=display_init_final;  

}
//***************************************************first display board*************************************************************
void display_set1(void)
{  

 if( (dealer_vs==1)&& (display_time1==1))//petrol
 {
   //Serial.print("dealer code show 1");
   if((display_time1==1)&& (display_time2==1)&& (display_time3==1))
   {
    dealer_date();
    dealer_code(data11_temp,data12_temp,data13_temp,data14_temp,0,data_dealer1,data_dealer2,data_dealer3,data_dealer4,data_dealer5,data_dealer6);
   }
   else
    display_time1=2;
 }
 if(display_time1==1)
 {
  //Serial.println("inside1"); 
  if(dealer_vs!=1)
  display_time1=2;
 }

 //****************************************************petrol display************************************************************************  
 if(((display_time1==2)||(display_time1==3)) && (id_01_rec[5]==1 && id_01_rec[6]==1))//petrol
 {
  if((display_time1==2))
  {
   id1_data();
   data_display(data11_temp,data12_temp,data13_temp,data14_temp,0,0,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//petrol  
  }
  if(display_time1==3)
  {
   id1_data();
   data_display(data11_temp,data12_temp,data13_temp,data14_temp,0,5,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//petrol 
  }
 }
 else
 {
  //Serial.println("inside2");
  if( id_01_rec[5]!=1 && (display_time1==2))
   display_time1=4;
 }
 

//**************************************************diesel**********************************************************************************
  if( (display_time1==4|| display_time1==5 )&& (id_02_rec[5]==1 && id_02_rec[6]==2))//diesel
 {
  if(display_time1==4)
  { 
   id2_data();
   data_display(data11_temp,data12_temp,data13_temp,data14_temp,0,1,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//diesel
  }
  if(display_time1==5)
  {
   id2_data(); 
   data_display(data11_temp,data12_temp,data13_temp,data14_temp,0,6,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//diesel
  }
 }
  if( display_time1==4)
 {
  //Serial.println("inside4");
  if( id_02_rec[5]!=1 && (display_time1==4))
   display_time1=6;
 }


//**************************************************speed**********************************************************************************
  if((display_time1==6) && (id_03_rec[5]==1 && id_03_rec[6]==3))//speed
 {
  id3_data();
  data_display(data11_temp,data12_temp,data13_temp,data14_temp,0,2,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//speed  
 }
 if( display_time1==6)
 {
  //Serial.println("inside6");
  if( id_03_rec[5]!=1 && (display_time1==6))
   display_time1=8;
 }
//**************************************************speed in hindi**********************************************************************************
  if((display_time1==7)&& ((id_03_rec[5]==1 && id_03_rec[6]==3)))//speed
 {
  id3_data(); 
  data_display(data11_temp,data12_temp,data13_temp,data14_temp,0,7,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//speed   
  

 }

//**************************************************hi speed**********************************************************************************
 if((display_time1==8)&&(id_04_rec[5]==1 && id_04_rec[6]==4) )//hi speed
 {
  id4_data();
  data_display(data11_temp,data12_temp,data13_temp,data14_temp,0,3,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);// hi speed
 }
 if( display_time1==8)
 {
 //Serial.println("inside8");
 if( id_04_rec[5]!=1 && (display_time1==8))
   display_time1=10;
 }
//**************************************************hi speed in hindi**********************************************************************************
   if((display_time1==9)&& ((id_04_rec[5]==1 && id_04_rec[6]==4)) )//hi speed
 {
  id4_data(); 
  data_display(data11_temp,data12_temp,data13_temp,data14_temp,0,8,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);// hi speed

 }
 
//**************************************************LPG**********************************************************************************
  if((display_time1==10)&& (id_05_rec[5]==1 && id_05_rec[6]==5))//LPG
 {
  id5_data();
  data_display(data11_temp,data12_temp,data13_temp,data14_temp,0,4,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//LPG   
 }
 if( display_time1==10)
 {
 // Serial.println("inside10");
  if( id_05_rec[5]!=1 && (display_time1==10))
   display_time1=12;
 }
//**************************************************LPG in hindi **********************************************************************************
 if((display_time1==11) && (id_05_rec[5]==1 && id_05_rec[6]==5) )//LPG
 {
  id5_data();
  data_display(data11_temp,data12_temp,data13_temp,data14_temp,0,10,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//LPG  

 }
  
 //**************************************************CNG**********************************************************************************
  if((display_time1==12) && (id_06_rec[5]==1 && id_06_rec[6]==6))//CNG
 {
  id6_data();
  data_display(data11_temp,data12_temp,data13_temp,data14_temp,0,9,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//CNG    
 }
 if( display_time1==12)
 {
  //Serial.println("inside12");
  if( id_06_rec[5]!=1 && (display_time1==12))
    display_time1=14;
 }
//**************************************************CNG in hindi **********************************************************************************
   if((display_time1==13) && ((id_06_rec[5]==1 && id_06_rec[6]==6)))//CNG
  {
   id6_data();
   data_display(data11_temp,data12_temp,data13_temp,data14_temp,0,11,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//CNG  
  }
 
//**************************************************end and clear all data of shift register********************************************
 if(display_time1>13)
 {
 // Serial.println("inside14");
  //display_time1=0;
  if(id_06_rec[5]==1)
  {
   display_time1=12;
   id6_data();
   data_display(data11_temp,data12_temp,data13_temp,data14_temp,0,9,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//CNG    
  }
  if(id_05_rec[5]==1)
  {
   display_time1=10;
   id5_data();
   data_display(data11_temp,data12_temp,data13_temp,data14_temp,0,4,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//LPG   
  }
  if(id_04_rec[5]==1)
  {
   display_time1=8;
   id4_data();
   data_display(data11_temp,data12_temp,data13_temp,data14_temp,0,3,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);// hi speed

  }
  if(id_03_rec[5]==1)
  {
   id3_data();
  data_display(data11_temp,data12_temp,data13_temp,data14_temp,0,2,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//speed  
  display_time1=6;
  }
 if(id_02_rec[5]==1)
  {
   display_time1=4;
   id2_data();
   data_display(data11_temp,data12_temp,data13_temp,data14_temp,0,1,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//diesel 
  }
  if(id_01_rec[5]==1)
  {
   display_time1=2;
   id1_data();
   data_display(data11_temp,data12_temp,data13_temp,data14_temp,0,0,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//petrol  
  }
 /* if(dealer_vs==1 )
  {
   dealer_date();
   dealer_code(data11_temp,data12_temp,data13_temp,data14_temp,0,data_dealer1,data_dealer2,data_dealer3,data_dealer4,data_dealer5,data_dealer6); 
   display_time1=1;
  } */

  if(id_01_rec[5]!=1 && id_02_rec[5]!=1 && id_03_rec[5]!=1 && id_04_rec[5]!=1 && id_05_rec[5]!=1 && id_06_rec[5]!=1 && dealer_vs!=1)
   display_time1=0;
 }

 /* Serial.print('\n');
 Serial.print("set1=");
 Serial.print(display_time1);
 Serial.print('\n');*/
}

void display_set1_verti(void)
{
// Serial.println(display_time1);
 //****************************************************petrol display************************************************************************ 
 if((display_time1==1) && (dealer_vs==1))//dealer code
 {
  func_verti_line1();  
 }
 
  if((display_time1==2) && (id_01_rec[5]==1 && id_01_rec[6]==1) )//petrol
 {
  if(id_02_rec[5]!=1 && id_03_rec[5]!=1 && id_04_rec[5]!=1 && id_05_rec[5]!=1 && id_06_rec[5]!=1 && dealer_vs!=1)
      func_verti_line1_product();
  else
      func_verti_line1();  
 }
 //****************************************************petrol in hindi************************************************************************
 else if((display_time1==3)&& (id_01_rec[5]==1 && id_01_rec[6]==1))//
 {
  func_verti_line1_product();  
 }
//**************************************************diesel**********************************************************************************
 else if((display_time1==4)&& (id_02_rec[5]==1 && id_02_rec[6]==2))//diesel
 {
  
     if(id_01_rec[5]!=1 && id_03_rec[5]!=1 && id_04_rec[5]!=1 && id_05_rec[5]!=1 && id_06_rec[5]!=1 && dealer_vs!=1)
       func_verti_line1_product();    
     else
      func_verti_line1(); 
 }
//**************************************************diesel in hindi**********************************************************************************
 else if((display_time1==5)&& (id_02_rec[5]==1 && id_02_rec[6]==2))//diesel
 {
  func_verti_line1_product();  
 } 
//**************************************************speed**********************************************************************************
 else if((display_time1==6)&& (id_03_rec[5]==1 && id_03_rec[6]==3))//speed
 {
  
     if(id_02_rec[5]!=1 && id_01_rec[5]!=1 && id_04_rec[5]!=1 && id_05_rec[5]!=1 && id_06_rec[5]!=1 && dealer_vs!=1)
      func_verti_line1_product();
     else
      func_verti_line1();  
 
 }
//**************************************************speed in hindi**********************************************************************************
 else if((display_time1==7)&& (id_03_rec[5]==1 && id_03_rec[6]==3))//speed
 {
  func_verti_line1_product();   
 }
//**************************************************hi speed**********************************************************************************
 else if((display_time1==8)&& (id_04_rec[5]==1 && id_04_rec[6]==4))//hi speed
 {
  if(id_03_rec[5]!=1 && id_02_rec[5]!=1 && id_01_rec[5]!=1 && id_05_rec[5]!=1 && id_06_rec[5]!=1 && dealer_vs!=1)
   func_verti_line1_product();
  else
   func_verti_line1();  
 }
//**************************************************hi speed in hindi**********************************************************************************
 else if((display_time1==9)&& (id_04_rec[5]==1 && id_04_rec[6]==4))//hi speed
 {
  func_verti_line1_product();
 }
//**************************************************LPG**********************************************************************************
 else if((display_time1==10)&& (id_05_rec[5]==1 && id_05_rec[6]==5))//LPG
 {
  if(id_04_rec[5]!=1 && id_03_rec[5]!=1 && id_02_rec[5]!=1 && id_01_rec[5]!=1 && id_06_rec[5]!=1 && dealer_vs!=1)
   func_verti_line1_product();
  else
   func_verti_line1();  
 }
//**************************************************LPG in hindi **********************************************************************************
 else if((display_time1==11)&& (id_05_rec[5]==1 && id_05_rec[6]==5))//LPG
 {
  func_verti_line1_product();   
 }
 //**************************************************CNG**********************************************************************************
 else if((display_time1==12)&& (id_06_rec[5]==1 && id_06_rec[6]==6))//CNG
 {
  if(id_05_rec[5]!=1 && id_04_rec[5]!=1 && id_03_rec[5]!=1 && id_02_rec[5]!=1 && id_01_rec[5]!=1 && dealer_vs!=1)
   func_verti_line1_product();
  else
   func_verti_line1();   
 }
//**************************************************CNG in hindi **********************************************************************************
 else if((display_time1==13)&& (id_06_rec[5]==1 && id_06_rec[6]==6))//CNG
 {
  func_verti_line1_product();   
 }
//**************************************************end and clear all data of shift register********************************************
 else if(display_time1==0)
 {
    if(id_01_rec[5]!=1 && id_02_rec[5]!=1 && id_03_rec[5]!=1 && id_04_rec[5]!=1 && id_05_rec[5]!=1 && id_06_rec[5]!=1 && dealer_vs!=1)
    {
     for(cm=0;cm<96;cm++)
     {
       data11[cm]=0;
       data12[cm]=0;
       data13[cm]=0;
       data14[cm]=0;
     }
   }  
 }
}


//***************************************************second display board*************************************************************
void display_set2(void)
{
 
 if( (dealer_vs==1)&& (display_time2==1))//petrol
 {
   //Serial.print("dealer code show 2");
   if((display_time1==1)&& (display_time2==1)&& (display_time3==1))
   {
   dealer_date();
   dealer_code(data21_temp,data22_temp,data23_temp,data24_temp,0,data_dealer1,data_dealer2,data_dealer3,data_dealer4,data_dealer5,data_dealer6);
   }
   else
   display_time2=2;
 }
 if(display_time2==1)
 {
  //Serial.println("inside1"); 
  if(dealer_vs!=1)
  display_time2=2;
 }

 //****************************************************petrol display************************************************************************  
 if(((display_time2==2)||(display_time2==3)) && (id_01_rec[5]==2 && id_01_rec[6]==1))//petrol
 {
  if((display_time2==2))
  {
   id1_data();
   data_display(data21_temp,data22_temp,data23_temp,data24_temp,0,0,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//petrol  
  }
  if(display_time2==3)
  {
   id1_data();
   data_display(data21_temp,data22_temp,data23_temp,data24_temp,0,5,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//petrol 
  }
 }
 else
 {
 // Serial.println("2inside2");
  if( id_01_rec[5]!=2 && (display_time2==2))
   display_time2=4;
 }
 

//**************************************************diesel**********************************************************************************
  if( (display_time2==4|| display_time2==5 )&& (id_02_rec[5]==2 && id_02_rec[6]==2))//diesel
 {
  if(display_time2==4)
  { 
   id2_data();
   data_display(data21_temp,data22_temp,data23_temp,data24_temp,0,1,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//diesel
  }
  if(display_time2==5)
  {
   id2_data(); 
   data_display(data21_temp,data22_temp,data23_temp,data24_temp,0,6,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//diesel
  }
 }
  if( display_time2==4)
 {
  //Serial.println("2inside4");
  if( id_02_rec[5]!=2 && (display_time2==4))
   display_time2=6;
 }


//**************************************************speed**********************************************************************************
  if((display_time2==6) && (id_03_rec[5]==2 && id_03_rec[6]==3))//speed
 {
  id3_data();
  data_display(data21_temp,data22_temp,data23_temp,data24_temp,0,2,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//speed  
 }
 if( display_time2==6)
 {
 // Serial.println("inside6");
  if( id_03_rec[5]!=2 && (display_time2==6))
   display_time2=8;
 }
//**************************************************speed in hindi**********************************************************************************
  if((display_time2==7)&& ((id_03_rec[5]==2 && id_03_rec[6]==3)))//speed
 {
  id3_data(); 
  data_display(data21_temp,data22_temp,data23_temp,data24_temp,0,7,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//speed   
  

 }

//**************************************************hi speed**********************************************************************************
 if((display_time2==8)&&(id_04_rec[5]==2 && id_04_rec[6]==4) )//hi speed
 {
  id4_data();
  data_display(data21_temp,data22_temp,data23_temp,data24_temp,0,3,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);// hi speed
 }
 if( display_time2==8)
 {
// Serial.println("2inside8");
 if( id_04_rec[5]!=2 && (display_time2==8))
   display_time2=10;
 }
//**************************************************hi speed in hindi**********************************************************************************
   if((display_time2==9)&& ((id_04_rec[5]==2 && id_04_rec[6]==4)) )//hi speed
 {
  id4_data(); 
  data_display(data21_temp,data22_temp,data23_temp,data24_temp,0,8,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);// hi speed

 }
 
//**************************************************LPG**********************************************************************************
  if((display_time2==10)&& (id_05_rec[5]==2 && id_05_rec[6]==5))//LPG
 {
  id5_data();
  data_display(data21_temp,data22_temp,data23_temp,data24_temp,0,4,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//LPG   
 }
 if( display_time2==10)
 {
  //Serial.println("2inside10");
  if( id_05_rec[5]!=2 && (display_time2==10))
   display_time2=12;
 }
//**************************************************LPG in hindi **********************************************************************************
 if((display_time2==11) && (id_05_rec[5]==2 && id_05_rec[6]==5) )//LPG
 {
  id5_data();
  data_display(data21_temp,data22_temp,data23_temp,data24_temp,0,10,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//LPG  

 }
  
 //**************************************************CNG**********************************************************************************
  if((display_time2==12) && (id_06_rec[5]==2 && id_06_rec[6]==6))//CNG
 {
  id6_data();
  data_display(data21_temp,data22_temp,data23_temp,data24_temp,0,9,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//CNG    
 }
 if( display_time2==12)
 {
  //Serial.println("2inside12");
  if( id_06_rec[5]!=2 && (display_time2==12))
    display_time2=14;
 }
//**************************************************CNG in hindi **********************************************************************************
   if((display_time2==13) && ((id_06_rec[5]==2 && id_06_rec[6]==6)))//CNG
  {
   id6_data();
   data_display(data21_temp,data22_temp,data23_temp,data24_temp,0,11,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//CNG  
  }
 
//**************************************************end and clear all data of shift register********************************************
 if(display_time2>13)
 {
  //Serial.println("2inside14");
  //display_time1=0;
  if(id_06_rec[5]==2)
  {
   display_time2=12;
   id6_data();
   data_display(data21_temp,data22_temp,data23_temp,data24_temp,0,9,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//CNG    
  }
  if(id_05_rec[5]==2)
  {
   display_time2=10;
   id5_data();
   data_display(data21_temp,data22_temp,data23_temp,data24_temp,0,4,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//LPG   
  }
  if(id_04_rec[5]==2)
  {
   display_time2=8;
   id4_data();
   data_display(data21_temp,data22_temp,data23_temp,data24_temp,0,3,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);// hi speed

  }
  if(id_03_rec[5]==2)
  {
   id3_data();
  data_display(data21_temp,data22_temp,data23_temp,data24_temp,0,2,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//speed  
  display_time2=6;
  }
 if(id_02_rec[5]==2)
  {
   display_time2=4;
   id2_data();
   data_display(data21_temp,data22_temp,data23_temp,data24_temp,0,1,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//diesel 
  }
  if(id_01_rec[5]==2)
  {
   display_time2=2;
   id1_data();
   data_display(data21_temp,data22_temp,data23_temp,data24_temp,0,0,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//petrol  
  }
/*  if(dealer_vs==1)
  {
   dealer_date();
   dealer_code(data21_temp,data22_temp,data23_temp,data24_temp,0,data_dealer1,data_dealer2,data_dealer3,data_dealer4,data_dealer5,data_dealer6); 
   display_time2=1;
  } */

  if(id_01_rec[5]!=2 && id_02_rec[5]!=2 && id_03_rec[5]!=2 && id_04_rec[5]!=2 && id_05_rec[5]!=2 && id_06_rec[5]!=2 && dealer_vs!=1)
   display_time2=0;
 }
/*
 Serial.print('\n');
 Serial.print("set2=");
 Serial.print(display_time2);
 Serial.print('\n');*/
}

void display_set2_verti(void)
{
 //Serial.println(display_time2);
 //****************************************************petrol display************************************************************************ 
 if((display_time2==1) && (dealer_vs==1))//dealer code
 {
  func_verti_line2();  
 }
 
  if((display_time2==2) && (id_01_rec[5]==2 && id_01_rec[6]==1) )//petrol
 {
  if(id_02_rec[5]!=2 && id_03_rec[5]!=2 && id_04_rec[5]!=2 && id_05_rec[5]!=2 && id_06_rec[5]!=2 && dealer_vs!=1)
      func_verti_line2_product();
  else
      func_verti_line2();  
 }
 //****************************************************petrol in hindi************************************************************************
 else if((display_time2==3)&& (id_01_rec[5]==2 && id_01_rec[6]==1))//
 {
  func_verti_line2_product();  
 }
//**************************************************diesel**********************************************************************************
 else if((display_time2==4)&& (id_02_rec[5]==2 && id_02_rec[6]==2))//diesel
 {
  
    if(id_01_rec[5]!=2 && id_03_rec[5]!=2 && id_04_rec[5]!=2 && id_05_rec[5]!=2 && id_06_rec[5]!=2 && dealer_vs!=1)
      func_verti_line2_product();
  else
      func_verti_line2(); 
 }
//**************************************************diesel in hindi**********************************************************************************
 else if((display_time2==5)&& (id_02_rec[5]==2 && id_02_rec[6]==2))//diesel
 {
  func_verti_line2_product();  
 } 
//**************************************************speed**********************************************************************************
 else if((display_time2==6)&& (id_03_rec[5]==2 && id_03_rec[6]==3))//speed
 {
  
     if(id_01_rec[5]!=2 && id_02_rec[5]!=2 && id_04_rec[5]!=2 && id_05_rec[5]!=2 && id_06_rec[5]!=2 && dealer_vs!=1)
      func_verti_line2_product();
  else
      func_verti_line2();  
 
 }
//**************************************************speed in hindi**********************************************************************************
 else if((display_time2==7)&& (id_03_rec[5]==2 && id_03_rec[6]==3))//speed
 {
  func_verti_line2_product();   
 }
//**************************************************hi speed**********************************************************************************
 else if((display_time2==8)&& (id_04_rec[5]==2 && id_04_rec[6]==4))//hi speed
 {
  if(id_01_rec[5]!=2 && id_02_rec[5]!=2 && id_03_rec[5]!=2 && id_05_rec[5]!=2 && id_06_rec[5]!=2 && dealer_vs!=1)
      func_verti_line2_product();
  else
      func_verti_line2();
 }
//**************************************************hi speed in hindi**********************************************************************************
 else if((display_time2==9)&& (id_04_rec[5]==2 && id_04_rec[6]==4))//hi speed
 {
  func_verti_line2_product();
 }
//**************************************************LPG**********************************************************************************
 else if((display_time2==10)&& (id_05_rec[5]==2 && id_05_rec[6]==5))//LPG
 {
  if(id_01_rec[5]!=2 && id_02_rec[5]!=2 && id_03_rec[5]!=2 && id_04_rec[5]!=2 && id_06_rec[5]!=2 && dealer_vs!=1)
      func_verti_line2_product();
  else
      func_verti_line2(); 
 }
//**************************************************LPG in hindi **********************************************************************************
 else if((display_time2==11)&& (id_05_rec[5]==2 && id_05_rec[6]==5))//LPG
 {
  func_verti_line2_product();   
 }
 //**************************************************CNG**********************************************************************************
 else if((display_time2==12)&& (id_06_rec[5]==2 && id_06_rec[6]==6))//CNG
 {
  if(id_01_rec[5]!=2 && id_02_rec[5]!=2 && id_03_rec[5]!=2 && id_04_rec[5]!=2 && id_05_rec[5]!=2 && dealer_vs!=1)
      func_verti_line2_product();
  else
      func_verti_line2();   
 }
//**************************************************CNG in hindi **********************************************************************************
 else if((display_time2==13)&& (id_06_rec[5]==2 && id_06_rec[6]==6))//CNG
 {
  func_verti_line2_product();   
 }
//**************************************************end and clear all data of shift register********************************************
 else if(display_time2==0)
 {
    if(id_01_rec[5]!=2 && id_02_rec[5]!=2 && id_03_rec[5]!=2 && id_04_rec[5]!=2 && id_05_rec[5]!=2 && id_06_rec[5]!=2 && dealer_vs!=1)
    {
     for(cm=0;cm<96;cm++)
     {
       data21[cm]=0;
       data22[cm]=0;
       data23[cm]=0;
       data24[cm]=0;
     }
   }  
 }
}
//***************************************************Third display board*************************************************************
void display_set3(void)
{

 if( (dealer_vs==1)&& (display_time3==1))//petrol
 {
  // Serial.print("dealer code show 3");
   if((display_time1==1)&& (display_time2==1)&& (display_time3==1))
   {
    dealer_date();
    dealer_code(data31_temp,data32_temp,data33_temp,data34_temp,0,data_dealer1,data_dealer2,data_dealer3,data_dealer4,data_dealer5,data_dealer6);
   }
   else
    display_time3=2;
 }
 if(display_time3==1)
 {
  //Serial.println("3inside1"); 
  if(dealer_vs!=1)
  display_time3=2;
 }

 //****************************************************petrol display************************************************************************  
 if(((display_time3==2)||(display_time3==3)) && (id_01_rec[5]==3 && id_01_rec[6]==1))//petrol
 {
  if((display_time3==2))
  {
   id1_data();
   data_display(data31_temp,data32_temp,data33_temp,data34_temp,0,0,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//petrol  
  }
  if(display_time3==3)
  {
   id1_data();
   data_display(data31_temp,data32_temp,data33_temp,data34_temp,0,5,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//petrol 
  }
 }
 else
 {
  //Serial.println("3inside2");
  if( id_01_rec[5]!=3 && (display_time3==2))
   display_time3=4;
 }
 

//**************************************************diesel**********************************************************************************
  if( (display_time3==4|| display_time3==5 )&& (id_02_rec[5]==3 && id_02_rec[6]==2))//diesel
 {
  if(display_time3==4)
  { 
   id2_data();
   data_display(data31_temp,data32_temp,data33_temp,data34_temp,0,1,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//diesel
  }
  if(display_time3==5)
  {
   id2_data(); 
   data_display(data31_temp,data32_temp,data33_temp,data34_temp,0,6,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//diesel
  }
 }
 else
 {
  //Serial.println("3inside4");
  if( id_02_rec[5]!=3 && (display_time3==4))
   display_time3=6;
 }


//**************************************************speed**********************************************************************************
  if((display_time3==6) && (id_03_rec[5]==3 && id_03_rec[6]==3))//speed
 {
  id3_data();
  data_display(data31_temp,data32_temp,data33_temp,data34_temp,0,2,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//speed  
 }
 if( display_time3==6)
 {
 // Serial.println("3inside6");
  if( id_03_rec[5]!=3 && (display_time3==6))
   display_time3=8;
 }
//**************************************************speed in hindi**********************************************************************************
  if((display_time3==7)&& ((id_03_rec[5]==3 && id_03_rec[6]==3)))//speed
 {
  id3_data(); 
  data_display(data31_temp,data32_temp,data33_temp,data34_temp,0,7,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//speed   
 }

//**************************************************hi speed**********************************************************************************
 if((display_time3==8)&&(id_04_rec[5]==3 && id_04_rec[6]==4) )//hi speed
 {
  id4_data();
  data_display(data31_temp,data32_temp,data33_temp,data34_temp,0,3,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);// hi speed
 }
 if( display_time3==8)
 {
// Serial.println("3inside8");
 if( id_04_rec[5]!=3 && (display_time3==8))
   display_time3=10;
 }
//**************************************************hi speed in hindi**********************************************************************************
   if((display_time3==9)&& ((id_04_rec[5]==3 && id_04_rec[6]==4)) )//hi speed
 {
  id4_data(); 
  data_display(data31_temp,data32_temp,data33_temp,data34_temp,0,8,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);// hi speed

 }
 
//**************************************************LPG**********************************************************************************
  if((display_time3==10)&& (id_05_rec[5]==3 && id_05_rec[6]==5))//LPG
 {
  id5_data();
  data_display(data31_temp,data32_temp,data33_temp,data34_temp,0,4,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//LPG   
 }
 if( display_time3==10)
 {
 // Serial.println("3inside10");
  if( id_05_rec[5]!=3 && (display_time3==10))
   display_time3=12;
 }
//**************************************************LPG in hindi **********************************************************************************
 if((display_time3==11) && (id_05_rec[5]==3 && id_05_rec[6]==5) )//LPG
 {
  id5_data();
  data_display(data31_temp,data32_temp,data33_temp,data34_temp,0,10,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//LPG  

 }
  
 //**************************************************CNG**********************************************************************************
  if((display_time3==12) && (id_06_rec[5]==3 && id_06_rec[6]==6))//CNG
 {
  id6_data();
  data_display(data31_temp,data32_temp,data33_temp,data34_temp,0,9,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//CNG    
 }
 if( display_time3==12)
 {
 // Serial.println("3inside12");
  if( id_06_rec[5]!=3 && (display_time3==12))
    display_time3=14;
 }
//**************************************************CNG in hindi **********************************************************************************
   if((display_time3==13) && ((id_06_rec[5]==3 && id_06_rec[6]==6)))//CNG
  {
   id6_data();
   data_display(data31_temp,data32_temp,data33_temp,data34_temp,0,11,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//CNG  
  }
 
//**************************************************end and clear all data of shift register********************************************
 if(display_time3>13)
 {
 // Serial.println("3inside14");
  //display_time1=0;
  if(id_06_rec[5]==3)
  {
   display_time3=12;
   id6_data();
   data_display(data31_temp,data32_temp,data33_temp,data34_temp,0,9,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//CNG    
  }
  if(id_05_rec[5]==3)
  {
   display_time3=10;
   id5_data();
   data_display(data31_temp,data32_temp,data33_temp,data34_temp,0,4,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//LPG   
  }
  if(id_04_rec[5]==3)
  {
   display_time3=8;
   id4_data();
   data_display(data31_temp,data32_temp,data33_temp,data34_temp,0,3,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);// hi speed

  }
  if(id_03_rec[5]==3)
  {
   id3_data();
  data_display(data31_temp,data32_temp,data33_temp,data34_temp,0,2,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//speed  
  display_time3=6;
  }
 if(id_02_rec[5]==3)
  {
   display_time3=4;
   id2_data();
   data_display(data31_temp,data32_temp,data33_temp,data34_temp,0,1,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//diesel 
  }
  if(id_01_rec[5]==3)
  {
   display_time3=2;
   id1_data();
   data_display(data31_temp,data32_temp,data33_temp,data34_temp,0,0,data_num_big_rec1,data_num_big_rec2,data_num_big_rec3,data_num_med_dot,data_num_med_rec1,data_num_med_rec2);//petrol  
  }
 /* if(dealer_vs==1 )
  {
   dealer_date();
   dealer_code(data31_temp,data32_temp,data33_temp,data34_temp,0,data_dealer1,data_dealer2,data_dealer3,data_dealer4,data_dealer5,data_dealer6); 
   display_time3=1;
  } */

  if(id_01_rec[5]!=3 && id_02_rec[5]!=3 && id_03_rec[5]!=3 && id_04_rec[5]!=3 && id_05_rec[5]!=3 && id_06_rec[5]!=3 && dealer_vs!=1)
   display_time3=0;
 }
/*
  Serial.print('\n');
 Serial.print("set3=");
 Serial.print(display_time3);
 Serial.print('\n');*/
}

void display_set3_verti(void)
{
//Serial.println(display_time2);
 //****************************************************petrol display************************************************************************ 
 if((display_time3==1) && (dealer_vs==1))//dealer code
 {
  func_verti_line3();  
 }
 
  if((display_time3==2) && (id_01_rec[5]==3 && id_01_rec[6]==1) )//petrol
 {
  if(id_02_rec[5]!=3 && id_03_rec[5]!=3 && id_04_rec[5]!=3 && id_05_rec[5]!=3 && id_06_rec[5]!=3 && dealer_vs!=1)
      func_verti_line3_product();
  else
      func_verti_line3();  
 }
 //****************************************************petrol in hindi************************************************************************
 else if((display_time3==3)&& (id_01_rec[5]==3 && id_01_rec[6]==1))//
 {
  func_verti_line3_product();  
 }
//**************************************************diesel**********************************************************************************
 else if((display_time3==4)&& (id_02_rec[5]==3 && id_02_rec[6]==2))//diesel
 {
  
    if(id_01_rec[5]!=3 && id_03_rec[5]!=3 && id_04_rec[5]!=3 && id_05_rec[5]!=3 && id_06_rec[5]!=3 && dealer_vs!=1)
      func_verti_line3_product();
  else
      func_verti_line3(); 
 }
//**************************************************diesel in hindi**********************************************************************************
 else if((display_time3==5)&& (id_02_rec[5]==3 && id_02_rec[6]==2))//diesel
 {
  func_verti_line3_product();  
 } 
//**************************************************speed**********************************************************************************
 else if((display_time3==6)&& (id_03_rec[5]==3 && id_03_rec[6]==3))//speed
 {
  
     if(id_01_rec[5]!=3 && id_02_rec[5]!=3 && id_04_rec[5]!=3 && id_05_rec[5]!=3 && id_06_rec[5]!=3 && dealer_vs!=1)
      func_verti_line3_product();
  else
      func_verti_line3();  
 
 }
//**************************************************speed in hindi**********************************************************************************
 else if((display_time3==7)&& (id_03_rec[5]==3 && id_03_rec[6]==3))//speed
 {
  func_verti_line3_product();   
 }
//**************************************************hi speed**********************************************************************************
 else if((display_time3==8)&& (id_04_rec[5]==3 && id_04_rec[6]==4))//hi speed
 {
  if(id_01_rec[5]!=3 && id_02_rec[5]!=3 && id_03_rec[5]!=3 && id_05_rec[5]!=3 && id_06_rec[5]!=3 && dealer_vs!=1)
      func_verti_line3_product();
  else
      func_verti_line3();
 }
//**************************************************hi speed in hindi**********************************************************************************
 else if((display_time3==9)&& (id_04_rec[5]==3 && id_04_rec[6]==4))//hi speed
 {
  func_verti_line3_product();
 }
//**************************************************LPG**********************************************************************************
 else if((display_time3==10)&& (id_05_rec[5]==3 && id_05_rec[6]==5))//LPG
 {
  if(id_01_rec[5]!=3 && id_02_rec[5]!=3 && id_03_rec[5]!=3 && id_04_rec[5]!=3 && id_06_rec[5]!=3 && dealer_vs!=1)
      func_verti_line3_product();
  else
      func_verti_line3(); 
 }
//**************************************************LPG in hindi **********************************************************************************
 else if((display_time3==11)&& (id_05_rec[5]==3 && id_05_rec[6]==5))//LPG
 {
  func_verti_line3_product();   
 }
 //**************************************************CNG**********************************************************************************
 else if((display_time3==12)&& (id_06_rec[5]==3 && id_06_rec[6]==6))//CNG
 {
  if(id_01_rec[5]!=3 && id_02_rec[5]!=3 && id_03_rec[5]!=3 && id_04_rec[5]!=3 && id_05_rec[5]!=3 && dealer_vs!=1)
      func_verti_line3_product();
  else
      func_verti_line3();   
 }
//**************************************************CNG in hindi **********************************************************************************
 else if((display_time3==13)&& (id_06_rec[5]==3 && id_06_rec[6]==6))//CNG
 {
  func_verti_line3_product();   
 }
//**************************************************end and clear all data of shift register********************************************
 else if(display_time3==0)
 {
    if(id_01_rec[5]!=3 && id_02_rec[5]!=3 && id_03_rec[5]!=3 && id_04_rec[5]!=3 && id_05_rec[5]!=3 && id_06_rec[5]!=3 && dealer_vs!=1)
    {
     for(cm=0;cm<96;cm++)
     {
       data31[cm]=0;
       data32[cm]=0;
       data33[cm]=0;
       data34[cm]=0;
     }
   }  
 }
}

//*************************function for sending values to shift register****************************************************
void use(unsigned char *data1_shift,unsigned char *data2_shift,unsigned char *data3_shift,unsigned char *data4_shift)
{ 
 for(n=0;n<95;n++)
 {
  data11_shift=data1_shift[n];
  data21_shift=data2_shift[n];
  data31_shift=data3_shift[n];
  data41_shift=data4_shift[n];
  analogWrite(OE,bright_v);
  for(m=0;m<8;m++)
  {
   digitalWrite(SECLK,HIGH);
   if(data11_shift & 0x80)
    digitalWrite(SERDS1,LOW);
   else
    digitalWrite(SERDS1,HIGH);
    
   if(data21_shift & 0x80)
    digitalWrite(SERDS2,LOW);
   else
    digitalWrite(SERDS2,HIGH);
    
   if(data31_shift & 0x80)
    digitalWrite(SERDS3,LOW);
   else
    digitalWrite(SERDS3,HIGH);
     
   digitalWrite(SECLK,LOW);
   data11_shift= data11_shift<<1;
   data21_shift= data21_shift<<1;
   data31_shift= data31_shift<<1;
   data41_shift= data41_shift<<1;
  } 
 }
 
 data11_shift=data1_shift[95];
 data21_shift=data2_shift[95];
 data31_shift=data3_shift[95];
 data41_shift=data4_shift[95];
 
  analogWrite(OE,bright_v);
 for(m=0;m<9;m++)
 {
  digitalWrite(SECLK,HIGH);
   if(data11_shift & 0x80)
    digitalWrite(SERDS1,LOW);
   else
    digitalWrite(SERDS1,HIGH);
    
   if(data21_shift & 0x80)
    digitalWrite(SERDS2,LOW);
   else
    digitalWrite(SERDS2,HIGH);
    
   if(data31_shift & 0x80)
    digitalWrite(SERDS3,LOW);
   else
    digitalWrite(SERDS3,HIGH);
    
   digitalWrite(SECLK,LOW);
   data11_shift= data11_shift<<1;
   data21_shift= data21_shift<<1;
   data31_shift= data31_shift<<1;
   data41_shift= data41_shift<<1;
 }
}

//*********************************************sending data to shift register****************************************************************************** 
void shift_func(void)
{
  
 digitalWrite(LATCH,LOW);    
 use(data11,data21,data31,data41);//1,5,9,13  
 digitalWrite(A,LOW);
 digitalWrite(B,LOW);
 digitalWrite(LATCH,HIGH);
 //delay(5);
//************************************  
 digitalWrite(LATCH,LOW);
 use(data14,data24,data34,data44);//4,8,12,16
 digitalWrite(A,HIGH);
 digitalWrite(B,HIGH);
 digitalWrite(LATCH,HIGH);
 //delay(5);
 //************************************ 
 digitalWrite(LATCH,LOW);
 use(data12,data22,data32,data42);//2,6,10,14
 digitalWrite(A,HIGH);
 digitalWrite(B,LOW);
 digitalWrite(LATCH,HIGH);
 //delay(5);
 //************************************ 
 digitalWrite(LATCH,LOW);
 use(data13,data23,data33,data43);//3,7,11,15
 digitalWrite(A,LOW);
 digitalWrite(B,HIGH);
 digitalWrite(LATCH,HIGH);  
 //delay(5); 
}

//*************************function for dsiplay big,medium and characters ****************************************************
void data_display(unsigned char *data1_temp,unsigned char *data2_temp,unsigned char *data3_temp,unsigned char *data4_temp,unsigned char p10_no,unsigned char pt_di,unsigned char *data_fi,unsigned char *data_se,unsigned char *data_th,unsigned char *data_fo,unsigned char *data_fv,unsigned char *data_si)
{
 if(pt_di==0)
  data_display_petrol(data1_temp,data2_temp,data3_temp,data4_temp,p10_no);
 else if(pt_di==1)
  data_display_diesel(data1_temp,data2_temp,data3_temp,data4_temp,p10_no);
 else if(pt_di==2)
  data_display_speed(data1_temp,data2_temp,data3_temp,data4_temp,p10_no);
 else if(pt_di==3)
  data_display_HIspeed(data1_temp,data2_temp,data3_temp,data4_temp,p10_no);
 else if(pt_di==4)
  data_display_LPG(data1_temp,data2_temp,data3_temp,data4_temp,p10_no);
 else if(pt_di==5)//petrol in hindi
  data_display_petrol_hindi(data1_temp,data2_temp,data3_temp,data4_temp,p10_no);//petrol
 else if(pt_di==6)//diesel in hindi
  data_display_diesel_hindi(data1_temp,data2_temp,data3_temp,data4_temp,p10_no);//diesel
 else if(pt_di==7)//speed in hindi
  data_display_speed_hindi(data1_temp,data2_temp,data3_temp,data4_temp,p10_no);//speed
 else if(pt_di==8)//hispeed in hindi
  data_display_hispeed_hindi(data1_temp,data2_temp,data3_temp,data4_temp,p10_no);
 else if(pt_di==9)
  data_display_CNG(data1_temp,data2_temp,data3_temp,data4_temp,p10_no);//CNG
 else if(pt_di==10)//LPG in hindi
  data_display_LPG_hindi(data1_temp,data2_temp,data3_temp,data4_temp,p10_no);
 else if(pt_di==11)//CNG in hindi
  data_display_CNG_hindi(data1_temp,data2_temp,data3_temp,data4_temp,p10_no);
  
//********************big fonts *********************************
 seg_no1=4+(p10_no*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0x70 &(data_fi[im+16]<<4))|(0x0F &(data_fi[im+64]>>4));//first big number      
  for(im=0;im<4;im++)
 {  
  data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************  
 seg_no1=36+(p10_no*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0x70 &(data_fi[im+32]<<4))|(0x0F &(data_fi[im+80]>>4));//first big number    
 for(im=0;im<4;im++)
 {  
  data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=8+(p10_no*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0x20 &(data_se[im]<<5))|(0x1F &(data_se[im+16]>>3));//gap +second big number     
 for(im=0;im<4;im++)
 {  
  data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=40+(p10_no*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0x20 &(data_se[im+48]<<5))|(0x1F &(data_se[im+32]>>3));//gap + second big number
 for(im=0;im<4;im++)
 {  
  data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=12+(p10_no*96);
 for(im=0;im<16;im++)
  data_temp[im]=(0xE0 &(data_se[im+16]<<5))|(0x1F &(data_se[im+64]>>3));//second big number 
 for(im=0;im<4;im++)
 {  
  data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************  
 seg_no1=44+(p10_no*96);   
 for(im=0;im<16;im++)
  data_temp[im]=(0xE0 &(data_se[im+32]<<5))|(0x1F &(data_se[im+80]>>3));//second big number 
 for(im=0;im<4;im++)
 {  
  data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************  
 seg_no1=16+(p10_no*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0xE0 &(data_se[im+64]<<5))| (0x04 &(data_th[im]<<2)) | (0x03 &(data_th[im+16]>>6));//second big number + gap + third big number  
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************  
 seg_no1=48+(p10_no*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0xE0 &(data_se[im+80]<<5)) | (0x04 &(data_th[im+48]<<2)) | (0x03 &(data_th[im+32]>>6));//second big number + gapp + third big number 
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=20+(p10_no*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0xFC &(data_th[im+16]<<2)) | (0x03 &(data_th[im+64]>>6));// third big number
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=52+(p10_no*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0xFC &(data_th[im+32]<<2)) | (0x03 &(data_th[im+80]>>6));// third big number 
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
 //************************************ 
 seg_no1=24+(p10_no*96);    
 for(im=0;im<16;im++)
   data_temp[im]=(0xFC &(data_th[im+64]<<2));// third big number+gap
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=56+(p10_no*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0xFC &(data_th[im+80]<<2));// third big number+gap  
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
  }
  
//*******************medium digits and dot**********************
 seg_no1=28+(p10_no*96);    
 for(im=0;im<16;im++)
  data_temp[im]=0;//blank  
 for(im=0;im<4;im++)
 {  
  data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=60+(p10_no*96);    
 for(im=0;im<16;im++)
   data_temp[im]=0xC0 &(data_fo[im])| 0x0F &(data_fv[im]>>3);//dot+ gap + first medium digit
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=32+(p10_no*96);   
 for(im=0;im<16;im++)
   data_temp[im]=0x00;//blank  
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=64+(p10_no*96);    
 for(im=0;im<16;im++)
   data_temp[im]=0xE0 &(data_fv[im]<<5)|(0x0F &(data_si[im]>>3));//first medium digit
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=68+(p10_no*96);    
 for(im=0;im<16;im++)
   data_temp[im]=0xE0 &(data_si[im]<<5);//second medium digit  
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
}

//*************************************************petrol*******************************************************************************
void data_display_petrol(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1)
{
//************************************ 
 seg_no1=72+(p10_no1*96);    
 for(im=0;im<16;im++)
   data_temp[im]=(0x3F & (data_num_P[im]>>1));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=76+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0x80 & (data_num_P[im]<<7))|(0x3F & (data_num_e[im]>>1));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=80+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0x80 & (data_num_e[im]<<7))|(0x3E & (data_num_t[im]<<1));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=84+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0xF8 & (data_num_r[im]<<3))|(0x03 & (data_num_o[im]>>5));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=88+(p10_no1*96);
 for(im=0;im<16;im++)
  data_temp[im]=(0xF8 & (data_num_o[im]<<3))|(0x03 & (data_num_l[im]));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=92+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=0;
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************  
 seg_no1=96+(p10_no1*96);
 for(im=0;im<16;im++)
  data_temp[im]=0;
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
}

//****************************************************************diesel******************************************************************************
void data_display_diesel(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1)
{
//************************************ 
 seg_no1=72+(p10_no1*96);    
 for(im=0;im<16;im++)
   data_temp[im]=(0x3F & (data_num_D[im]>>2));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=76+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0xC0 & (data_num_D[im]<<6))|(0x18 & (data_num_i[im]<<3)) |(0x03 & (data_num_e[im]>>5));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************  
 seg_no1=80+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0xF8 & (data_num_e[im]<<3))|(0x03 & (data_num_s[im]>>4));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=84+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0xF0 & (data_num_s[im]<<4))|(0x07 & (data_num_e[im]>>4));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=88+(p10_no1*96);
 for(im=0;im<16;im++)
  data_temp[im]=(0xF0 & (data_num_e[im]<<4))|(0x06 & (data_num_l[im]<<1));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=92+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=0;
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=96+(p10_no1*96);
 for(im=0;im<16;im++)
  data_temp[im]=0;
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
}

//****************************************************************speed******************************************************************************
void data_display_speed(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1)
{
//************************************ 
 seg_no1=72+(p10_no1*96);    
 for(im=0;im<16;im++)
   data_temp[im]=(0x3F & (data_num_S[im]>>1));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=76+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0x80 & (data_num_S[im]<<7))|(0x3F & (data_num_p[im]>>1));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=80+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0x80 & (data_num_p[im]<<7))|(0x3F & (data_num_e[im]>>1));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=84+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0x80 & (data_num_e[im]<<7))|(0x3F & (data_num_e[im]>>1));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=88+(p10_no1*96);
 for(im=0;im<16;im++)
  data_temp[im]=(0x80 & (data_num_e[im]<<7))|(0x3F & (data_num_d[im]>>1));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=92+(p10_no1*96);   
 for(im=0;im<16;im++)
  data_temp[im]=(0x80 & (data_num_d[im]<<7));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************  
 seg_no1=96+(p10_no1*96);
 for(im=0;im<16;im++)
  data_temp[im]=0;
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
}

//****************************************************************hi speed******************************************************************************
void data_display_HIspeed(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1)
{
//************************************ 
 seg_no1=72+(p10_no1*96);    
 for(im=0;im<16;im++)
   data_temp[im]=(0x3F & (data_num_H[im]>>1));  
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=76+(p10_no1*96);   
 for(im=0;im<16;im++)
  data_temp[im]=(0x80 & (data_num_H[im]<<7))|(0x30 & (data_num_i[im]<<4))|(0x06 & (data_num_DASH[im]<<1));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=80+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0xFE & (data_num_S[im]<<1));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=84+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0xFE & (data_num_p[im]<<1));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=88+(p10_no1*96);
 for(im=0;im<16;im++)
  data_temp[im]=(0xFE & (data_num_e[im]<<1));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=92+(p10_no1*96);   
 for(im=0;im<16;im++)
  data_temp[im]=(0xFE & (data_num_e[im]<<1));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************  
 seg_no1=96+(p10_no1*96);
 for(im=0;im<16;im++)
  data_temp[im]=(0xFE & (data_num_d[im]<<1));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
}

//****************************************************************lpg******************************************************************************
void data_display_LPG(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1)
{
//************************************ 
 seg_no1=72+(p10_no1*96);    
 for(im=0;im<16;im++)
   data_temp[im]=(0x3F & (data_num_L[im]>>1));  
  for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=76+(p10_no1*96);     
 for(im=0;im<16;im++)
  data_temp[im]=(0x80 & (data_num_L[im]<<7))|(0x3F & (data_num_P[im]>>1));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=80+(p10_no1*96);     
 for(im=0;im<16;im++)
  data_temp[im]=(0x80 & (data_num_P[im]<<7))|(0x3F & (data_num_G[im]>>2));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=84+(p10_no1*96);     
 for(im=0;im<16;im++)
  data_temp[im]=(0xC0 & (data_num_G[im]<<6));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=88+(p10_no1*96); 
 for(im=0;im<16;im++)
  data_temp[im]=0x00;
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=92+(p10_no1*96);     
 for(im=0;im<16;im++)
  data_temp[im]=0x00;
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************  
 seg_no1=96+(p10_no1*96); 
 for(im=0;im<16;im++)
  data_temp[im]=0x00;
  for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
}

//****************************************************************CNG******************************************************************************
void data_display_CNG(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1)
{
//************************************ 
 seg_no1=72+(p10_no1*96);    
 for(im=0;im<16;im++)
   data_temp[im]=(0x3F & (data_num_C[im]>>1));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=76+(p10_no1*96);     
 for(im=0;im<16;im++)
  data_temp[im]=(0x80 & (data_num_C[im]<<7))|(0x3F & (data_num_N[im]>>1));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=80+(p10_no1*96);     
 for(im=0;im<16;im++)
  data_temp[im]=(0x80 & (data_num_N[im]<<7))|(0x3F & (data_num_G[im]>>2));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=84+(p10_no1*96);     
 for(im=0;im<16;im++)
  data_temp[im]=(0xC0 & (data_num_G[im]<<6));
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=88+(p10_no1*96); 
 for(im=0;im<16;im++)
  data_temp[im]=0x00;
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=92+(p10_no1*96);     
 for(im=0;im<16;im++)
  data_temp[im]=0x00;
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=96+(p10_no1*96);  
 for(im=0;im<16;im++)
  data_temp[im]=0x00;
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
}

//****************************************************************petrol in hindi******************************************************************************
void data_display_petrol_hindi(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1)
{
//************************************ 
 seg_no1=72+(p10_no1*96); 
 for(im=0;im<16;im++)
  data_temp[im]=data_num_pete1[im];
  for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=76+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=data_num_pete2[im];//    
  for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
  seg_no1=80+(p10_no1*96);    
  for(im=0;im<16;im++)
  data_temp[im]=data_num_pete3[im];
  for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
  seg_no1=84+(p10_no1*96);    
  for(im=0;im<16;im++)
  data_temp[im]=data_num_pete4[im];    
  for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
  seg_no1=88+(p10_no1*96);   
  for(im=0;im<16;im++)
   data_temp[im]=data_num_pete5[im];
  for(im=0;im<4;im++)
  {  
   data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
  }
//************************************ 
  seg_no1=92+(p10_no1*96);    
  for(im=0;im<16;im++)
   data_temp[im]=data_num_pete6[im];  
  for(im=0;im<4;im++)
  {  
   data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
  }
//************************************   
 seg_no1=96+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=data_num_pete7[im];
 for(im=0;im<4;im++)
 {  
   data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }  
}

//****************************************************************diesel in hindi******************************************************************************
void data_display_diesel_hindi(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1)
{
//************************************ 
 seg_no1=72+(p10_no1*96);    
 for(im=0;im<16;im++)
 data_temp[im]=data_num_des1[im];//data_r6
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=76+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=data_num_des2[im];//   
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=80+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=data_num_des3[im];
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=84+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=data_num_des4[im];
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************  
 seg_no1=88+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=data_num_des5[im];
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************  
 seg_no1=92+(p10_no1*96);
 for(im=0;im<16;im++)
  data_temp[im]=data_num_des6[im];
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=96+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=0x00;
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }  
}

//****************************************************************speed in hindi******************************************************************************
void data_display_speed_hindi(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1)
{
//************************************ 
 seg_no1=72+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=data_num_speed1[im];
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=76+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=data_num_speed2[im];
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=80+(p10_no1*96);    
 for(im=0;im<16;im++)
   data_temp[im]=data_num_speed3[im];
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=84+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=data_num_speed4[im];
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************  
 seg_no1=88+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=data_num_speed5[im];//data_r2
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=92+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=0x00;//data_r2
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=96+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=0x00;//data_r2
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
}

//****************************************************************hi speed in hindi******************************************************************************
void data_display_hispeed_hindi(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1)
{
//************************************ 
 seg_no1=72+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=data_num_hispeed1[im]; 
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=76+(p10_no1*96);    
 for(im=0;im<16;im++)
   data_temp[im]= data_num_hispeed2[im];
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=80+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]= data_num_speed1[im];
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=84+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=data_num_speed2[im];
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=88+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=data_num_speed3[im];//
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=92+(p10_no1*96);    
 for(im=0;im<16;im++)
   data_temp[im]=data_num_speed4[im];//
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************  
 seg_no1=96+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=data_num_speed5[im];//data_r22
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }  
}

//****************************************************************LPG in hindi******************************************************************************
void data_display_LPG_hindi(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1)
{
//************************************ 
 seg_no1=72+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=data_num_lpg1[im]; 
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
  seg_no1=76+(p10_no1*96);    
 for(im=0;im<16;im++)
   data_temp[im]=data_num_lpg2[im] ;
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=80+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]= data_num_lpg3[im];
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=84+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=data_num_lpg4[im] ;
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
  seg_no1=88+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=data_num_lpg5[im];//
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=92+(p10_no1*96);    
 for(im=0;im<16;im++)
   data_temp[im]=data_num_lpg6[im];//
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************  
 seg_no1=96+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=data_num_lpg7[im];//
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }  
}

//****************************************************************CNG in hindi******************************************************************************
void data_display_CNG_hindi(unsigned char *data1_temp1,unsigned char *data2_temp1,unsigned char *data3_temp1,unsigned char *data4_temp1,unsigned char p10_no1)
{
//************************************  
 seg_no1=72+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=data_num_cng1[im];
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=76+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=data_num_cng2[im];
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=80+(p10_no1*96);    
 for(im=0;im<16;im++)
   data_temp[im]=data_num_cng3[im];
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=84+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=data_num_cng4[im];
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=88+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=data_num_cng5[im]; 
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=92+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]= data_num_cng6[im];//
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=96+(p10_no1*96);    
 for(im=0;im<16;im++)
  data_temp[im]=0x00;//data_r2  
 for(im=0;im<4;im++)
 {  
  data1_temp1[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp1[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp1[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp1[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
}

//*******************************************dealer code*****************************************************************************************
void dealer_date(void)
{
 selecting_num_med(dealer[0],data_dealer1);//dealer code
 selecting_num_med(dealer[1],data_dealer2);
 selecting_num_med(dealer[2],data_dealer3);
 selecting_num_med(dealer[3],data_dealer4);
 selecting_num_med(dealer[4],data_dealer5);
 selecting_num_med(dealer[5],data_dealer6);
 
 selecting_num_med(date_save[0],date1);//d//date
 selecting_num_med(date_save[1],date2);//d
 selecting_num_med(date_save[2],date3);//m
 selecting_num_med(date_save[3],date4);//m
 selecting_num_med(date_save[4],date5);//y
 selecting_num_med(date_save[5],date6);//y
}
void dealer_code(unsigned char *data1_temp,unsigned char *data2_temp,unsigned char *data3_temp,unsigned char *data4_temp,unsigned char p10_no,unsigned char *data_fi,unsigned char *data_se,unsigned char *data_th,unsigned char *data_fo,unsigned char *data_fv,unsigned char *data_si)
{
  
//********************no value*********************************
 seg_no1=4+(p10_no*96);    
 for(im=0;im<16;im++)
  data_temp[im]=0xFF & date1[im]; //date1   
  for(im=0;im<4;im++)
 {  
  data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************  
 seg_no1=36+(p10_no*96);    
 for(im=0;im<16;im++)
  data_temp[im]=0x7F &(data_num_D[im]);//D   
 for(im=0;im<4;im++)
 {  
  data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=8+(p10_no*96);    
 for(im=0;im<16;im++)
  data_temp[im]=0xFF & date2[im];//date2     
 for(im=0;im<4;im++)
 {  
  data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=40+(p10_no*96);    
 for(im=0;im<16;im++)
  data_temp[im]=0x7F &(data_num_e[im]);//e
 for(im=0;im<4;im++)
 {  
  data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=12+(p10_no*96);
 for(im=0;im<16;im++)
  data_temp[im]=(0x60 & data_num_med_dot[im]>>1) | (0x1F & date3[im]>>3); //dot and date3
 for(im=0;im<4;im++)
 {  
  data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************  
 seg_no1=44+(p10_no*96);   
 for(im=0;im<16;im++)
  data_temp[im]=0x7F &(data_num_a[im]);//a
 for(im=0;im<4;im++)
 {  
  data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************  
 seg_no1=16+(p10_no*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0xE0 & date3[im]<<5) | (0x1F & date4[im]>>3); //dot,date3 and date4 
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************  
 seg_no1=48+(p10_no*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0x60&(data_num_l[im]<<5))|(0x0F&(data_num_e[im]>>3));//l and e
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=20+(p10_no*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0xE0 & date4[im]<<5)|(0x0C & data_num_med_dot[im]>>4)|(0x03 & date5[im]>>6); //date4,dot and date5 
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=52+(p10_no*96);    
 for(im=0;im<16;im++)
   data_temp[im]=(0xE0&(data_num_e[im]<<5))|(0x0F & (data_num_r[im]>>1));//e and r 
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
 //************************************ 
 seg_no1=24+(p10_no*96);    
 for(im=0;im<16;im++)
   data_temp[im]=(0xFC & date5[im]<<2)|(0x03 & date6[im]>>6); //date5,date6
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=56+(p10_no*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0x80&(data_num_r[im]<<7))|(0x3F&(data_num_C[im]>>2));//r +gap+ C 
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
  }
  
//*******************medium digits and dot**********************
 seg_no1=28+(p10_no*96);    
 for(im=0;im<16;im++)
 data_temp[im]=(0xFC & date6[im]<<2); //date6  
 for(im=0;im<4;im++)
 {  
  data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
  data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
  data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
  data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=60+(p10_no*96);    
 for(im=0;im<16;im++)
   data_temp[im]=(0xC0&(data_num_C[im]<<6))|(0x3F &(data_num_o[im]>>2));//C+o 
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=32+(p10_no*96);   
 for(im=0;im<16;im++)
   data_temp[im]=0;//blank  
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=64+(p10_no*96);    
 for(im=0;im<16;im++)
   data_temp[im]=(0xC0 & (data_num_o[im]<<6))|(0x3F&(data_num_d[im]>>2));//o+d 
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 }
//************************************ 
 seg_no1=68+(p10_no*96);    
 for(im=0;im<16;im++)
   data_temp[im]=(0xC0&(data_num_d[im]<<6))|(0x3F&(data_num_e[im]>>2));//d+e 
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 

 //************************************  
 seg_no1=72+(p10_no*96);    
 for(im=0;im<16;im++)
 data_temp[im]=(0xC0&(data_num_e[im]<<6))|(0x18 &(data_num_DASH[im]<<3)) |(0x04 & (data_num_DASH[im]<<1));//e+dash 
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=76+(p10_no*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0xFE &(data_fi[im])<<1);//first
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=80+(p10_no*96);    
 for(im=0;im<16;im++)
   data_temp[im]=(0xFE &(data_se[im]<<1));//second
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=84+(p10_no*96);    
 for(im=0;im<16;im++)
 data_temp[im]=(0xFE &(data_th[im]<<1));//third
for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=88+(p10_no*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0xFE &(data_fo[im]<<1));//fourth
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=92+(p10_no*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0xFE &(data_fv[im]<<1));//fifth
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
//************************************ 
 seg_no1=96+(p10_no*96);    
 for(im=0;im<16;im++)
  data_temp[im]=(0xFE &(data_si[im]<<1));//sixth  
 for(im=0;im<4;im++)
 {  
   data1_temp[seg_no1-(im+1)]=data_temp[im*4+0];
   data2_temp[seg_no1-(im+1)]=data_temp[im*4+1];
   data3_temp[seg_no1-(im+1)]=data_temp[im*4+2];
   data4_temp[seg_no1-(im+1)]=data_temp[im*4+3]; 
 } 
}

//************************************************function for veritcal scrolling all characte***************************************************
void func_verti(unsigned char p10_no,unsigned char vertical_temp)
{
  if(vertical_temp==0)
  {
   u[8]=0+(p10_no*96);
    u[9]=4+(p10_no*96);
    u[10]=8+(p10_no*96);
    u[11]=12+(p10_no*96);     
    u[12]=16+(p10_no*96);
    u[13]=20+(p10_no*96);
    u[14]=24+(p10_no*96);
    u[15]=28+(p10_no*96);     
  }
  else if(vertical_temp==1)
  { 
    u[0]=80+(p10_no*96);
    u[1]=84+(p10_no*96);
    u[2]=88+(p10_no*96);
    u[3]=92+(p10_no*96); 
    u[4]=64+(p10_no*96);
    u[5]=68+(p10_no*96);
    u[6]=72+(p10_no*96);
    u[7]=76+(p10_no*96);
    u[8]=1+(p10_no*96);
    u[9]=5+(p10_no*96);
    u[10]=9+(p10_no*96);
    u[11]=13+(p10_no*96);    
    u[12]=17+(p10_no*96);
    u[13]=21+(p10_no*96);
    u[14]=25+(p10_no*96);
    u[15]=29+(p10_no*96); 
  }
  else if(vertical_temp==2)
  {
   u[8]=2+(p10_no*96);
    u[9]=6+(p10_no*96);
    u[10]=10+(p10_no*96);
    u[11]=14+(p10_no*96);    
    u[12]=18+(p10_no*96);
    u[13]=22+(p10_no*96);
    u[14]=26+(p10_no*96);
    u[15]=30+(p10_no*96);     
  }
  else if(vertical_temp==3)
  {
    u[0]=81+(p10_no*96);
    u[1]=85+(p10_no*96);
    u[2]=89+(p10_no*96);
    u[3]=93+(p10_no*96);  
    u[4]=65+(p10_no*96);
    u[5]=69+(p10_no*96);
    u[6]=73+(p10_no*96);
    u[7]=77+(p10_no*96);
    u[8]=3+(p10_no*96);
    u[9]=7+(p10_no*96);
    u[10]=11+(p10_no*96);
    u[11]=15+(p10_no*96);    
    u[12]=19+(p10_no*96);
    u[13]=23+(p10_no*96);
    u[14]=27+(p10_no*96);
    u[15]=31+(p10_no*96);     
  }
  else if(vertical_temp==4)
  {
    u[8]=32+(p10_no*96);
    u[9]=36+(p10_no*96);
    u[10]=40+(p10_no*96);
    u[11]=44+(p10_no*96); 
    u[12]=48+(p10_no*96);
    u[13]=52+(p10_no*96);
    u[14]=56+(p10_no*96);
    u[15]=60+(p10_no*96);  
  }
  else if(vertical_temp==5)
  {
    u[0]=82+(p10_no*96);
    u[1]=86+(p10_no*96);
    u[2]=90+(p10_no*96);
    u[3]=94+(p10_no*96);    
    u[4]=66+(p10_no*96);
    u[5]=70+(p10_no*96);
    u[6]=74+(p10_no*96);
    u[7]=78+(p10_no*96); 
    u[8]=33+(p10_no*96);
    u[9]=37+(p10_no*96);
    u[10]=41+(p10_no*96);
    u[11]=45+(p10_no*96);    
    u[12]=49+(p10_no*96);
    u[13]=53+(p10_no*96);
    u[14]=57+(p10_no*96);
    u[15]=61+(p10_no*96);   
  }
  else if(vertical_temp==6)
  {
   u[8]=34+(p10_no*96);
    u[9]=38+(p10_no*96);
    u[10]=42+(p10_no*96);
    u[11]=46+(p10_no*96);    
    u[12]=50+(p10_no*96);
    u[13]=54+(p10_no*96);
    u[14]=58+(p10_no*96);
    u[15]=62+(p10_no*96);     
  }
  else if(vertical_temp==7)
  {
    u[0]=83+(p10_no*96);
    u[1]=87+(p10_no*96);
    u[2]=91+(p10_no*96);
    u[3]=95+(p10_no*96);    
    u[4]=67+(p10_no*96);
    u[5]=71+(p10_no*96);
    u[6]=75+(p10_no*96);
    u[7]=79+(p10_no*96); 
    u[8]=35+(p10_no*96);
    u[9]=39+(p10_no*96);
    u[10]=43+(p10_no*96);
    u[11]=47+(p10_no*96);    
    u[12]=51+(p10_no*96);
    u[13]=55+(p10_no*96);
    u[14]=59+(p10_no*96);
    u[15]=63+(p10_no*96); 
  }
}
//************************************************function for veritcal scrolling product name***************************************************
void func_verti_product(unsigned char p10_no,unsigned char vertical_temp)
{
  if(vertical_temp==1)
  { 
    u[0]=80+(p10_no*96);
    u[1]=84+(p10_no*96);
    u[2]=88+(p10_no*96);
    u[3]=92+(p10_no*96); 
    u[5]=68+(p10_no*96);
    u[6]=72+(p10_no*96); 
    u[7]=76+(p10_no*96);  
  }
 
  else if(vertical_temp==3)
  {
    u[0]=81+(p10_no*96);
    u[1]=85+(p10_no*96);
    u[2]=89+(p10_no*96);
    u[3]=93+(p10_no*96);  
    u[5]=69+(p10_no*96);
    u[6]=73+(p10_no*96);
    u[7]=77+(p10_no*96);        
  }
  else if(vertical_temp==5)
  {
    u[0]=82+(p10_no*96);
    u[1]=86+(p10_no*96);
    u[2]=90+(p10_no*96);
    u[3]=94+(p10_no*96);    
    u[5]=70+(p10_no*96);
    u[6]=74+(p10_no*96);
    u[7]=78+(p10_no*96);      
  }
  else if(vertical_temp==7)
  {
    u[0]=83+(p10_no*96);
    u[1]=87+(p10_no*96);
    u[2]=91+(p10_no*96);
    u[3]=95+(p10_no*96);    
    u[5]=71+(p10_no*96);
    u[6]=75+(p10_no*96);
    u[7]=79+(p10_no*96);   
  }
}
//************************************************rolling vertical all characters******************************************* 
void rolling_vertical(unsigned char *data_change,unsigned char *data_change1,unsigned char p10_no, unsigned char vertical_temp)
{
 if((vertical_temp%2)==1)
 {
  data_change[80+(p10_no*96)]=data_change[81+(p10_no*96)];
  data_change[81+(p10_no*96)]=data_change[82+(p10_no*96)];
  data_change[82+(p10_no*96)]=data_change[83+(p10_no*96)];
  data_change[83+(p10_no*96)]=data_change1[u[0]];//uo
  
  data_change[84+(p10_no*96)]=data_change[85+(p10_no*96)];
  data_change[85+(p10_no*96)]=data_change[86+(p10_no*96)];
  data_change[86+(p10_no*96)]=data_change[87+(p10_no*96)];
  data_change[87+(p10_no*96)]=data_change1[u[1]];//u1
  
  data_change[88+(p10_no*96)]=data_change[89+(p10_no*96)];
  data_change[89+(p10_no*96)]=data_change[90+(p10_no*96)];
  data_change[90+(p10_no*96)]=data_change[91+(p10_no*96)];
  data_change[91+(p10_no*96)]=data_change1[u[2]];//u2
  
  data_change[92+(p10_no*96)]=data_change[93+(p10_no*96)];
  data_change[93+(p10_no*96)]=data_change[94+(p10_no*96)];
  data_change[94+(p10_no*96)]=data_change[95+(p10_no*96)];
  data_change[95+(p10_no*96)]=data_change1[u[3]];//u3

  data_change[64+(p10_no*96)]=data_change[65+(p10_no*96)];
  data_change[65+(p10_no*96)]=data_change[66+(p10_no*96)];
  data_change[66+(p10_no*96)]=data_change[67+(p10_no*96)];
  data_change[67+(p10_no*96)]=data_change1[u[4]];//u4
  
  data_change[68+(p10_no*96)]=data_change[69+(p10_no*96)];
  data_change[69+(p10_no*96)]=data_change[70+(p10_no*96)];
  data_change[70+(p10_no*96)]=data_change[71+(p10_no*96)];
  data_change[71+(p10_no*96)]=data_change1[u[5]];//u5
  
  data_change[72+(p10_no*96)]=data_change[73+(p10_no*96)];
  data_change[73+(p10_no*96)]=data_change[74+(p10_no*96)];
  data_change[74+(p10_no*96)]=data_change[75+(p10_no*96)];
  data_change[75+(p10_no*96)]=data_change1[u[6]];//u6 
  
  data_change[76+(p10_no*96)]=data_change[77+(p10_no*96)];
  data_change[77+(p10_no*96)]=data_change[78+(p10_no*96)];
  data_change[78+(p10_no*96)]=data_change[79+(p10_no*96)];
  data_change[79+(p10_no*96)]=data_change1[u[7]];//u7
 }
 
  data_change[0+(p10_no*96)]=data_change[1+(p10_no*96)];
  data_change[1+(p10_no*96)]=data_change[2+(p10_no*96)];
  data_change[2+(p10_no*96)]=data_change[3+(p10_no*96)];
  data_change[3+(p10_no*96)]=data_change[32+(p10_no*96)];
  data_change[32+(p10_no*96)]=data_change[33+(p10_no*96)];
  data_change[33+(p10_no*96)]=data_change[34+(p10_no*96)];
  data_change[34+(p10_no*96)]=data_change[35+(p10_no*96)];
  data_change[35+(p10_no*96)]=data_change1[u[8]];//u8

  data_change[4+(p10_no*96)]=data_change[5+(p10_no*96)];
  data_change[5+(p10_no*96)]=data_change[6+(p10_no*96)];
  data_change[6+(p10_no*96)]=data_change[7+(p10_no*96)];
  data_change[7+(p10_no*96)]=data_change[36+(p10_no*96)];
  data_change[36+(p10_no*96)]=data_change[37+(p10_no*96)];
  data_change[37+(p10_no*96)]=data_change[38+(p10_no*96)];
  data_change[38+(p10_no*96)]=data_change[39+(p10_no*96)];
  data_change[39+(p10_no*96)]=data_change1[u[9]];//u9

  data_change[8+(p10_no*96)]=data_change[9+(p10_no*96)];
  data_change[9+(p10_no*96)]=data_change[10+(p10_no*96)];
  data_change[10+(p10_no*96)]=data_change[11+(p10_no*96)];
  data_change[11+(p10_no*96)]=data_change[40+(p10_no*96)];
  data_change[40+(p10_no*96)]=data_change[41+(p10_no*96)];
  data_change[41+(p10_no*96)]=data_change[42+(p10_no*96)];
  data_change[42+(p10_no*96)]=data_change[43+(p10_no*96)];
  data_change[43+(p10_no*96)]=data_change1[u[10]];//u10

  data_change[12+(p10_no*96)]=data_change[13+(p10_no*96)];
  data_change[13+(p10_no*96)]=data_change[14+(p10_no*96)];
  data_change[14+(p10_no*96)]=data_change[15+(p10_no*96)];
  data_change[15+(p10_no*96)]=data_change[44+(p10_no*96)];
  data_change[44+(p10_no*96)]=data_change[45+(p10_no*96)];
  data_change[45+(p10_no*96)]=data_change[46+(p10_no*96)];
  data_change[46+(p10_no*96)]=data_change[47+(p10_no*96)];
  data_change[47+(p10_no*96)]=data_change1[u[11]];//u11

  data_change[16+(p10_no*96)]=data_change[17+(p10_no*96)];
  data_change[17+(p10_no*96)]=data_change[18+(p10_no*96)];
  data_change[18+(p10_no*96)]=data_change[19+(p10_no*96)];
  data_change[19+(p10_no*96)]=data_change[48+(p10_no*96)];
  data_change[48+(p10_no*96)]=data_change[49+(p10_no*96)];
  data_change[49+(p10_no*96)]=data_change[50+(p10_no*96)];
  data_change[50+(p10_no*96)]=data_change[51+(p10_no*96)];
  data_change[51+(p10_no*96)]=data_change1[u[12]];//u12
  
  data_change[20+(p10_no*96)]=data_change[21+(p10_no*96)];
  data_change[21+(p10_no*96)]=data_change[22+(p10_no*96)];
  data_change[22+(p10_no*96)]=data_change[23+(p10_no*96)];
  data_change[23+(p10_no*96)]=data_change[52+(p10_no*96)];
  data_change[52+(p10_no*96)]=data_change[53+(p10_no*96)];
  data_change[53+(p10_no*96)]=data_change[54+(p10_no*96)];
  data_change[54+(p10_no*96)]=data_change[55+(p10_no*96)];
  data_change[55+(p10_no*96)]=data_change1[u[13]];//u13

  data_change[24+(p10_no*96)]=data_change[25+(p10_no*96)];
  data_change[25+(p10_no*96)]=data_change[26+(p10_no*96)];
  data_change[26+(p10_no*96)]=data_change[27+(p10_no*96)];
  data_change[27+(p10_no*96)]=data_change[56+(p10_no*96)];
  data_change[56+(p10_no*96)]=data_change[57+(p10_no*96)];
  data_change[57+(p10_no*96)]=data_change[58+(p10_no*96)];
  data_change[58+(p10_no*96)]=data_change[59+(p10_no*96)];
  data_change[59+(p10_no*96)]=data_change1[u[14]];//u14
   
  data_change[28+(p10_no*96)]=data_change[29+(p10_no*96)];
  data_change[29+(p10_no*96)]=data_change[30+(p10_no*96)];
  data_change[30+(p10_no*96)]=data_change[31+(p10_no*96)];
  data_change[31+(p10_no*96)]=data_change[60+(p10_no*96)];
  data_change[60+(p10_no*96)]=data_change[61+(p10_no*96)];
  data_change[61+(p10_no*96)]=data_change[62+(p10_no*96)];
  data_change[62+(p10_no*96)]=data_change[63+(p10_no*96)];
  data_change[63+(p10_no*96)]=data_change1[u[15]];//u15
}

//************************************************rolling vertical productname******************************************* 
void rolling_vertical_product(unsigned char *data_change,unsigned char *data_change1,unsigned char p10_no, unsigned char vertical_temp)
{
 if((vertical_temp)==0)
 {
  for(cm=0;cm<68;cm++)
   data_change[cm]=data_change1[cm];
  
 }
 if((vertical_temp%2)==1)
 {
  data_change[80+(p10_no*96)]=data_change[81+(p10_no*96)];
  data_change[81+(p10_no*96)]=data_change[82+(p10_no*96)];
  data_change[82+(p10_no*96)]=data_change[83+(p10_no*96)];
  data_change[83+(p10_no*96)]=data_change1[u[0]];//uo
  
  data_change[84+(p10_no*96)]=data_change[85+(p10_no*96)];
  data_change[85+(p10_no*96)]=data_change[86+(p10_no*96)];
  data_change[86+(p10_no*96)]=data_change[87+(p10_no*96)];
  data_change[87+(p10_no*96)]=data_change1[u[1]];//u1
  
  data_change[88+(p10_no*96)]=data_change[89+(p10_no*96)];
  data_change[89+(p10_no*96)]=data_change[90+(p10_no*96)];
  data_change[90+(p10_no*96)]=data_change[91+(p10_no*96)];
  data_change[91+(p10_no*96)]=data_change1[u[2]];//u2
  
  data_change[92+(p10_no*96)]=data_change[93+(p10_no*96)];
  data_change[93+(p10_no*96)]=data_change[94+(p10_no*96)];
  data_change[94+(p10_no*96)]=data_change[95+(p10_no*96)];
  data_change[95+(p10_no*96)]=data_change1[u[3]];//u3

 
  
  data_change[68+(p10_no*96)]=data_change[69+(p10_no*96)];
  data_change[69+(p10_no*96)]=data_change[70+(p10_no*96)];
  data_change[70+(p10_no*96)]=data_change[71+(p10_no*96)];
  data_change[71+(p10_no*96)]=data_change1[u[5]];//u5
  
  data_change[72+(p10_no*96)]=data_change[73+(p10_no*96)];
  data_change[73+(p10_no*96)]=data_change[74+(p10_no*96)];
  data_change[74+(p10_no*96)]=data_change[75+(p10_no*96)];
  data_change[75+(p10_no*96)]=data_change1[u[6]];//u6 

  data_change[76+(p10_no*96)]=data_change[77+(p10_no*96)];
  data_change[77+(p10_no*96)]=data_change[78+(p10_no*96)];
  data_change[78+(p10_no*96)]=data_change[79+(p10_no*96)];
  data_change[79+(p10_no*96)]=data_change1[u[7]];//u7
 } 
   
}


//**************************************************selecti0n of big number **********************************************************************************
void selecting_num_big(unsigned char num_sel,unsigned char *data_num_big_rec)
{
  unsigned char sel_im=0;
  if(num_sel==0)
  {
   for(sel_im=0;sel_im<96;sel_im++)
    data_num_big_rec[sel_im]=data_num_big_0[sel_im];
  }
  else if(num_sel==1)
  {
   for(sel_im=0;sel_im<96;sel_im++)
    data_num_big_rec[sel_im]=data_num_big_1[sel_im];
  }
  else if(num_sel==2)
  {
   for(sel_im=0;sel_im<96;sel_im++)
    data_num_big_rec[sel_im]=data_num_big_2[sel_im];
  }
  else if(num_sel==3)
  {
   for(sel_im=0;sel_im<96;sel_im++)
    data_num_big_rec[sel_im]=data_num_big_3[sel_im];
  }
  else if(num_sel==4)
  {
   for(sel_im=0;sel_im<96;sel_im++)
    data_num_big_rec[sel_im]=data_num_big_4[sel_im];
  }
  else if(num_sel==5)
  {
   for(sel_im=0;sel_im<96;sel_im++)
    data_num_big_rec[sel_im]=data_num_big_5[sel_im];
  }
  else if(num_sel==6)
  {
   for(sel_im=0;sel_im<96;sel_im++)
    data_num_big_rec[sel_im]=data_num_big_6[sel_im];
  }
  else if(num_sel==7)
  {
   for(sel_im=0;sel_im<96;sel_im++)
    data_num_big_rec[sel_im]=data_num_big_7[sel_im];
  }
  else if(num_sel==8)
  {
   for(sel_im=0;sel_im<96;sel_im++)
    data_num_big_rec[sel_im]=data_num_big_8[sel_im];
  }
  else if(num_sel==9)
  {
   for(sel_im=0;sel_im<96;sel_im++)
    data_num_big_rec[sel_im]=data_num_big_9[sel_im];
  }
  else if(num_sel==10)
  {
   for(sel_im=0;sel_im<96;sel_im++)
    data_num_big_rec[sel_im]=data_num_big_gapp[sel_im];
  }
 }
//***********************************selection of medium number******************* 
void selecting_num_med(unsigned char num_sel,unsigned char *data_num_med_rec) 
{
  unsigned char sel_im=0;
 if(num_sel==0)
 {
  for(sel_im=0;sel_im<16;sel_im++)
   data_num_med_rec[sel_im]=data_num_med_0[sel_im];
 }
 else if(num_sel==1)
 {
  for(sel_im=0;sel_im<16;sel_im++)
   data_num_med_rec[sel_im]=data_num_med_1[sel_im];
 }
 else if(num_sel==2)
 {
  for(sel_im=0;sel_im<16;sel_im++)
   data_num_med_rec[sel_im]=data_num_med_2[sel_im];
 }
  else if(num_sel==3)
 {
  for(sel_im=0;sel_im<16;sel_im++)
   data_num_med_rec[sel_im]=data_num_med_3[sel_im];
 }
 else if(num_sel==4)
 {
  for(sel_im=0;sel_im<16;sel_im++)
   data_num_med_rec[sel_im]=data_num_med_4[sel_im];
 }
 else if(num_sel==5)
 {
  for(sel_im=0;sel_im<16;sel_im++)
   data_num_med_rec[sel_im]=data_num_med_5[sel_im];
 }
 else if(num_sel==6)
 {
  for(sel_im=0;sel_im<16;sel_im++)
   data_num_med_rec[sel_im]=data_num_med_6[sel_im];
 }
  else if(num_sel==7)
 {
   for(sel_im=0;sel_im<16;sel_im++)
   data_num_med_rec[sel_im]=data_num_med_7[sel_im];
 }
 else if(num_sel==8)
 {
  for(sel_im=0;sel_im<16;sel_im++)
   data_num_med_rec[sel_im]=data_num_med_8[sel_im];
 }
  else if(num_sel==9)
 {
  for(sel_im=0;sel_im<16;sel_im++)
   data_num_med_rec[sel_im]=data_num_med_9[sel_im];
 }
 else if(num_sel==10)
 {
  for(sel_im=0;sel_im<16;sel_im++)
   data_num_med_rec[sel_im]=data_num_med_gapp[sel_im];
 }
}

//**************************************************function for clear all data**********************************************************************************
void clear_fun(void)
{
 
 for(cm=0;cm<96;cm++)
 {
  data11[cm]=0;
  data12[cm]=0;
  data13[cm]=0;
  data14[cm]=0;
   
  data21[cm]=0;
  data22[cm]=0;
  data23[cm]=0;
  data24[cm]=0; 
  
  data31[cm]=0;
  data32[cm]=0;
  data33[cm]=0;
  data34[cm]=0; 
  
  data41[cm]=0;
  data42[cm]=0;
  data43[cm]=0;
  data44[cm]=0;   
 }
  use(data11,data12,data13,data14);//1,5,9,13
  digitalWrite(A,LOW);
  digitalWrite(B,LOW);
 
  use(data12,data22,data32,data42);//2,6,10,14
  digitalWrite(A,HIGH);
  digitalWrite(B,LOW);

  use(data13,data23,data33,data43);//3,7,11,15
  digitalWrite(A,LOW);
  digitalWrite(B,HIGH);

  use(data14,data24,data34,data44);//4,8,12,16
  digitalWrite(A,HIGH);
  digitalWrite(B,HIGH);
}

//***********************************************************************************************************************************
void save_serial_data(void)
{
  clear_fun();        
  recieved_data = Serial.readString();
//*********************************************ID1(petrol)******************************************************************************  
 if(recieved_data[0]==':'&& recieved_data[1]=='A'&& recieved_data[3]=='1' && recieved_data[4]=='S' && recieved_data[10]=='B' && recieved_data[11]==';')//SET DATA OF ID1
 {
  for(rec_im=0;rec_im<5;rec_im++)
  {
   id_01_rec[rec_im]=(recieved_data[rec_im+5]-48);//vlaue to display
   EEPROM.write(rec_im,id_01_rec[rec_im]);
  }  
  memory_store_product(id_01_rec,1);           
 }


//*********************************************ID2(diesel)******************************************************************************    
 else if(recieved_data[0]==':'&& recieved_data[1]=='A' && recieved_data[3]=='2' && recieved_data[4]=='S' && recieved_data[10]=='B' && recieved_data[11]==';')//SET DATA OF ID2
 {
  for(rec_im=0;rec_im<5;rec_im++)
  {
   id_02_rec[rec_im]=(recieved_data[rec_im+5]-48);//vlaue to display
    EEPROM.write(rec_im+7,id_02_rec[rec_im]);
  }
  memory_store_product(id_02_rec,2);
 }
//*********************************************ID3(speed)******************************************************************************  
 else if(recieved_data[0]==':'&& recieved_data[1]=='A'&&  recieved_data[3]=='3' && recieved_data[4]=='S' && recieved_data[10]=='B' && recieved_data[11]==';')//SET DATA OF ID3
 {
  for(rec_im=0;rec_im<5;rec_im++)
  {
   id_03_rec[rec_im]=(recieved_data[rec_im+5]-48);//vlaue to display
   EEPROM.write(rec_im+14,id_03_rec[rec_im]); 
  }
  memory_store_product(id_03_rec,3); 
 }
//*********************************************ID4(high speed)******************************************************************************     
 else if(recieved_data[0]==':'&& recieved_data[1]=='A' && recieved_data[3]=='4' && recieved_data[4]=='S' && recieved_data[10]=='B' && recieved_data[11]==';')//SET DATA OF ID4
  {
   for(rec_im=0;rec_im<5;rec_im++)
   {
    id_04_rec[rec_im]=(recieved_data[rec_im+5]-48);//vlaue to display  
    EEPROM.write(rec_im+21,id_04_rec[rec_im]); 
   } 
   memory_store_product(id_04_rec,4);
  }

//*********************************************ID5(LPG)******************************************************************************     
else if(recieved_data[0]==':'&& recieved_data[1]=='A' && recieved_data[3]=='5' && recieved_data[4]=='S' && recieved_data[10]=='B' && recieved_data[11]==';')//SET DATA OF ID5
  {
   for(rec_im=0;rec_im<5;rec_im++)
   {
    id_05_rec[rec_im]=(recieved_data[rec_im+5]-48);//vlaue to display   
    EEPROM.write(rec_im+28,id_05_rec[rec_im]); 
   }
   memory_store_product(id_05_rec,5); 
  }   

 //*********************************************ID6(CNG)******************************************************************************     
 else if(recieved_data[0]==':'&& recieved_data[1]=='A' && recieved_data[3]=='6' && recieved_data[4]=='S' && recieved_data[10]=='B' && recieved_data[11]==';')//SET DATA OF ID6
  {
   for(rec_im=0;rec_im<5;rec_im++)
   {
    id_06_rec[rec_im]=(recieved_data[rec_im+5]-48);//vlaue to display   
    EEPROM.write(rec_im+35,id_06_rec[rec_im]); 
   }
   memory_store_product(id_06_rec,6); 
  }   

  //*********************************************dealer code ******************************************************************************     
 else if(recieved_data[0]==':'&& recieved_data[1]=='A' && recieved_data[2]=='D' && recieved_data[3]=='C' && recieved_data[10]=='B' && recieved_data[11]==';')//set dealer code
  {
    for(rec_im=0;rec_im<6;rec_im++)
    { 
     dealer[rec_im]=recieved_data[rec_im+4]-48;
     EEPROM.write(rec_im+60,dealer[rec_im]);
    }
        
    dealer_v[0]='h';
    dealer_v[1]='i';
    
    for(rec_im=0;rec_im<2;rec_im++)
     EEPROM.write(rec_im+68,dealer_v[rec_im]);
   dealer_vs=1;
  }  
 //*********************************************line 1 add product******************************************************************************     
 else if(recieved_data[0]==':'&& recieved_data[1]=='A' && recieved_data[2]=='L' && recieved_data[3]=='1' && recieved_data[4]=='A' && recieved_data[5]=='D' && recieved_data[6]=='D' && recieved_data[10]=='B' && recieved_data[11]==';')
  {
   if(recieved_data[8]=='0'&& recieved_data[9]=='1')
   {
     id_01_rec[5]=1;
     id_01_rec[6]=1;
     EEPROM.write(5,id_01_rec[5]);
     EEPROM.write(6,id_01_rec[6]);
   }
   else if(recieved_data[8]=='0'&& recieved_data[9]=='2')
   {
     id_02_rec[5]=1;
     id_02_rec[6]=2;
     EEPROM.write(12,id_02_rec[5]);
     EEPROM.write(13,id_02_rec[6]);
   }
   else if(recieved_data[8]=='0'&& recieved_data[9]=='3')
   {
     id_03_rec[5]=1;
     id_03_rec[6]=3;
     EEPROM.write(19,id_03_rec[5]);
     EEPROM.write(20,id_03_rec[6]);
   }
   else if(recieved_data[8]=='0'&& recieved_data[9]=='4')
   {
     id_04_rec[5]=1;
     id_04_rec[6]=4;
     EEPROM.write(26,id_04_rec[5]);
     EEPROM.write(27,id_04_rec[6]);
   }

   else if(recieved_data[8]=='0'&& recieved_data[9]=='5')
   {
     id_05_rec[5]=1;
     id_05_rec[6]=5;
     EEPROM.write(33,id_05_rec[5]);
     EEPROM.write(34,id_05_rec[6]);
   }

   else if(recieved_data[8]=='0'&& recieved_data[9]=='6')
   {
     id_06_rec[5]=1;
     id_06_rec[6]=6;
     EEPROM.write(40,id_06_rec[5]);
     EEPROM.write(41,id_06_rec[6]);
   }
   
  }

  //*********************************************line 2 add product******************************************************************************     
 else if(recieved_data[0]==':'&& recieved_data[1]=='A' && recieved_data[2]=='L' && recieved_data[3]=='2' && recieved_data[4]=='A' && recieved_data[5]=='D' && recieved_data[6]=='D' && recieved_data[10]=='B' && recieved_data[11]==';')
  {
   if(recieved_data[8]=='0'&& recieved_data[9]=='1')
   {
     id_01_rec[5]=2;
     id_01_rec[6]=1;
     EEPROM.write(5,id_01_rec[5]);
     EEPROM.write(6,id_01_rec[6]);
   }
   else if(recieved_data[8]=='0'&& recieved_data[9]=='2')
   {
     id_02_rec[5]=2;
     id_02_rec[6]=2;
     EEPROM.write(12,id_02_rec[5]);
     EEPROM.write(13,id_02_rec[6]);
   }
   else if(recieved_data[8]=='0'&& recieved_data[9]=='3')
   {
     id_03_rec[5]=2;
     id_03_rec[6]=3;
     EEPROM.write(19,id_03_rec[5]);
     EEPROM.write(20,id_03_rec[6]);
   }
   else if(recieved_data[8]=='0'&& recieved_data[9]=='4')
   {
     id_04_rec[5]=2;
     id_04_rec[6]=4;
     EEPROM.write(26,id_04_rec[5]);
     EEPROM.write(27,id_04_rec[6]);
   }

   else if(recieved_data[8]=='0'&& recieved_data[9]=='5')
   {
     id_05_rec[5]=2;
     id_05_rec[6]=5;
     EEPROM.write(33,id_05_rec[5]);
     EEPROM.write(34,id_05_rec[6]);
   }

   else if(recieved_data[8]=='0'&& recieved_data[9]=='6')
   {
     id_06_rec[5]=2;
     id_06_rec[6]=6;
     EEPROM.write(40,id_06_rec[5]);
     EEPROM.write(41,id_06_rec[6]);
   }
   
  }

  //*********************************************line 3 add product******************************************************************************     
 else if(recieved_data[0]==':'&& recieved_data[1]=='A' && recieved_data[2]=='L' && recieved_data[3]=='3' && recieved_data[4]=='A' && recieved_data[5]=='D' && recieved_data[6]=='D' && recieved_data[10]=='B' && recieved_data[11]==';')
  {
   if(recieved_data[8]=='0'&& recieved_data[9]=='1')
   {
     id_01_rec[5]=3;
     id_01_rec[6]=1;
     EEPROM.write(5,id_01_rec[5]);
     EEPROM.write(6,id_01_rec[6]);
   }
   else if(recieved_data[8]=='0'&& recieved_data[9]=='2')
   {
     id_02_rec[5]=3;
     id_02_rec[6]=2;
     EEPROM.write(12,id_02_rec[5]);
     EEPROM.write(13,id_02_rec[6]);
   }
   else if(recieved_data[8]=='0'&& recieved_data[9]=='3')
   {
     id_03_rec[5]=3;
     id_03_rec[6]=3;
     EEPROM.write(19,id_03_rec[5]);
     EEPROM.write(20,id_03_rec[6]);
   }
   else if(recieved_data[8]=='0'&& recieved_data[9]=='4')
   {
     id_04_rec[5]=3;
     id_04_rec[6]=4;
     EEPROM.write(26,id_04_rec[5]);
     EEPROM.write(27,id_04_rec[6]);
   }

   else if(recieved_data[8]=='0'&& recieved_data[9]=='5')
   {
     id_05_rec[5]=3;
     id_05_rec[6]=5;
     EEPROM.write(33,id_05_rec[5]);
     EEPROM.write(34,id_05_rec[6]);
   }

   else if(recieved_data[8]=='0'&& recieved_data[9]=='6')
   {
     id_06_rec[5]=3;
     id_06_rec[6]=6;
     EEPROM.write(40,id_06_rec[5]);
     EEPROM.write(41,id_06_rec[6]);
   }
   
  }

  //*********************************************line 1 remove product******************************************************************************     
 else if(recieved_data[0]==':'&& recieved_data[1]=='A' && recieved_data[2]=='L' && recieved_data[3]=='1' && recieved_data[4]=='R' && recieved_data[5]=='M' && recieved_data[6]=='V' && recieved_data[10]=='B' && recieved_data[11]==';' )
  {
   if(recieved_data[8]=='0'&& recieved_data[9]=='1' && id_01_rec[5]==1)
   {
     id_01_rec[5]=0;
     id_01_rec[6]=0;
     EEPROM.write(5,id_01_rec[5]);
     EEPROM.write(6,id_01_rec[6]);
   }
   else if(recieved_data[8]=='0'&& recieved_data[9]=='2' && id_02_rec[5]==1)
   {
     id_02_rec[5]=0;
     id_02_rec[6]=0;
     EEPROM.write(12,id_02_rec[5]);
     EEPROM.write(13,id_02_rec[6]);
   }
   else if(recieved_data[8]=='0'&& recieved_data[9]=='3' && id_03_rec[5]==1 )
   {
     id_03_rec[5]=0;
     id_03_rec[6]=0;
     EEPROM.write(19,id_03_rec[5]);
     EEPROM.write(20,id_03_rec[6]);
   }
   else if(recieved_data[8]=='0'&& recieved_data[9]=='4' && id_04_rec[5]==1)
   {
     id_04_rec[5]=0;
     id_04_rec[6]=0;
     EEPROM.write(26,id_04_rec[5]);
     EEPROM.write(27,id_04_rec[6]);
   }

   else if(recieved_data[8]=='0'&& recieved_data[9]=='5' && id_05_rec[5]==1)
   {
     id_05_rec[5]=0;
     id_05_rec[6]=0;
     EEPROM.write(33,id_05_rec[5]);
     EEPROM.write(34,id_05_rec[6]);
   } 

    else if(recieved_data[8]=='0'&& recieved_data[9]=='6' && id_06_rec[5]==1)
   {
     id_06_rec[5]=0;
     id_06_rec[6]=0;
     EEPROM.write(40,id_06_rec[5]);
     EEPROM.write(41,id_06_rec[6]);
   }
  }

  //*********************************************line 2 remove product******************************************************************************     
 else if(recieved_data[0]==':'&& recieved_data[1]=='A' && recieved_data[2]=='L' && recieved_data[3]=='2' && recieved_data[4]=='R' && recieved_data[5]=='M' && recieved_data[6]=='V' && recieved_data[10]=='B' && recieved_data[11]==';')
  {
   if(recieved_data[8]=='0'&& recieved_data[9]=='1' && id_01_rec[5]==2)
   {
     id_01_rec[5]=0;
     id_01_rec[6]=0;
     EEPROM.write(5,id_01_rec[5]);
     EEPROM.write(6,id_01_rec[6]);
   }
   else if(recieved_data[8]=='0'&& recieved_data[9]=='2' && id_02_rec[5]==2)
   {
     id_02_rec[5]=0;
     id_02_rec[6]=0;
     EEPROM.write(12,id_02_rec[5]);
     EEPROM.write(13,id_02_rec[6]);
   }
   else if(recieved_data[8]=='0'&& recieved_data[9]=='3' && id_03_rec[5]==2)
   {
     id_03_rec[5]=0;
     id_03_rec[6]=0;
     EEPROM.write(19,id_03_rec[5]);
     EEPROM.write(20,id_03_rec[6]);
   }
   else if(recieved_data[8]=='0'&& recieved_data[9]=='4' && id_04_rec[5]==2)
   {
     id_04_rec[5]=0;
     id_04_rec[6]=0;
     EEPROM.write(26,id_04_rec[5]);
     EEPROM.write(27,id_04_rec[6]);
   }

   else if(recieved_data[8]=='0'&& recieved_data[9]=='5' && id_05_rec[5]==2)
   {
     id_05_rec[5]=0;
     id_05_rec[6]=0;
     EEPROM.write(33,id_05_rec[5]);
     EEPROM.write(34,id_05_rec[6]);
   }  
    else if(recieved_data[8]=='0'&& recieved_data[9]=='6' && id_06_rec[5]==2)
   {
     id_06_rec[5]=0;
     id_06_rec[6]=0;
     EEPROM.write(40,id_06_rec[5]);
     EEPROM.write(41,id_06_rec[6]);
   }
  }

//*********************************************line 3 remove product******************************************************************************     
 else if(recieved_data[0]==':'&& recieved_data[1]=='A' && recieved_data[2]=='L' && recieved_data[3]=='3' && recieved_data[4]=='R' && recieved_data[5]=='M' && recieved_data[6]=='V' && recieved_data[10]=='B' && recieved_data[11]==';' )
  {
   if(recieved_data[8]=='0'&& recieved_data[9]=='1' && id_01_rec[5]==3)
   {
     id_01_rec[5]=0;
     id_01_rec[6]=0;
     EEPROM.write(5,id_01_rec[5]);
     EEPROM.write(6,id_01_rec[6]);
   }
   else if(recieved_data[8]=='0'&& recieved_data[9]=='2' && id_02_rec[5]==3)
   {
     id_02_rec[5]=0;
     id_02_rec[6]=0;
     EEPROM.write(12,id_02_rec[5]);
     EEPROM.write(13,id_02_rec[6]);
   }
   else if(recieved_data[8]=='0'&& recieved_data[9]=='3' && id_03_rec[5]==3)
   {
     id_03_rec[5]=0;
     id_03_rec[6]=0;
     EEPROM.write(19,id_03_rec[5]);
     EEPROM.write(20,id_03_rec[6]);
   }
   else if(recieved_data[8]=='0'&& recieved_data[9]=='4' && id_04_rec[5]==3)
   {
     id_04_rec[5]=0;
     id_04_rec[6]=0;
     EEPROM.write(26,id_04_rec[5]);
     EEPROM.write(27,id_04_rec[6]);
   }
   else if(recieved_data[8]=='0'&& recieved_data[9]=='5' && id_05_rec[5]==3)
   {
     id_05_rec[5]=0;
     id_05_rec[6]=0;
     EEPROM.write(33,id_05_rec[5]);
     EEPROM.write(34,id_05_rec[6]);
   }
    else if(recieved_data[8]=='0'&& recieved_data[9]=='6' && id_06_rec[5]==3)
   {
     id_06_rec[5]=0;
     id_06_rec[6]=0;
     EEPROM.write(40,id_06_rec[5]);
     EEPROM.write(41,id_06_rec[6]);
   }  
  }

  else if(recieved_data[0]==':'&& recieved_data[1]=='A' && recieved_data[2]=='R' && recieved_data[3]=='E'  && recieved_data[4]=='A' && recieved_data[5]=='D' && recieved_data[10]=='B' && recieved_data[11]==';')
   read_flag=1;
   
  else if(recieved_data[0]==':'&& recieved_data[1]=='A' && recieved_data[2]=='E' && recieved_data[3]=='R'  && recieved_data[4]=='A' && recieved_data[5]=='S' && recieved_data[6]=='E' && recieved_data[10]=='B' && recieved_data[11]==';')
   erase_flag=1;
  
//********************************************need this function after eeprom write function used******************************************************************************     
 EEPROM.commit(); 
//*******************************************clear sequence********************************************************************************************************************  
  display_time_m=0;
  display_time1=0;
  display_time2=0;
  display_time3=0; 
  for(rec_im=0;rec_im<20;rec_im++)
  {
   change_v[rec_im]=0;
   change_v2[rec_im]=0;
   change_v3[rec_im]=0;
  } 
 clear_horizontal_data();
 change_message=0;
}



///-----------------------------------Start of Function handleInit--------------------------------- 
void handleInit() 
{
  String window1;
 String start = "<html><body><script> window.location.href=" ;
  String mid1  = "'/?window1=" + window1 +"&show=true" + "';";
  String end1 = "</script> </body></html>";
  String  response = start + mid1 + end1 ;
  server.send(200,"text/html",response); 
}

//-----------------------------------Start of Function handleUpdate--------------------------------- 
void handleUpdate()
{
 //---------------------------------------------Petrol----------------------------------------------
  String window1 =server.arg("window1");    
             
  if(window1[0]!='.' && window1[1]!='.' && window1[4]!='.' && window1[5]!='.')
  {
   if(window1[2]=='.')
   {
    id_01_rec[0]=0;
    id_01_rec[1]=window1[0]-48;
    id_01_rec[2]=window1[1]-48;
    id_01_rec[3]=window1[3]-48;
    if(window1[4]==NULL)
     id_01_rec[4]=0;
    else
     id_01_rec[4]=window1[4]-48; 
     memory_store_product(id_01_rec,1);   
   }
   else if(window1[3]=='.')
   {
    id_01_rec[0]=window1[0]-48;
    id_01_rec[1]=window1[1]-48;
    id_01_rec[2]=window1[2]-48;
    id_01_rec[3]=window1[4]-48;
    if(window1[5]==NULL)
     id_01_rec[4]=0;
    else
     id_01_rec[4]=window1[5]-48; 
     memory_store_product(id_01_rec,1);   
   }
   for(rec_im=0;rec_im<5;rec_im++) //store value 
    EEPROM.write(rec_im,id_01_rec[rec_im]);
  }
  //----------------------------------------------Diesel------------------------------------------------
  String window2 =server.arg("window2");                  
  if(window2[0]!='.' && window2[1]!='.' && window2[4]!='.' && window2[5]!='.')
  {
   if(window2[2]=='.')
   {
    id_02_rec[0]=0;
    id_02_rec[1]=window2[0]-48;
    id_02_rec[2]=window2[1]-48;
    id_02_rec[3]=window2[3]-48;
    if(window2[4]==NULL)
     id_02_rec[4]=0;
    else
     id_02_rec[4]=window2[4]-48; 
      memory_store_product(id_02_rec,2);   
   }
   else if(window2[3]=='.')
   {
    id_02_rec[0]=window2[0]-48;
    id_02_rec[1]=window2[1]-48;
    id_02_rec[2]=window2[2]-48;
    id_02_rec[3]=window2[4]-48;
    if(window2[5]==NULL)
     id_02_rec[4]=0;
    else
     id_02_rec[4]=window2[5]-48; 
      memory_store_product(id_02_rec,2);   
   } 
   for(rec_im=0;rec_im<5;rec_im++) //store value 
    EEPROM.write(rec_im+7,id_02_rec[rec_im]);
    
  }
  
  //---------------------------------------------Speed-------------------------------------------------
  String window3 =server.arg("window3");                 
  if(window3[0]!='.' && window3[1]!='.' && window3[4]!='.' && window3[5]!='.')
  {
   if(window3[2]=='.')
   {
    id_03_rec[0]=0;
    id_03_rec[1]=window3[0]-48;
    id_03_rec[2]=window3[1]-48;
    id_03_rec[3]=window3[3]-48;
    if(window3[4]==NULL)
     id_03_rec[4]=0;
    else
     id_03_rec[4]=window3[4]-48;  
     memory_store_product(id_03_rec,3);  
   }
   else if(window3[3]=='.')
   {
    id_03_rec[0]=window3[0]-48;
    id_03_rec[1]=window3[1]-48;
    id_03_rec[2]=window3[2]-48;
    id_03_rec[3]=window3[4]-48;
    if(window3[5]==NULL)
     id_03_rec[4]=0;
    else
     id_03_rec[4]=window3[5]-48; 
      memory_store_product(id_03_rec,3);   
   }
   for(rec_im=0;rec_im<5;rec_im++) //store value 
    EEPROM.write(rec_im+14,id_03_rec[rec_im]);    
  }
          
  //------------------------------------------Hi-Speed----------------------------------------------------
  String window4 =server.arg("window4");                  
  if(window4[0]!='.' && window4[1]!='.' && window4[4]!='.' && window4[5]!='.')
  {
   if(window4[2]=='.')
   {
    id_04_rec[0]=0;
    id_04_rec[1]=window4[0]-48;
    id_04_rec[2]=window4[1]-48;
    id_04_rec[3]=window4[3]-48;
    if(window4[4]==NULL)
     id_04_rec[4]=0;
    else
     id_04_rec[4]=window4[4]-48;
     memory_store_product(id_04_rec,4);    
   }
   else if(window4[3]=='.')
   {
    id_04_rec[0]=window4[0]-48;
    id_04_rec[1]=window4[1]-48;
    id_04_rec[2]=window4[2]-48;
    id_04_rec[3]=window4[4]-48;
    if(window4[5]==NULL)
     id_04_rec[4]=0;
    else
     id_04_rec[4]=window4[5]-48;
     memory_store_product(id_04_rec,4);    
   }
   for(rec_im=0;rec_im<5;rec_im++) //store value 
    EEPROM.write(rec_im+21,id_04_rec[rec_im]);   
  }
 
 //-----------------------------------------LPG-----------------------------------------------------
 String window5 =server.arg("window5");                  
  if(window5[0]!='.' && window5[1]!='.' && window5[4]!='.' && window5[5]!='.')
  {
   if(window5[2]=='.')
   {
    id_05_rec[0]=0;
    id_05_rec[1]=window5[0]-48;
    id_05_rec[2]=window5[1]-48;
    id_05_rec[3]=window5[3]-48;
    if(window5[4]==NULL)
     id_05_rec[4]=0;
    else
     id_05_rec[4]=window5[4]-48;
     memory_store_product(id_05_rec,5);    
   }
   else if(window5[3]=='.')
   {
    id_05_rec[0]=window5[0]-48;
    id_05_rec[1]=window5[1]-48;
    id_05_rec[2]=window5[2]-48;
    id_05_rec[3]=window5[4]-48;
    if(window5[5]==NULL)
     id_05_rec[4]=0;
    else
     id_05_rec[4]=window5[5]-48;
     memory_store_product(id_05_rec,5);    
   }
  for(rec_im=0;rec_im<5;rec_im++) //store value 
    EEPROM.write(rec_im+28,id_05_rec[rec_im]);     
  }

  //-----------------------------------------CNG-----------------------------------------------------
 String window6 =server.arg("window6");                  
  if(window6[0]!='.' && window6[1]!='.' && window6[4]!='.' && window6[5]!='.')
  {
   if(window6[2]=='.')
   {
    id_06_rec[0]=0;
    id_06_rec[1]=window6[0]-48;
    id_06_rec[2]=window6[1]-48;
    id_06_rec[3]=window6[3]-48;
    if(window6[4]==NULL)
     id_06_rec[4]=0;
    else
     id_06_rec[4]=window6[4]-48;
     memory_store_product(id_06_rec,6);    
   }
   else if(window6[3]=='.')
   {
    id_06_rec[0]=window6[0]-48;
    id_06_rec[1]=window6[1]-48;
    id_06_rec[2]=window6[2]-48;
    id_06_rec[3]=window6[4]-48;
    if(window6[5]==NULL)
     id_06_rec[4]=0;
    else
     id_06_rec[4]=window6[5]-48;
     memory_store_product(id_06_rec,6);    
   }
   for(rec_im=0;rec_im<5;rec_im++) //store value 
    EEPROM.write(rec_im+35,id_06_rec[rec_im]);
  }
 //----------------------------------------------------------------------------------------------   
  String bright =server.arg("bright");
  if(bright[0]!=NULL && bright[1]!= NULL)
  {
   for(rec_im=0;rec_im<2;rec_im++)
   {
    brightness[rec_im]=bright[rec_im];
    EEPROM.write(rec_im+50,brightness[rec_im]);
   }
   bright_v=bright_high;
   //bright_f=bright_f_high;
   //digitalWrite(OE, HIGH);
   
   //analogWriteFreq(bright_f);  
  }  
  else
  {
   for(rec_im=0;rec_im<2;rec_im++)
   {
    brightness[rec_im]=0;
    EEPROM.write(rec_im+50,brightness[rec_im]);
   }
   bright_v=bright_low ;
   //bright_f=bright_f_low; 
   //analogWriteFreq(bright_f); 
  }
  
   String date1 =server.arg("date1");
  if(date1[0]!=NULL && date1[1]!=NULL &&  date1[2]!=NULL && date1[3]!=NULL)
  {
    date_save[0]=date1[8]-48;//d
    date_save[1]=date1[9]-48;//d
    date_save[2]=date1[5]-48;//m
    date_save[3]=date1[6]-48;//m
    date_save[4]=date1[2]-48;//y
    date_save[5]=date1[3]-48;//y

   for(rec_im=0;rec_im<6;rec_im++) //store value
   { 
    EEPROM.write(rec_im+80,date_save[rec_im]);
    Serial.print(date_save[rec_im]);
   }
    Serial.print(date1);
  }
  
  
  

  EEPROM.commit(); 
  //----------------------------------------http------------------------------------------------------
  String start = "<html><body><script> window.location.href=" ;
  String mid1  = "'/?window1=" + window1 + "&window2=" + window2 + "&window3=" + window3 + "&window4=" + window4 + "&window5=" + window5+"&window6=" + window6+"&date1=" +date1+ "&bright=" + bright  +"&show=true" + "';";
  String end1 = "</script> </body></html>";
  String  response = start + mid1 + end1 ;
  server.send(400,"text/html",response);
 //----------------------------------------------------------------------------------------------
display_time_m=0;
  display_time1=0;
  display_time2=0;
  display_time3=0; 
  for(rec_im=0;rec_im<20;rec_im++)
  {
   change_v[rec_im]=0;
   change_v2[rec_im]=0;
   change_v3[rec_im]=0;
  } 
 clear_horizontal_data();
 change_message=0; 
}

//-----------------------------------Start of Function handleUpdate1--------------------------------- 
void handleUpdate1()
{
 //*********************************************line 1 add/remove product****************************************************************************** 
 String window7 =server.arg("window7");     
  if(window7[0]!=NULL && window7[1]!=NULL )
  {
   if(window7[0]=='P'&& window7[1]=='+')
   {
     id_01_rec[5]=1;
     id_01_rec[6]=1;
     EEPROM.write(5,id_01_rec[5]);
     EEPROM.write(6,id_01_rec[6]);
   }
   else if(window7[0]=='P'&& window7[1]=='-' && id_01_rec[5]==1)
   {
     id_01_rec[5]=0;
     id_01_rec[6]=0;
     EEPROM.write(5,id_01_rec[5]);
     EEPROM.write(6,id_01_rec[6]);
   }
   else if(window7[0]=='D'&& window7[1]=='+')
   {
     id_02_rec[5]=1;
     id_02_rec[6]=2;
     EEPROM.write(12,id_02_rec[5]);
     EEPROM.write(13,id_02_rec[6]);
   }
   else if(window7[0]=='D'&& window7[1]=='-' && id_02_rec[5]==1)
   {
     id_02_rec[5]=0;
     id_02_rec[6]=0;
     EEPROM.write(12,id_02_rec[5]);
     EEPROM.write(13,id_02_rec[6]);
   }
   else if(window7[0]=='S'&& window7[1]=='+')
   {
     id_03_rec[5]=1;
     id_03_rec[6]=3;
     EEPROM.write(19,id_03_rec[5]);
     EEPROM.write(20,id_03_rec[6]);
   }
   else if(window7[0]=='S'&& window7[1]=='-' && id_03_rec[5]==1)
   {
     id_03_rec[5]=0;
     id_03_rec[6]=0;
     EEPROM.write(19,id_03_rec[5]);
     EEPROM.write(20,id_03_rec[6]);
   }
   else if(window7[0]=='H'&& window7[1]=='+')
   {
     id_04_rec[5]=1;
     id_04_rec[6]=4;
     EEPROM.write(26,id_04_rec[5]);
     EEPROM.write(27,id_04_rec[6]);
   }
   else if(window7[0]=='H'&& window7[1]=='-' && id_04_rec[5]==1)
   {
     id_04_rec[5]=0;
     id_04_rec[6]=0;
     EEPROM.write(26,id_04_rec[5]);
     EEPROM.write(27,id_04_rec[6]);
   }
   else if(window7[0]=='L'&& window7[1]=='+')
   {
     id_05_rec[5]=1;
     id_05_rec[6]=5;
     EEPROM.write(33,id_05_rec[5]);
     EEPROM.write(34,id_05_rec[6]);
   }
   else if(window7[0]=='L'&& window7[1]=='-' && id_05_rec[5]==1)
   {
     id_05_rec[5]=0;
     id_05_rec[6]=0;
     EEPROM.write(33,id_05_rec[5]);
     EEPROM.write(34,id_05_rec[6]);
   }
   else if(window7[0]=='C'&& window7[1]=='+')
   {
     id_06_rec[5]=1;
     id_06_rec[6]=6;
     EEPROM.write(40,id_06_rec[5]);
     EEPROM.write(41,id_06_rec[6]);
   }
   else if(window7[0]=='C'&& window7[1]=='-' && id_06_rec[5]==1)
   {
     id_06_rec[5]=0;
     id_06_rec[6]=0;
     EEPROM.write(40,id_06_rec[5]);
     EEPROM.write(41,id_06_rec[6]);
   } 
  }
//*********************************************line 2 add/remove product******************************************************************************
   String window8 =server.arg("window8");
         
  if(window8[0]!=NULL && window8[1]!=NULL )
  {
   if(window8[0]=='P'&& window8[1]=='+')
   {
     id_01_rec[5]=2;
     id_01_rec[6]=1;
     EEPROM.write(5,id_01_rec[5]);
     EEPROM.write(6,id_01_rec[6]);
   }
   else if(window8[0]=='P'&& window8[1]=='-' && id_01_rec[5]==2)
   {
     id_01_rec[5]=0;
     id_01_rec[6]=0;
     EEPROM.write(5,id_01_rec[5]);
     EEPROM.write(6,id_01_rec[6]);
   }
   else if(window8[0]=='D'&& window8[1]=='+')
   {
     id_02_rec[5]=2;
     id_02_rec[6]=2;
     EEPROM.write(12,id_02_rec[5]);
     EEPROM.write(13,id_02_rec[6]);
   }
   else if(window8[0]=='D'&& window8[1]=='-' && id_02_rec[5]==2)
   {
     id_02_rec[5]=0;
     id_02_rec[6]=0;
     EEPROM.write(12,id_02_rec[5]);
     EEPROM.write(13,id_02_rec[6]);
   }
   else if(window8[0]=='S'&& window8[1]=='+')
   {
     id_03_rec[5]=2;
     id_03_rec[6]=3;
     EEPROM.write(19,id_03_rec[5]);
     EEPROM.write(20,id_03_rec[6]);
   }
   else if(window8[0]=='S'&& window8[1]=='-' && id_03_rec[5]==2)
   {
     id_03_rec[5]=0;
     id_03_rec[6]=0;
     EEPROM.write(19,id_03_rec[5]);
     EEPROM.write(20,id_03_rec[6]);
   }
   else if(window8[0]=='H'&& window8[1]=='+')
   {
     id_04_rec[5]=2;
     id_04_rec[6]=4;
     EEPROM.write(26,id_04_rec[5]);
     EEPROM.write(27,id_04_rec[6]);
   }
   else if(window8[0]=='H'&& window8[1]=='-' && id_04_rec[5]==2)
   {
     id_04_rec[5]=0;
     id_04_rec[6]=0;
     EEPROM.write(26,id_04_rec[5]);
     EEPROM.write(27,id_04_rec[6]);
   }
   else if(window8[0]=='L'&& window8[1]=='+')
   {
     id_05_rec[5]=2;
     id_05_rec[6]=5;
     EEPROM.write(33,id_05_rec[5]);
     EEPROM.write(34,id_05_rec[6]);
   }
   else if(window8[0]=='L'&& window8[1]=='-' && id_05_rec[5]==2)
   {
     id_05_rec[5]=0;
     id_05_rec[6]=0;
     EEPROM.write(33,id_05_rec[5]);
     EEPROM.write(34,id_05_rec[6]);
   }
   else if(window8[0]=='C'&& window8[1]=='+')
   {
     id_06_rec[5]=2;
     id_06_rec[6]=6;
     EEPROM.write(40,id_06_rec[5]);
     EEPROM.write(41,id_06_rec[6]);
   }
   else if(window8[0]=='C'&& window8[1]=='-' && id_06_rec[5]==2)
   {
     id_06_rec[5]=0;
     id_06_rec[6]=0;
     EEPROM.write(40,id_06_rec[5]);
     EEPROM.write(41,id_06_rec[6]);
   } 
  }
  //*********************************************line 3 add/remove product****************************************************************************** 
   String window9 =server.arg("window9");
     
   if(window9[0]!=NULL && window9[1]!=NULL )
  {
   if(window9[0]=='P'&& window9[1]=='+')
   {
     id_01_rec[5]=3;
     id_01_rec[6]=1;
     EEPROM.write(5,id_01_rec[5]);
     EEPROM.write(6,id_01_rec[6]);
   }
   else if(window9[0]=='P'&& window9[1]=='-' && id_01_rec[5]==3)
   {
     id_01_rec[5]=0;
     id_01_rec[6]=0;
     EEPROM.write(5,id_01_rec[5]);
     EEPROM.write(6,id_01_rec[6]);
   }
   else if(window9[0]=='D'&& window9[1]=='+')
   {
     id_02_rec[5]=3;
     id_02_rec[6]=2;
     EEPROM.write(12,id_02_rec[5]);
     EEPROM.write(13,id_02_rec[6]);
   }
   else if(window9[0]=='D'&& window9[1]=='-' && id_02_rec[5]==3)
   {
     id_02_rec[5]=0;
     id_02_rec[6]=0;
     EEPROM.write(12,id_02_rec[5]);
     EEPROM.write(13,id_02_rec[6]);
   }
   else if(window9[0]=='S'&& window9[1]=='+')
   {
     id_03_rec[5]=3;
     id_03_rec[6]=3;
     EEPROM.write(19,id_03_rec[5]);
     EEPROM.write(20,id_03_rec[6]);
   }
   else if(window9[0]=='S'&& window9[1]=='-' && id_03_rec[5]==3)
   {
     id_03_rec[5]=0;
     id_03_rec[6]=0;
     EEPROM.write(19,id_03_rec[5]);
     EEPROM.write(20,id_03_rec[6]);
   }
   else if(window9[0]=='H'&& window9[1]=='+')
   {
     id_04_rec[5]=3;
     id_04_rec[6]=4;
     EEPROM.write(26,id_04_rec[5]);
     EEPROM.write(27,id_04_rec[6]);
   }
   else if(window9[0]=='H'&& window9[1]=='-' && id_04_rec[5]==3)
   {
     id_04_rec[5]=0;
     id_04_rec[6]=0;
     EEPROM.write(26,id_04_rec[5]);
     EEPROM.write(27,id_04_rec[6]);
   }
   else if(window9[0]=='L'&& window9[1]=='+')
   {
     id_05_rec[5]=3;
     id_05_rec[6]=5;
     EEPROM.write(33,id_05_rec[5]);
     EEPROM.write(34,id_05_rec[6]);
   }
   else if(window9[0]=='L'&& window9[1]=='-' && id_05_rec[5]==3)
   {
     id_05_rec[5]=0;
     id_05_rec[6]=0;
     EEPROM.write(33,id_05_rec[5]);
     EEPROM.write(34,id_05_rec[6]);
   }
   else if(window9[0]=='C'&& window9[1]=='+')
   {
     id_06_rec[5]=3;
     id_06_rec[6]=6;
     EEPROM.write(40,id_06_rec[5]);
     EEPROM.write(41,id_06_rec[6]);
   }
   else if(window9[0]=='C'&& window9[1]=='-' && id_06_rec[5]==3)
   {
     id_06_rec[5]=0;
     id_06_rec[6]=0;
     EEPROM.write(40,id_06_rec[5]);
     EEPROM.write(41,id_06_rec[6]);
   } 
  }
  //---------------------------------------------dealer code----------------------------------------------
  String window10 =server.arg("window10");                 
  if(window10[0]!=NULL && window10[1]!=NULL && window10[2]!=NULL && window10[3]!=NULL && window10[4]!=NULL  && window10[5]!=NULL)
  {
    dealer[0]=window10[0]-48;
    dealer[1]=window10[1]-48;
    dealer[2]=window10[2]-48;
    dealer[3]=window10[3]-48;
    dealer[4]=window10[4]-48;
    dealer[5]=window10[5]-48;
    
    
   for(rec_im=0;rec_im<6;rec_im++) //store value 
    EEPROM.write(rec_im+60,dealer[rec_im]);
    
  }
  //----------------------------------------------------------------------------------------------   
  String dealer_c=server.arg("dealer_c");
  if(dealer_c[0]!=NULL && dealer_c[1]!= NULL)
  {
   for(rec_im=0;rec_im<2;rec_im++)
   {
    dealer_v[rec_im]=dealer_c[rec_im];
    EEPROM.write(rec_im+68,dealer_c[rec_im]);
   }
   dealer_vs=1; 
  }  
  else
  {
   for(rec_im=0;rec_im<2;rec_im++)
   {
    dealer_v[rec_im]=dealer_c[rec_im];
    EEPROM.write(rec_im+68,dealer_c[rec_im]);
   }
   dealer_vs=0; 
  }

  EEPROM.commit(); 
  //----------------------------------------http------------------------------------------------------
  String start = "<html><body><script> window.location.href=" ;
  String mid1  = "'/?window7=" + window7 + "&window8=" + window8 + "&window9=" + window9 + "&window10=" + window10 + "&dealer_c=" + dealer_c +"&show=true" + "';";
  String end1 = "</script> </body></html>";
  String  response = start + mid1 + end1 ;
  server.send(400,"text/html",response);
 //----------------------------------------------------------------------------------------------
 display_time_m=0;
  display_time1=0;
  display_time2=0;
  display_time3=0; 
  for(rec_im=0;rec_im<20;rec_im++)
  {
   change_v[rec_im]=0;
   change_v2[rec_im]=0;
   change_v3[rec_im]=0;
  }
 clear_horizontal_data(); 
 change_message=0;
}

//-----------------------------------Start of Function handleUpdate1--------------------------------- 
void handleUpdate2()
{
 
 //*********************************************line 1 add/remove product****************************************************************************** 
 String promo_message =server.arg("promo_message");
   
  if(promo_message[0]!=NULL)
  {
     message_store_count=0;
    for(rec_im=0;rec_im<200;rec_im++)
    {
    
     message_store[rec_im]=promo_message[rec_im];
     EEPROM.write(100+rec_im,promo_message[rec_im]);
     Serial.println(promo_message[rec_im]);
     if(promo_message[rec_im]!=NULL)
      message_store_count++;
    }
    EEPROM.write(89,message_store_count);
    Serial.print("message_store_count=");
    Serial.println(message_store_count);
  }
  
  //----------------------------------------------------------------------------------------------   
  String message_f=server.arg("message_f");
  if(message_f[0]!=NULL && message_f[1]!= NULL)
  {
    message_flag=1;
    EEPROM.write(87,message_flag);
    
  }  
  else
  {
    message_flag=0;
    EEPROM.write(87,message_flag); 
  }

  EEPROM.commit(); 
  //----------------------------------------http------------------------------------------------------
  String start = "<html><body><script> window.location.href=" ;
  String mid1  = "'/?promo_message=" + promo_message + "&message_f=" + message_f  +"&show=true" + "';";
  String end1 = "</script> </body></html>";
  String  response = start + mid1 + end1 ;
  server.send(400,"text/html",response);
 //----------------------------------------------------------------------------------------------
  display_time_m=0;
  display_time1=0;
  display_time2=0;
  display_time3=0; 
  for(rec_im=0;rec_im<20;rec_im++)
  {
   change_v[rec_im]=0;
   change_v2[rec_im]=0;
   change_v3[rec_im]=0;
  } 
  clear_horizontal_data();
  change_message=0;
}
///-----------------------------------End of Function handleUpdate--------------------------------- 
void handleRoot()
{
  String s = MAIN_page; //Read HTML contents
  server.send(200, "text/html", s); //Send web page
/*
 server.sendHeader("Location", "/index.html");
 String window1 =server.arg("window1");
 String window2 =server.arg("window2");
 String window3 =server.arg("window3");
 String start = "<html><body><script> window.location.href=" ;
  String mid1  = "'/?window1=" + window1 + "&window2=" + window2 + "&window3=" + window3 +"&show=true" + "';";
  String end1 = "</script> </body></html>";
  String response = start + mid1 + end1 ;
  server.send(200,"text/html",response);
 */
}
//********************************write internal memory************************
void memory_store_product(unsigned char *store_array,unsigned char prodcut)
{
  product_info.store_int=(store_array[0]*10000)+(store_array[1]*1000)+(store_array[2]*100)+(store_array[3]*10)+(store_array[4]*1);
  if(prodcut==1)
   memory_ch[0]='P';
  else if(prodcut==2)
   memory_ch[0]='D';
  else if(prodcut==3)
   memory_ch[0]='S';
  else if(prodcut==4)
   memory_ch[0]='H';
  else if(prodcut==5)
   memory_ch[0]='L';
  else if(prodcut==6)
   memory_ch[0]='C';
  
  memory_ch[1]=product_info.store_ch[0];
  memory_ch[2]=product_info.store_ch[1];

  for(rec_im=0;rec_im<3;rec_im++)
   EEPROM.write(310+rec_im+(store_count*3), memory_ch[rec_im]);
  
  store_count++;
  if(store_count>51)
   store_count=0;
  EEPROM.write(302,store_count);  
}
//****************************read intrnal memory************************
void memory_read(void)
{


// Serial.println("memory in ");
 for(read_ch=0;read_ch<51;read_ch++)
 {
  for(rec_im=0;rec_im<3;rec_im++)
   memory_ch[rec_im]=EEPROM.read(310+rec_im+(read_ch*3));

  if(memory_ch[0]=='P')
   Serial.print("Petrol -");
  else if(memory_ch[0]=='D')
   Serial.print("Diesel -");
  else if(memory_ch[0]=='S')
   Serial.print("Speed -");
  else if(memory_ch[0]=='H')
   Serial.print("Hi-speed -");
  else if(memory_ch[0]=='L')
   Serial.print("LPG -");
  else if(memory_ch[0]=='C')
   Serial.print("CNG -");
 
  
  product_info.store_ch[0]=memory_ch[1];
  product_info.store_ch[1]=memory_ch[2];
  if(((product_info.store_int/10000)%10)==1)
    Serial.print((product_info.store_int/10000)%10);
  else
   Serial.print(' ');
  Serial.print((product_info.store_int/1000)%10);
  Serial.print((product_info.store_int/100)%10);
  Serial.print('.');
  Serial.print((product_info.store_int/10)%10);
  Serial.print((product_info.store_int/1)%10);
  Serial.print('\n');
 }
 //Serial.println("memory out ");
}
//***********************erase intenal memory************************
void memory_erase(void)
{
  unsigned read_ch=0;
  for(read_ch=0;read_ch<store_count;read_ch++)
  {
   for(rec_im=0;rec_im<3;rec_im++)
    EEPROM.write(310+rec_im+(read_ch*3),0);  
  }
  store_count=0; 
  EEPROM.write(302,store_count);
  EEPROM.commit(); 
}

//*************************function for welcome message scrolling****************************************************

void mes_rol(void)
{
  unsigned char im=0;
  counter_change++;
  
  if(jm==0)
  {
   zm=0x01;
   zm1=0xFE;
  }
  else if(jm==1)
  {
   zm=0x03;
   zm1=0xFC;
  }
  else if(jm==2)
  {
   zm=0x07;
   zm1=0xF8;
  } 
  else if(jm==3)
  {
   zm=0x0F;
   zm1=0xF0;
  } 
  else if(jm==4)
  {
   zm=0x1F;
   zm1=0xE0;
  }
  else if(jm==5)
  {
   zm=0x3F;
   zm1=0xC0;
  } 
  else if(jm==6)
  {
   zm=0x7F;
   zm1=0x80;
  } 
  else if(jm==7)
  {
   zm=0xFF;
   zm1=0x00;
  } 
  else if(jm==8)
  {
   for(im=0;im<16;im++)
   {
    data_temp_m15[im]=(data_temp_m14[im]);
    data_temp_m14[im]=(data_temp_m13[im]);
    data_temp_m13[im]=(data_temp_m12[im]);
    data_temp_m12[im]=(data_temp_m11[im]);
    data_temp_m11[im]=(data_temp_m10[im]);
    data_temp_m10[im]=(data_temp_m9[im]);
    data_temp_m9[im]=(data_temp_m8[im]);
    data_temp_m8[im]=(data_temp_m7[im]);
    data_temp_m7[im]=(data_temp_m6[im]);
    data_temp_m6[im]=(data_temp_m5[im]);
    data_temp_m5[im]=(data_temp_m4[im]); 
    data_temp_m4[im]=(data_temp_m3[im]);
    data_temp_m3[im]=(data_temp_m2[im]);
    data_temp_m2[im]=(data_temp_m1[im]);
    data_temp_m1[im]=(data_temp_m[im]);
    data_temp_m[im]=(message[im+(16*change_message)]);
   } 
   change_message++;   
   jm=0;
  }
    
  if(counter_change>=3)
  {
   counter_change=0;
   for(im=0;im<4;im++)
   {  
    data11[96-(im+1)]=data21[96-(im+1)]=data31[96-(im+1)]=(((data_temp_m[im*4+0]>>(8-jm))&zm)|((data_temp_m1[im*4+0]<<jm)&zm1));
    data12[96-(im+1)]=data22[96-(im+1)]=data32[96-(im+1)]=(((data_temp_m[im*4+1]>>(8-jm))&zm)|((data_temp_m1[im*4+1]<<jm)&zm1));
    data13[96-(im+1)]=data23[96-(im+1)]=data33[96-(im+1)]=(((data_temp_m[im*4+2]>>(8-jm))&zm)|((data_temp_m1[im*4+2]<<jm)&zm1));
    data14[96-(im+1)]=data24[96-(im+1)]=data34[96-(im+1)]=(((data_temp_m[im*4+3]>>(8-jm))&zm)|((data_temp_m1[im*4+3]<<jm)&zm1));
       
    data11[92-(im+1)]=data21[92-(im+1)]=data31[92-(im+1)]=(((data_temp_m1[im*4+0]>>(8-jm))&zm)|((data_temp_m2[im*4+0]<<jm)&zm1));
    data12[92-(im+1)]=data22[92-(im+1)]=data32[92-(im+1)]=(((data_temp_m1[im*4+1]>>(8-jm))&zm)|((data_temp_m2[im*4+1]<<jm)&zm1));
    data13[92-(im+1)]=data23[92-(im+1)]=data33[92-(im+1)]=(((data_temp_m1[im*4+2]>>(8-jm))&zm)|((data_temp_m2[im*4+2]<<jm)&zm1));
    data14[92-(im+1)]=data24[92-(im+1)]=data34[92-(im+1)]=(((data_temp_m1[im*4+3]>>(8-jm))&zm)|((data_temp_m2[im*4+3]<<jm)&zm1));
    
    data11[88-(im+1)]=data21[88-(im+1)]=data31[88-(im+1)]=(((data_temp_m2[im*4+0]>>(8-jm))&zm)|((data_temp_m3[im*4+0]<<jm)&zm1));
    data12[88-(im+1)]=data22[88-(im+1)]=data32[88-(im+1)]=(((data_temp_m2[im*4+1]>>(8-jm))&zm)|((data_temp_m3[im*4+1]<<jm)&zm1));
    data13[88-(im+1)]=data23[88-(im+1)]=data33[88-(im+1)]=(((data_temp_m2[im*4+2]>>(8-jm))&zm)|((data_temp_m3[im*4+2]<<jm)&zm1));
    data14[88-(im+1)]=data24[88-(im+1)]=data34[88-(im+1)]=(((data_temp_m2[im*4+3]>>(8-jm))&zm)|((data_temp_m3[im*4+3]<<jm)&zm1));
     
    data11[84-(im+1)]=data21[84-(im+1)]=data31[84-(im+1)]=(((data_temp_m3[im*4+0]>>(8-jm))&zm)|((data_temp_m4[im*4+0]<<jm)&zm1));
    data12[84-(im+1)]=data22[84-(im+1)]=data32[84-(im+1)]=(((data_temp_m3[im*4+1]>>(8-jm))&zm)|((data_temp_m4[im*4+1]<<jm)&zm1));
    data13[84-(im+1)]=data23[84-(im+1)]=data33[84-(im+1)]=(((data_temp_m3[im*4+2]>>(8-jm))&zm)|((data_temp_m4[im*4+2]<<jm)&zm1));
    data14[84-(im+1)]=data24[84-(im+1)]=data34[84-(im+1)]=(((data_temp_m3[im*4+3]>>(8-jm))&zm)|((data_temp_m4[im*4+3]<<jm)&zm1));
    
    data11[80-(im+1)]=data21[80-(im+1)]=data31[80-(im+1)]=(((data_temp_m4[im*4+0]>>(8-jm))&zm)|((data_temp_m5[im*4+0]<<jm)&zm1));
    data12[80-(im+1)]=data22[80-(im+1)]=data32[80-(im+1)]=(((data_temp_m4[im*4+1]>>(8-jm))&zm)|((data_temp_m5[im*4+1]<<jm)&zm1));
    data13[80-(im+1)]=data23[80-(im+1)]=data33[80-(im+1)]=(((data_temp_m4[im*4+2]>>(8-jm))&zm)|((data_temp_m5[im*4+2]<<jm)&zm1));
    data14[80-(im+1)]=data24[80-(im+1)]=data34[80-(im+1)]=(((data_temp_m4[im*4+3]>>(8-jm))&zm)|((data_temp_m5[im*4+3]<<jm)&zm1));
     
    data11[76-(im+1)]=data21[76-(im+1)]=data31[76-(im+1)]=(((data_temp_m5[im*4+0]>>(8-jm))&zm)|((data_temp_m6[im*4+0]<<jm)&zm1));
    data12[76-(im+1)]=data22[76-(im+1)]=data32[76-(im+1)]=(((data_temp_m5[im*4+1]>>(8-jm))&zm)|((data_temp_m6[im*4+1]<<jm)&zm1));
    data13[76-(im+1)]=data23[76-(im+1)]=data33[76-(im+1)]=(((data_temp_m5[im*4+2]>>(8-jm))&zm)|((data_temp_m6[im*4+2]<<jm)&zm1));
    data14[76-(im+1)]=data24[76-(im+1)]=data34[76-(im+1)]=(((data_temp_m5[im*4+3]>>(8-jm))&zm)|((data_temp_m6[im*4+3]<<jm)&zm1));
     
    data11[72-(im+1)]=data21[72-(im+1)]=data31[72-(im+1)]=(((data_temp_m6[im*4+0]>>(8-jm))&zm)|((data_temp_m7[im*4+0]<<jm)&zm1));
    data12[72-(im+1)]=data22[72-(im+1)]=data32[72-(im+1)]=(((data_temp_m6[im*4+1]>>(8-jm))&zm)|((data_temp_m7[im*4+1]<<jm)&zm1));
    data13[72-(im+1)]=data23[72-(im+1)]=data33[72-(im+1)]=(((data_temp_m6[im*4+2]>>(8-jm))&zm)|((data_temp_m7[im*4+2]<<jm)&zm1));
    data14[72-(im+1)]=data24[72-(im+1)]=data34[72-(im+1)]=(((data_temp_m6[im*4+3]>>(8-jm))&zm)|((data_temp_m7[im*4+3]<<jm)&zm1));
     
    data11[68-(im+1)]=data21[68-(im+1)]=data31[68-(im+1)]=(((data_temp_m7[im*4+0]>>(8-jm))&zm)|((data_temp_m8[im*4+0]<<jm)&zm1));
    data12[68-(im+1)]=data22[68-(im+1)]=data32[68-(im+1)]=(((data_temp_m7[im*4+1]>>(8-jm))&zm)|((data_temp_m8[im*4+1]<<jm)&zm1));
    data13[68-(im+1)]=data23[68-(im+1)]=data33[68-(im+1)]=(((data_temp_m7[im*4+2]>>(8-jm))&zm)|((data_temp_m8[im*4+2]<<jm)&zm1));
    data14[68-(im+1)]=data24[68-(im+1)]=data34[68-(im+1)]=(((data_temp_m7[im*4+3]>>(8-jm))&zm)|((data_temp_m8[im*4+3]<<jm)&zm1)); 
   
    data11[64-(im+1)]=data21[64-(im+1)]=data31[64-(im+1)]=(((data_temp_m8[im*4+0]>>(8-jm))&zm)|((data_temp_m9[im*4+0]<<jm)&zm1));
    data12[64-(im+1)]=data22[64-(im+1)]=data32[64-(im+1)]=(((data_temp_m8[im*4+1]>>(8-jm))&zm)|((data_temp_m9[im*4+1]<<jm)&zm1));
    data13[64-(im+1)]=data23[64-(im+1)]=data33[64-(im+1)]=(((data_temp_m8[im*4+2]>>(8-jm))&zm)|((data_temp_m9[im*4+2]<<jm)&zm1));
    data14[64-(im+1)]=data24[64-(im+1)]=data34[64-(im+1)]=(((data_temp_m8[im*4+3]>>(8-jm))&zm)|((data_temp_m9[im*4+3]<<jm)&zm1)); 
   
    data11[60-(im+1)]=data21[60-(im+1)]=data31[60-(im+1)]=(((data_temp_m9[im*4+0]>>(8-jm))&zm)|((data_temp_m10[im*4+0]<<jm)&zm1));
    data12[60-(im+1)]=data22[60-(im+1)]=data32[60-(im+1)]=(((data_temp_m9[im*4+1]>>(8-jm))&zm)|((data_temp_m10[im*4+1]<<jm)&zm1));
    data13[60-(im+1)]=data23[60-(im+1)]=data33[60-(im+1)]=(((data_temp_m9[im*4+2]>>(8-jm))&zm)|((data_temp_m10[im*4+2]<<jm)&zm1));
    data14[60-(im+1)]=data24[60-(im+1)]=data34[60-(im+1)]=(((data_temp_m9[im*4+3]>>(8-jm))&zm)|((data_temp_m10[im*4+3]<<jm)&zm1));
   
    data11[56-(im+1)]=data21[56-(im+1)]=data31[56-(im+1)]=(((data_temp_m10[im*4+0]>>(8-jm))&zm)|((data_temp_m11[im*4+0]<<jm)&zm1));
    data12[56-(im+1)]=data22[56-(im+1)]=data32[56-(im+1)]=(((data_temp_m10[im*4+1]>>(8-jm))&zm)|((data_temp_m11[im*4+1]<<jm)&zm1));
    data13[56-(im+1)]=data23[56-(im+1)]=data33[56-(im+1)]=(((data_temp_m10[im*4+2]>>(8-jm))&zm)|((data_temp_m11[im*4+2]<<jm)&zm1));
    data14[56-(im+1)]=data24[56-(im+1)]=data34[56-(im+1)]=(((data_temp_m10[im*4+3]>>(8-jm))&zm)|((data_temp_m11[im*4+3]<<jm)&zm1)); 
    
    data11[52-(im+1)]=data21[52-(im+1)]=data31[52-(im+1)]=(((data_temp_m11[im*4+0]>>(8-jm))&zm)|((data_temp_m12[im*4+0]<<jm)&zm1));
    data12[52-(im+1)]=data22[52-(im+1)]=data32[52-(im+1)]=(((data_temp_m11[im*4+1]>>(8-jm))&zm)|((data_temp_m12[im*4+1]<<jm)&zm1));
    data13[52-(im+1)]=data23[52-(im+1)]=data33[52-(im+1)]=(((data_temp_m11[im*4+2]>>(8-jm))&zm)|((data_temp_m12[im*4+2]<<jm)&zm1));
    data14[52-(im+1)]=data24[52-(im+1)]=data34[52-(im+1)]=(((data_temp_m11[im*4+3]>>(8-jm))&zm)|((data_temp_m12[im*4+3]<<jm)&zm1));
   
    data11[48-(im+1)]=data21[48-(im+1)]=data31[48-(im+1)]=(((data_temp_m12[im*4+0]>>(8-jm))&zm)|((data_temp_m13[im*4+0]<<jm)&zm1));
    data12[48-(im+1)]=data22[48-(im+1)]=data32[48-(im+1)]=(((data_temp_m12[im*4+1]>>(8-jm))&zm)|((data_temp_m13[im*4+1]<<jm)&zm1));
    data13[48-(im+1)]=data23[48-(im+1)]=data33[48-(im+1)]=(((data_temp_m12[im*4+2]>>(8-jm))&zm)|((data_temp_m13[im*4+2]<<jm)&zm1));
    data14[48-(im+1)]=data24[48-(im+1)]=data34[48-(im+1)]=(((data_temp_m12[im*4+3]>>(8-jm))&zm)|((data_temp_m13[im*4+3]<<jm)&zm1));
    
    data11[44-(im+1)]=data21[44-(im+1)]=data31[44-(im+1)]=(((data_temp_m13[im*4+0]>>(8-jm))&zm)|((data_temp_m14[im*4+0]<<jm)&zm1));
    data12[44-(im+1)]=data22[44-(im+1)]=data32[44-(im+1)]=(((data_temp_m13[im*4+1]>>(8-jm))&zm)|((data_temp_m14[im*4+1]<<jm)&zm1));
    data13[44-(im+1)]=data23[44-(im+1)]=data33[44-(im+1)]=(((data_temp_m13[im*4+2]>>(8-jm))&zm)|((data_temp_m14[im*4+2]<<jm)&zm1));
    data14[44-(im+1)]=data24[44-(im+1)]=data34[44-(im+1)]=(((data_temp_m13[im*4+3]>>(8-jm))&zm)|((data_temp_m14[im*4+3]<<jm)&zm1));
     
    data11[40-(im+1)]=data21[40-(im+1)]=data31[40-(im+1)]=(((data_temp_m14[im*4+0]>>(8-jm))&zm)|((data_temp_m15[im*4+0]<<jm)&zm1));
    data12[40-(im+1)]=data22[40-(im+1)]=data32[40-(im+1)]=(((data_temp_m14[im*4+1]>>(8-jm))&zm)|((data_temp_m15[im*4+1]<<jm)&zm1));
    data13[40-(im+1)]=data23[40-(im+1)]=data33[40-(im+1)]=(((data_temp_m14[im*4+2]>>(8-jm))&zm)|((data_temp_m15[im*4+2]<<jm)&zm1));
    data14[40-(im+1)]=data24[40-(im+1)]=data34[40-(im+1)]=(((data_temp_m14[im*4+3]>>(8-jm))&zm)|((data_temp_m15[im*4+3]<<jm)&zm1));
   
    data11[36-(im+1)]=data21[36-(im+1)]=data31[36-(im+1)]=((data_temp_m15[im*4+0]>>(8-jm))&zm);
    data12[36-(im+1)]=data22[36-(im+1)]=data32[36-(im+1)]=((data_temp_m15[im*4+1]>>(8-jm))&zm);
    data13[36-(im+1)]=data23[36-(im+1)]=data33[36-(im+1)]=((data_temp_m15[im*4+2]>>(8-jm))&zm);
    data14[36-(im+1)]=data24[36-(im+1)]=data34[36-(im+1)]=((data_temp_m15[im*4+3]>>(8-jm))&zm);    
   }
   jm++;  
  } 
}

void message_display(void)
{
  unsigned char message_count=0;

 for(message_count=0;message_count<200;message_count++)
 {
  if(message_store[message_count]=='0')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_0[i];  //0
  }
  else if(message_store[message_count]=='1')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_1[i];  //1
  }
   else if(message_store[message_count]=='2')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_2[i];  //2
  } 
  else if(message_store[message_count]=='3')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_3[i];  //3
  }
   else if(message_store[message_count]=='4')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_4[i];  //4
  } 
  else if(message_store[message_count]=='5')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_5[i];  //5
  }
   else if(message_store[message_count]=='6')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_6[i];  //6
  } 
  else if(message_store[message_count]=='7')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_7[i];  //7
  }
   else if(message_store[message_count]=='8')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_8[i];  //8
  } 
  else if(message_store[message_count]=='9')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_9[i];  //9
  }
   else if(message_store[message_count]=='A')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_A[i];  //A
  } 
  else if(message_store[message_count]=='B')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_B[i];  //B
  }
   else if(message_store[message_count]=='C')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_C[i];  //C
  }
  else if(message_store[message_count]=='D')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_D[i];  //D
  }
   else if(message_store[message_count]=='E')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_E[i];  //E
  } 
  else if(message_store[message_count]=='F')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_F[i];  //F
  }
   else if(message_store[message_count]=='G')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_G[i];  //G
  } 
  else if(message_store[message_count]=='H')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_H[i];  //H
  }
   else if(message_store[message_count]=='I')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_I[i];  //I
  } 
  else if(message_store[message_count]=='J')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_J[i];  //J
  }
   else if(message_store[message_count]=='K')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_K[i];  //K
  } 
  else if(message_store[message_count]=='L')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_L[i];  //L
  }
   else if(message_store[message_count]=='M')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_M[i];  //M
  } 
  else if(message_store[message_count]=='N')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_N[i];  //N
  }
   else if(message_store[message_count]=='O')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_O[i];  //O
  } 
  else if(message_store[message_count]=='P')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_P[i];  //P
  } 
  else if(message_store[message_count]=='Q')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_Q[i];  //Q
  }
   else if(message_store[message_count]=='R')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_R[i];  //R
  }
  else if(message_store[message_count]=='S')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_S[i];  //S
  }
   else if(message_store[message_count]=='T')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_T[i];  //T
  } 
  else if(message_store[message_count]=='U')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_U[i];  //U
  }
   else if(message_store[message_count]=='V')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_V[i];  //V
  } 
  else if(message_store[message_count]=='W')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_W[i];  //W
  }
   else if(message_store[message_count]=='X')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_X[i];  //X
  } 
  else if(message_store[message_count]=='Y')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_Y[i];  //Y
  }
   else if(message_store[message_count]=='Z')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_Z[i];  //Z
  } 
   else if(message_store[message_count]=='a')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_a[i];  //a
  } 
  else if(message_store[message_count]=='b')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_b[i];  //b
  }
   else if(message_store[message_count]=='c')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_c[i];  //c
  }
  else if(message_store[message_count]=='d')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_d[i];  //d
  }
   else if(message_store[message_count]=='e')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_e[i];  //e
  } 
  else if(message_store[message_count]=='f')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_f[i];  //f
  }
   else if(message_store[message_count]=='g')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_g[i];  //g
  } 
  else if(message_store[message_count]=='h')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_h[i];  //h
  }
   else if(message_store[message_count]=='i')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_i[i];  //i
  } 
  else if(message_store[message_count]=='j')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_j[i];  //j
  }
   else if(message_store[message_count]=='k')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_k[i];  //k
  } 
  else if(message_store[message_count]=='l')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_l[i];  //l
  }
   else if(message_store[message_count]=='m')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_m[i];  //m
  } 
  else if(message_store[message_count]=='n')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_n[i];  //n
  }
   else if(message_store[message_count]=='o')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_o[i];  //o
  } 
  else if(message_store[message_count]=='p')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_p[i];  //p
  } 
  else if(message_store[message_count]=='q')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_q[i];  //q
  }
   else if(message_store[message_count]=='r')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_r[i];  //r
  }
  else if(message_store[message_count]=='s')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_s[i];  //s
  }
   else if(message_store[message_count]=='t')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_t[i];  //t
  } 
  else if(message_store[message_count]=='u')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_u[i];  //u
  }
   else if(message_store[message_count]=='v')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_v[i];  //v
  } 
  else if(message_store[message_count]=='w')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_w[i];  //w
  }
   else if(message_store[message_count]=='x')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_x[i];  //x
  } 
  else if(message_store[message_count]=='y')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_y[i];  //y
  }
   else if(message_store[message_count]=='z')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_z[i];  //z
  } 
  else if(message_store[message_count]==' ')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_blank[i];  //blank
  }
   else if(message_store[message_count]=='/')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_fslash[i];  //forword slash
  } 
  else if(message_store[message_count]=='-')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_dash[i];  //-
  }
   else if(message_store[message_count]=='.')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_dot[i];  //.
  }
  else if(message_store[message_count]=='!')
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_ex[i];  //!
  }
  else
  {
   for(i=0;i<16;i++)
    message[i+(16*message_count)]=data_m_blank[i];  //
  }
 }
}

void clear_horizontal_data(void)
{
  for(rec_im=0;rec_im<16;rec_im++)
  {
   data_temp_m[rec_im]=0;
   data_temp_m1[rec_im]=0;
   data_temp_m2[rec_im]=0;
   data_temp_m3[rec_im]=0;
   data_temp_m4[rec_im]=0;
   data_temp_m5[rec_im]=0;
   data_temp_m6[rec_im]=0;
   data_temp_m7[rec_im]=0;
   data_temp_m8[rec_im]=0;
   data_temp_m9[rec_im]=0;
   data_temp_m10[rec_im]=0;
   data_temp_m11[rec_im]=0;
   data_temp_m12[rec_im]=0;
   data_temp_m13[rec_im]=0;
   data_temp_m14[rec_im]=0;
   data_temp_m15[rec_im]=0; 
  } 
}
//****************************************wifi initialization and other function***********************************************************************************************
void wifi_init(void)
{
      Serial.begin ( 9600 );
      WiFi.mode(WIFI_AP_STA);           //Only Access point
      WiFi.softAP(ssid, password);  //Start HOTspot removing password will disable security
      IPAddress myIP = WiFi.softAPIP(); //Get IP address
 
      Serial.print("HotSpt IP:");
      Serial.println(myIP);
//-------------------------------------------------------------------------------------------------------------
 /* if (!SPIFFS.begin())
    Serial.println("SPIFFS Mount failed");
  else 
    Serial.println("SPIFFS Mount succesfull");
  
  //server.serveStatic("/img", SPIFFS, "/img");
  //server.serveStatic("/", SPIFFS, "/index.html");
  server.serveStatic("/", SPIFFS, "/index1.html");*/
//--------------------------------------------------------------------------------------------------------------
  server.begin();
  Serial.println ( "HTTP server started" );
  server.on("/", handleRoot);
  server.on("/update3", handleUpdate);
  server.on("/update1", handleUpdate1);
  server.on("/update2", handleUpdate2);
  server.on("/init", handleInit);
  //server.on("/index.html",handleRoot );

  
  
}